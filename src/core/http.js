import React from "react";
import axios from "axios";
import { notification } from "antd";

function credentialFn() {

  let adminId = localStorage.getItem('adminId');
  let token = localStorage.getItem('token');
  let userName = localStorage.getItem('userName');
  let isSuperUser = localStorage.getItem('isSuperUser') === "1";
  let permission = JSON.parse(localStorage.getItem('permission') || '[]');

  this.setAdminId = (_adminId) => {
    adminId = _adminId;
    if (_adminId) {
      localStorage.setItem("adminId", _adminId);
    } else {
      localStorage.removeItem("adminId");
    }
  };
  this.getAdminId = () => {
    return adminId;
  };
  this.setToken = (_token) => {
    token = _token;
    if (_token) {
      localStorage.setItem("token", _token);
    } else {
      localStorage.removeItem("token");
    }
  };
  this.getToken = () => {
    return token;
  };
  this.setUserName = (_userName) => {
    userName = _userName;
    if (_userName) {
      localStorage.setItem("userName", _userName);
    } else {
      localStorage.removeItem("userName");
    }
  };
  this.getUserName = () => {
    return userName;
  };
  this.setIsSuperUser = (_isSuperUser) => {
    isSuperUser = _isSuperUser === 1;
    if (_isSuperUser) {
      localStorage.setItem("isSuperUser", JSON.stringify(_isSuperUser));
    } else {
      localStorage.removeItem("isSuperUser");
    }
  };
  this.getIsSuperUser = () => {
    return isSuperUser;
  };
  this.setPermission = (_permission) => {
    permission = _permission;
    if (_permission) {
      localStorage.setItem("permission", JSON.stringify(_permission));
    } else {
      localStorage.removeItem("permission");
    }
  };
  this.getPermission = () => {
    return permission;
  };
}

export const credential = new credentialFn();

export const APP_BASE_URL = 'http://app.c895.xyz/';

let baseURL = 'http://admin.c895.xyz/';

if (PRODUCTION) {
  baseURL = '';
}

export const BASE_URL = baseURL;

const instance = axios.create({
  baseURL, timeout: 30000,
});

instance.defaults.headers.post['Content-Type'] = 'application/json';
instance.defaults.headers.delete['Content-Type'] = 'application/json';

instance.interceptors.request.use((request) => {
  let adminId = credential.getAdminId();
  let token = credential.getToken();
  if (request.method !== 'OPTIONS'
    && request.method !== 'options'
    && (request.url.indexOf('login') < 0 || request.url.indexOf('user/login/list') >= 0)
    && request.baseURL.indexOf(APP_BASE_URL) < 0) {
    if (adminId) {
      request.headers = {
        ...(request.headers || {}),
        'admin-id': adminId,
      };
    }
    if (token) {
      request.headers = {
        ...(request.headers || {}),
        token: token,
      };
    }
  }
  return request;
}, (error) => {
  notification.error({
    duration: 3,
    message: '请求错误',
    description: error,
  });
  return Promise.reject(error);
});

instance.interceptors.response.use((response) => {
  const { data, error_no, error_msg } = response.data;
  const result = {
    data,
    errNo: error_no,
    errMsg: error_msg,
  };
  if (error_no !== 0 && error_no !== 100) {
    let errMsg = error_msg;
    if (error_msg.toUpperCase().indexOf('TOKEN') > -1
      || error_msg.indexOf('登录') > -1) {
      errMsg = <span>登录已过期，请<a href="/login">重新登录</a></span>;
    }
    notification.destroy();
    notification.error({
      duration: 60,
      message: '错误消息',
      description: errMsg,
    });
    return Promise.reject(result);
  } else {
    return Promise.resolve(result);
  }
}, (error) => {
  let errTitle = '未知异常';
  let errContent = <span>发生未知错误，请刷新或<a href="/login">重新登录</a>重试</span>
  if (typeof (error) === 'string') {
    errContent = error;
  }
  notification.error({
    duration: 3,
    message: errTitle,
    description: errContent,
  });
  return Promise.reject(error);
});

export default instance;
