import { connect } from 'dva';

function extendedConnect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps, options) {
    let overrideMapStateToProps = (state, ownProps) => {
      let originalMappedProps = (mapStateToProps && mapStateToProps(state, ownProps)) || {};
      return {
        authenticated: state.app.authenticated,
        routePath: state.app.location.path,
        menus: state.app.menus,
        ...originalMappedProps,
      };
    }
    return connect(
      overrideMapStateToProps,
      mapDispatchToProps,
      mergeProps, options
    );
 }

 export default extendedConnect;