import * as React from 'react';
import dva from 'dva';
import { Router, Route, withRouter } from 'dva/router';
import LocaleProvider from 'antd/lib/locale-provider';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import message from 'antd/lib/message';
import dynamic, { registerModel } from './dynamic';

import moment from 'moment';
import 'moment/locale/zh-cn';

moment.locale('zh-cn');

let app;
let appOpts;

const defaultSetupProviders = [];
let localSetupProviders;

const RouterRenderer = withRouter(props => props.render());

// opts: { defaultRoute, defaultModule ... }
const moka = function (opts) {
  appOpts = opts;
  app = dva({
    onError(error) {
      if (error.message) {
        // message.destroy();
        // message.error(error.message);
        // console.error(error.message);
      }
    },
    ...opts,
  });
  if (opts.defaultModule && opts.defaultModule.model) {
    registerModel(app, opts.defaultModule.model);
  }
  return moka;
};

moka.setup = function (...setupProviders) {
  localSetupProviders = setupProviders;
  return moka;
};

moka.use = function (plugin) {
  app.use(plugin);
  return moka;
};

function renderDynamicRoutes(localApp, parentKey, modules) {
  return modules.map((mokaModule) => {
    const lazyComponent = dynamic({
      app: localApp,
      module: mokaModule.module,
    })
   return <Route key={parentKey + mokaModule.route} exact path={mokaModule.route} component={lazyComponent} />
  });
}

// module: { route, module ... }
moka.modules = function (modules) {
  const dvaRoutes = function ({ history, app: localApp }) { // eslint-disable-line
    let DefaultComponent = appOpts.defaultModule && appOpts.defaultModule.component;
    return (
      <LocaleProvider locale={zh_CN}>
        <Router history={history}>
          <RouterRenderer render={() => (
            !!DefaultComponent ? (
              <DefaultComponent>
                {renderDynamicRoutes(localApp, 'root', modules)}
              </DefaultComponent>
            ) : renderDynamicRoutes(localApp, 'root', modules)
          )} />
        </Router>
      </LocaleProvider>
    );
  };
  app.router(dvaRoutes);
  return moka;
};

moka.start = function (selector) {
  localSetupProviders = localSetupProviders || defaultSetupProviders;
  localSetupProviders.forEach(provider => provider({ app }));
  return app.start(selector);
};

export default moka;
