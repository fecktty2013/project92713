import * as React from 'react';
import { Route, Redirect } from "dva/router";

export default function withAuth(WrappedComponent) {
  // TODO
  // 除了验证是否登录，还需要验证菜单是否授权
  return function ComponentWithAuth(props) {
    const { authenticated, menus, routePath } = props;
    const authorized = menus.some(m => {
      return m.href === routePath || (
        m.subMenus && m.subMenus.some(sm => sm.href === routePath)
      );
    });
    return authenticated && authorized ? (
      <WrappedComponent {...props} />
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }} />
    )
  }
}