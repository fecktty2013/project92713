import pathToRegexp from 'path-to-regexp';

function match(pathname, routePath) {
  const keys = [];
  const groups = pathToRegexp(routePath, keys).exec(pathname, keys);
  if (groups) {
    const params = {};
    keys.forEach((k, i) => {
      params[k.name] = groups[i + 1];
    });
    return params;
  }
  return undefined;
}

export default function setupSubscription(dispatch, routePath, pathname, query, action) {
  const params = match(pathname, routePath);
  if (params && action) {
    dispatch({ type: action, payload: { params, query } });
  }
}
