import connect from "./connect";
import withAuth from "./withAuth";

export default function(mapStateToProps,
  mapDispatchToProps,
  mergeProps, options) {
    return (WrappedComponent) => connect(
      mapStateToProps,
      mapDispatchToProps,
      mergeProps, options
    )(withAuth(WrappedComponent));
}