import React from "react";
import * as history from "history";
import createLoading from "dva-loading";
import moka from "./core/moka";
import app from "./modules/app";
import "./style.scss";

moka({
  defaultRoute: "/",
  defaultModule: app,
  history: history.createBrowserHistory(),
})
  .use(createLoading())
  .modules([
    {
      route: '/',
      module: () => import(/* webpackChunkName: "overview" */ './modules/overview'),
    },
    {
      route: '/login',
      module: () => import(/* webpackChunkName: "login" */ './modules/login'),
    },
    {
      route: '/modifyPassword',
      module: () => import(/* webpackChunkName: "modifyPassword" */ './modules/modifyPassword'),
    },
    {
      route: '/user/manage',
      module: () => import(/* webpackChunkName: "userManage" */ './modules/user/manage'),
    },
    {
      route: '/user/:userId/edit',
      module: () => import(/* webpackChunkName: "userEdit" */ './modules/user/edit'),
    },
    {
      route: '/user/avatars',
      module: () => import(/* webpackChunkName: "userAvatar" */ './modules/user/avatar'),
    },
    {
      route: '/user/distribution',
      module: () => import(/* webpackChunkName: "userDistribution" */ './modules/user/distribution'),
    },
    {
      route: '/content/video',
      module: () => import(/* webpackChunkName: "videoManage" */ './modules/content/video'),
    },
    {
      route: '/content/audio',
      module: () => import(/* webpackChunkName: "audioManage" */ './modules/content/audio'),
    },
    {
      route: '/content/photo',
      module: () => import(/* webpackChunkName: "photoManage" */ './modules/content/photo'),
    },
    {
      route: '/category',
      module: () => import(/* webpackChunkName: "categoryManage" */ './modules/category'),
    },
    {
      route: '/operation/manage',
      module: () => import(/* webpackChunkName: "opManage" */ './modules/operation/manage'),
    },
    {
      route: '/operation/settings',
      module: () => import(/* webpackChunkName: "opSettings" */ './modules/operation/settings'),
    },
    {
      route: '/finance',
      module: () => import(/* webpackChunkName: "financeManage" */ './modules/finance'),
    },
    {
      route: '/settings',
      module: () => import(/* webpackChunkName: "settingsManage" */ './modules/settings'),
    },
    {
      route: '/comments',
      module: () => import(/* webpackChunkName: "commentsManage" */ './modules/comments'),
    },
    {
      route: '/comments/robots',
      module: () => import(/* webpackChunkName: "robotsManage" */ './modules/comments/robots'),
    },
    {
      route: '/protected',
      module: () => import(/* webpackChunkName: "protected" */ './modules/protect'),
    },
    {
      route: '/accounts',
      module: () => import(/* webpackChunkName: "accountManage" */ './modules/account'),
    },
    {
      route: '/logging',
      module: () => import(/* webpackChunkName: "logging" */ './modules/logging'),
    },
    {
      route: '/system',
      module: () => import(/* webpackChunkName: "system" */ './modules/system'),
    },
  ])
  .start("#app");
