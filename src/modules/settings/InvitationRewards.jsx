import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber } from "antd";

import style from "./style.scss";

const InvitationRewards = (props) => {

  const { getFieldDecorator, validateFields, resetFields } = props.form;

  const { editingInviReward } = props.settingsManage;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleQuery = () => {
    props.dispatch({
      type: 'settingsManage/fetchInviRewardsList',
      pagination,
    });
  };

  const handleTableChanged = (p) => {
    setPagination(p);
  };

  const resetForm = () => {
    resetFields(["editingRewardName", "editingCoinCount"]);
  };

  const eventHandlers = {
    handleNewInviRewardsStart(e) {
      resetForm();
      props.dispatch({ type: 'settingsManage/newInviRewardStart' });
    },
    handleEditInviRewardsStart(record) {
      resetForm();
      props.dispatch({
        type: 'settingsManage/editInviRewardStart',
        payload: record,
      });
    },
    handleInviRewardsCancel(e) {
      props.dispatch({
        type: 'settingsManage/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    handleInviRewardsSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'settingsManage/saveInviReward',
          payload: values,
        });
        await props.dispatch({
          type: 'settingsManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const columns = [
    { title: '序号', dataIndex: 'sn' },
    { title: '层级', dataIndex: 'level' },
    { title: '硬币数量', dataIndex: 'coinCount' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditInviRewardsStart(record)}>编辑</Button>
        </span>
      )
    },
  ];

  useEffect(handleQuery, [pagination]);

  return (
    <React.Fragment>
      <Card title="邀请奖励"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
        extra={
          <Button type="dashed" className={style.headBtn} onClick={eventHandlers.handleNewInviRewardsStart}>
            <Icon type="plus" /> 新增
          </Button>
        }
      >
        <Table
          rowKey={record => record.id}
          columns={columns}
          onChange={handleTableChanged}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.settingsManage.inviRewardsTotal,
            showTotal: () => <span>总共 {props.settingsManage.inviRewardsTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          scroll={{ x: 'max-content' }}
          dataSource={props.settingsManage.inviRewardsList}
        />
      </Card>
      <Modal
        title="邀请奖励"
        width={600}
        visible={props.settingsManage.modalVisibility === 'inviRewards'}
        onCancel={eventHandlers.handleInviRewardsCancel}
        onOk={eventHandlers.handleInviRewardsSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="层级">
            {getFieldDecorator('editingRewardName', {
              rules: [{ required: true, message: '请输入层级信息' }],
              initialValue: editingInviReward.level
            })(<Input />)}
          </Form.Item>
          <Form.Item label="硬币数量">
            {getFieldDecorator('editingCoinCount', {
              rules: [{ required: true, message: '请输入硬币数量' }],
              initialValue: editingInviReward.coinCount
            })(<InputNumber style={{ width: '100%' }} />)}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(InvitationRewards);
