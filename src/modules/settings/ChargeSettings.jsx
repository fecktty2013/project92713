import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber } from "antd";

import style from "./style.scss";

const ChargeSettings = (props) => {

  const { getFieldDecorator, validateFields } = props.form;

  const { editingCharge } = props.settingsManage;

  const [pagination ,setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleQueryStatus = () => {
    props.dispatch({ type: 'settingsManage/getChargeStatus' });
  };

  const handleQuery = () => {
    props.dispatch({ type: 'settingsManage/fetchChargeList', pagination });
  };

  const handleTableChanged = (p) => {
    setPagination(p);
  };

  const eventHandlers = {
    handleCloseCharge(e) {
      props.dispatch({
        type: 'settingsManage/setChargeStatus',
        payload: { status: 0 },
      });
    },
    handleOpenCharge(e) {
      props.dispatch({
        type: 'settingsManage/setChargeStatus',
        payload: { status: 1 },
      });
    },
    handleNewChargeStart(e) {
      props.dispatch({ type: 'settingsManage/newChargeStart' });
    },
    handleEditChargeStart(record) {
      props.dispatch({
        type: 'settingsManage/editChargeStart',
        payload: record,
      });
    },
    handleChargeCancel(e) {
      props.dispatch({
        type: 'settingsManage/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    handleChargeSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'settingsManage/saveCharge',
          payload: values,
        });
        await props.dispatch({
          type: 'settingsManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const columns = [
    { title: '序号', dataIndex: 'sn' },
    { title: '数量', dataIndex: 'quantity' },
    { title: '金额', dataIndex: 'amount' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditChargeStart(record)}>编辑</Button>
        </span>
      )
    },
  ];

  useEffect(handleQuery, [pagination]);
  useEffect(handleQueryStatus, []);

  return (
    <React.Fragment>
      <Card title="充值设置"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
        extra={
          <React.Fragment>
            <Button type="dashed" className={style.headBtn} onClick={eventHandlers.handleNewChargeStart}>
              <Icon type="plus" /> 新增
            </Button>
            {props.settingsManage.chargeStatus === 1 && <Button type="dashed" className={style.headBtn} onClick={eventHandlers.handleCloseCharge}>
              <Icon type="close" /> 关闭充值
            </Button>}
            {props.settingsManage.chargeStatus === 0 && <Button type="dashed" className={style.headBtn} onClick={eventHandlers.handleOpenCharge}>
              <Icon type="close" /> 开启充值
            </Button>}
          </React.Fragment>
        }
      >
        <Table
          rowKey={record => record.id}
          columns={columns}
          onChange={handleTableChanged}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.settingsManage.chargeTotal,
            showTotal: () => <span>总共 {props.settingsManage.chargeTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          pagination={false}
          scroll={{ x: 'max-content' }}
          dataSource={props.settingsManage.chargeList}
        />
      </Card>
      <Modal
        title="充值设置"
        width={600}
        visible={props.settingsManage.modalVisibility === 'charge'}
        onCancel={eventHandlers.handleChargeCancel}
        onOk={eventHandlers.handleChargeSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="硬币数量">
            {getFieldDecorator('editingQuantity', {
              rules: [{ required: true, message: '请输入硬币数量' }],
              initialValue: editingCharge.quantity || 0
            })(<InputNumber style={{ width: '100%' }} />)}
          </Form.Item>
          <Form.Item label="硬币金额">
            {getFieldDecorator('editingAmount', {
              rules: [{ required: true, message: '请输入硬币金额' }],
              initialValue: editingCharge.amount || 0
            })(<InputNumber style={{ width: '100%' }} />)}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(ChargeSettings);
