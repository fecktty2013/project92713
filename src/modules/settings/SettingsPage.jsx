import React from "react";
import { Breadcrumb, Row, Col } from "antd";
import connectWithAuth from "../../core/connectWithAuth";
import RewardsSettings from "./RewardsSettings";
import ChargeSettings from "./ChargeSettings";
import RedeemCode from "./RedeemCode";
import InvitationRewards from "./InvitationRewards";

import style from "./style.scss";
import DomainSettings from "./DomainSettings";

const SettingsPage = (props) => {

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>设置管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <DomainSettings {...props} />
        <RewardsSettings {...props} />
        <ChargeSettings {...props} />
        <RedeemCode {...props} />
        <InvitationRewards {...props} />
      </div>
    </div>
  )
};

export default connectWithAuth(({ settingsManage }) => ({ settingsManage }))(SettingsPage);
