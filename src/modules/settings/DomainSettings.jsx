import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Popconfirm, InputNumber } from "antd";

import style from "./style.scss";
import { ADV_STATUS } from "../../common/constants";

const DomainSettings = (props) => {

  const { getFieldDecorator, validateFields, resetFields } = props.form;

  const { editingDomain } = props.settingsManage;

  const handleQuery = () => {
    props.dispatch({ type: 'settingsManage/fetchDomainList' });
  };

  const resetForm = () => {
    resetFields(["editingName", "editingDomain", "editingWeight", "editingStatus"]);
  };

  const eventHandlers = {
    handleNewDomainStart(e) {
      resetForm();
      props.dispatch({ type: 'settingsManage/newDomainStart' });
    },
    handleEditDomainStart(record) {
      resetForm();
      props.dispatch({
        type: 'settingsManage/editDomainStart',
        payload: record,
      });
    },
    async handleUpdateDomainStatus(record, status) {
      await props.dispatch({
        type: 'settingsManage/saveDomain',
        payload: { 
          id: record.id,
          editingName: record.name,
          editingDomain: record.domain,
          editingWeight: record.weight,
          editingStatus: status === 1,
        },
      });
      handleQuery();
    },
    handleDomainCancel(e) {
      props.dispatch({
        type: 'settingsManage/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    async handleDomainDelete(record) {
      await props.dispatch({
        type: 'settingsManage/deleteDomain',
        payload: record,
      });
      handleQuery();
    },
    handleDomainSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'settingsManage/saveDomain',
          payload: values,
        });
        await props.dispatch({
          type: 'settingsManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const columns = [
    { title: '序号', dataIndex: 'sn' },
    { title: '名称', dataIndex: 'name' },
    { title: '域名', dataIndex: 'domain' },
    { title: '权重', dataIndex: 'weight' },
    { title: '状态', dataIndex: 'status' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleDomainDelete(record)}>
            <a>删除</a>
          </Popconfirm>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditDomainStart(record)}>编辑</Button>
          {
            record.statusVal == 1 ?
              <Popconfirm title="确认禁用吗？" onConfirm={e => eventHandlers.handleUpdateDomainStatus(record, ADV_STATUS.DISABLED)}>
                <Button className={style.rowBtn} type="link">禁用</Button>
              </Popconfirm> : undefined
          }
          {
            record.statusVal == 2 ?
              <Popconfirm title="确认启用吗？" onConfirm={e => eventHandlers.handleUpdateDomainStatus(record, ADV_STATUS.ENABLED)}>
                <Button className={style.rowBtn} type="link">启用</Button>
              </Popconfirm> : undefined
          }
        </span>
      )
    },
  ];

  const validateDomain = (rule, value, callback) => {
    let pattern = /^https?:\/\/.+/;
    if (value && !pattern.test(value)) {
      callback('域名必须包含协议字段，如http://a.b.c/');
      return;
    }
    callback();
  };

  const validateWeight = (rule, value, callback) => {
    if (value && (value < 0 || value > 100)) {
      callback('权重必须输入0到100之间的数值'); 
      return;
    }
    callback();
  };

  useEffect(handleQuery, []);

  return (
    <React.Fragment>
      <Card title="域名设置"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
        extra={
          <React.Fragment>
            <Button type="dashed" className={style.headBtn} onClick={eventHandlers.handleNewDomainStart}>
              <Icon type="plus" /> 新增
            </Button>
          </React.Fragment>
        }
      >
        <Table
          rowKey={record => record.id}
          columns={columns}
          pagination={false}
          scroll={{ x: 'max-content' }}
          dataSource={props.settingsManage.domainList}
        />
      </Card>
      <Modal
        title={!!editingDomain.id ? '编辑域名' : '新增域名'}
        width={600}
        visible={props.settingsManage.modalVisibility === 'domain'}
        onCancel={eventHandlers.handleDomainCancel}
        onOk={eventHandlers.handleDomainSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="名称">
            {getFieldDecorator('editingName', {
              rules: [{ required: true, message: '请输入名称' }],
              initialValue: editingDomain.name,
            })(<Input style={{ width: '100%' }} />)}
          </Form.Item>
          <Form.Item label="域名">
            {getFieldDecorator('editingDomain', {
              rules: [
                { required: true, message: '请输入完整域名' },
                { validator: validateDomain },
              ],
              initialValue: editingDomain.domain,
            })(<Input placeholder="域名必须包含协议字段，如http://a.b.c/" style={{ width: '100%' }} />)}
          </Form.Item>
          <Form.Item label="权重">
            {getFieldDecorator('editingWeight', {
              rules: [
                { required: true, message: '请输入域名权重' },
                { validator: validateWeight },
              ],
              initialValue: editingDomain.weight || 0,
            })(<InputNumber style={{ width: '100%' }} />)}
          </Form.Item>
          <Form.Item label="状态">
            {getFieldDecorator('editingStatus', {
              initialValue: editingDomain.statusVal == 1,
              valuePropName: 'checked',
            })(
              <Checkbox>启用</Checkbox>
            )}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(DomainSettings);
