import { routerRedux } from 'dva/router';
import * as service from './service';

const initialState = {
  rewardsList: [],
  rewardsTotal: 0,

  chargeList: [],
  chargeTotal: 0,
  chargeStatus: 1,
  editingCharge: {},

  redeemCodeList: [],
  redeemCodeTotal: 0,

  inviRewardsList: [],
  inviRewardsTotal: 0,
  editingInviReward: {},

  domainList: [],
  editingDomain: {},

  modalVisibility: '',

};

export default {
  namespace: 'settingsManage',

  state: initialState,

  subscriptions: {
  },

  effects: {
    *fetchRewardsList({ pagination }, { put, select, call }) {
      const httpResult = yield call(service.fetchRewardList, {
        reward_type: 'task',
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const rewardsList = httpResult.data.reward_list.map((itm, idx) => ({
        id: itm.reward_id,
        sn: idx + 1,
        task: itm.reward_name,
        coinCount: itm.coin_num,
        rewardsInterval: itm.interval_zh,
      }));
      const rewardsTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { rewardsList, rewardsTotal } });
    },
    *saveNewReward({ payload }, { put, call }) {
      console.log('save new', payload);
      const intervalType = payload.editingRewardsInterval.unit === 'byTimes' ? 'times' : 'cycle';
      yield call(service.saveReward, {
        reward_name: payload.editingTask,
        coin_num: payload.editingCoinCount,
        reward_type: 'task',
        interval_type: intervalType,
        interval: payload.editingRewardsInterval.number,
      });
    },
    *deleteReward({ payload }, { call }) {
      yield call(service.deleteReward, { reward_id: payload.id });
    },
    *fetchChargeList({ pagination }, { put, call, select }) {
      const httpResult = yield call(service.fetchChargeList, {
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const chargeList = httpResult.data.config_list.map((itm, idx) => ({
        id: itm.config_id,
        sn: idx + 1,
        quantity: itm.coin_num,
        amount: itm.pay_money,
      }));
      const chargeTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { chargeList, chargeTotal } });
    },
    *newChargeStart({ payload }, { call, put }) {
      yield put({
        type: 'stateChanged',
        payload: {
          editingCharge: {},
          modalVisibility: 'charge',
        }
      });
    },
    *editChargeStart({ payload }, { call, put }) {
      yield put({
        type: 'stateChanged',
        payload: {
          editingCharge: payload,
          modalVisibility: 'charge',
        }
      });
    },
    *saveCharge({ payload }, { select, call }) {
      let httpRequest = {
        coin_num: payload.editingQuantity,
        pay_money: payload.editingAmount
      };
      const { editingCharge } = yield select(s => s.settingsManage);
      if (editingCharge.id !== undefined) {
        httpRequest.config_id = editingCharge.id;
      }
      yield call(service.saveCharge, httpRequest);
    },
    *getChargeStatus(action, { put, call }) {
      const httpResult = yield call(service.getChargeStatus);
      yield put({ type: 'stateChanged', payload: { chargeStatus: httpResult.data.enable } });
    },
    *setChargeStatus({ payload }, { put, call }) {
      yield call(service.saveChargeStatus, {
        enable: payload.status,
      });
      yield put({ type: 'stateChanged', payload: { chargeStatus: payload.status } })
    },
    *fetchRedeemCodeList({ pagination }, { call, put, select }) {
      const httpResult = yield call(service.fetchRedeemList, {
        order_type: 'exchange',
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const redeemCodeList = httpResult.data.order_list.map((itm, idx) => ({
        id: itm.order_id,
        sn: idx + 1,
        redeemCode: itm.redeem_code,
        coinCount: itm.gain_coin,
        status: itm.is_complete == 1 ? '已使用' : '未使用',
        createdTime: itm.create_time,
        usedTime: itm.charge_time,
        userId: itm.user_id,
      }));
      const redeemCodeTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { redeemCodeList, redeemCodeTotal } });
    },
    *batchSaveRedeem({ payload }, { call, put }) {
      yield call(service.batchCreateRedeem, {
        num: payload.editingRedeemCount,
        gain_coin: payload.editingCoinCount,
      });
    },
    *fetchInviRewardsList({ pagination }, { put, call }) {
      const httpResult = yield call(service.fetchRewardList, {
        reward_type: 'invite',
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const inviRewardsList = httpResult.data.reward_list.map((itm, idx) => ({
        id: itm.reward_id,
        sn: idx + 1,
        level: itm.reward_name,
        coinCount: itm.coin_num,
      }));
      const inviRewardsTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { inviRewardsList, inviRewardsTotal } });
    },
    *newInviRewardStart({ payload }, { call, put }) {
      yield put({
        type: 'stateChanged',
        payload: {
          editingInviReward: {},
          modalVisibility: 'inviRewards',
        }
      });
    },
    *editInviRewardStart({ payload }, { select, call, put }) {
      yield put({
        type: 'stateChanged',
        payload: {
          editingInviReward: payload,
          modalVisibility: 'inviRewards',
        }
      });
    },
    *saveInviReward({ payload }, { select, call, put }) {
      const { editingInviReward } = yield select(state => state.settingsManage);
      let httpRequest = {
        reward_name: payload.editingRewardName,
        coin_num: payload.editingCoinCount,
        reward_type: 'invite',
        interval_type: 'times',
        interval: 1,
      };
      if (editingInviReward.id !== undefined) {
        httpRequest.config_id = editingInviReward.id;
      }
      yield call(service.saveReward, httpRequest);
    },
    *fetchDomainList(action, { put, call }) {
      const httpResult = yield call(service.fetchDomainList);
      const domainList = httpResult.data.domain_list.map((itm, idx) => ({
        id: itm.domain_id,
        sn: idx + 1,
        name: itm.name,
        domain: itm.domain,
        weight: itm.weight,
        statusVal: itm.status,
        status: itm.status_txt,
      }));
      yield put({ type: 'stateChanged', payload: { domainList } });
    },
    *newDomainStart({ payload }, { call, put }) {
      yield put({
        type: 'stateChanged',
        payload: {
          editingDomain: {},
          modalVisibility: 'domain',
        }
      });
    },
    *editDomainStart({ payload }, { select, call, put }) {
      yield put({
        type: 'stateChanged',
        payload: {
          editingDomain: payload,
          modalVisibility: 'domain',
        }
      });
    },
    *saveDomain({ payload }, { select, call, put }) {
      const { editingDomain } = yield select(state => state.settingsManage);
      let httpRequest = {
        name: payload.editingName,
        domain: payload.editingDomain,
        weight: payload.editingWeight,
        status: payload.editingStatus ? 1 : 2,
      };
      httpRequest.domain_id = payload.id || editingDomain.id;
      yield call(service.saveDomain, httpRequest);
    },
    *deleteDomain({ payload }, { call }) {
      yield call(service.deleteDomain, { domain_id: payload.id });
    },
    *setModalVisibility({ payload: { modalVisibility }}, { put }) {
      yield put({ type: 'stateChanged', payload: { modalVisibility }});
    },
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}