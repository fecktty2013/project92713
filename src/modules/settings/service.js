import $http from "../../core/http";

export function saveReward(payload) {
  return $http.post("/config/reward", payload);
}

export function deleteReward(payload) {
  return $http.delete("/config/reward", {
    params: payload,
  });
}

export function fetchRewardList(payload) {
  return $http.get("/config/reward/list", {
    params: payload,
  });
}

export function saveCharge(payload) {
  return $http.post("/config/charge", payload);
}

export function deleteCharge(payload) {
  return $http.delete("/config/charge", {
    params: payload,
  });
}

export function fetchChargeList(payload) {
  return $http.get("/config/charge/list", {
    params: payload,
  });
}

export function getChargeStatus() {
  return $http.get("/config/charge/enable");
}

export function saveChargeStatus(payload) {
  return $http.post("/config/charge/enable", payload);
}

export function fetchRedeemList(payload) {
  return $http.get("/config/order/list", {
    params: payload,
  });
}

export function batchCreateRedeem(payload) {
  return $http.post("/config/order/batch_create", payload);
}

export function fetchDomainList() {
  return $http.get("/config/domain/list");
}

export function saveDomain(payload) {
  return $http.post("/config/domain", payload);
}

export function deleteDomain(payload) {
  return $http.delete("/config/domain", {
    params: payload,
  });
}
