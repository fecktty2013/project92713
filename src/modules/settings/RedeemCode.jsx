import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber } from "antd";

import style from "./style.scss";

const RedeemCode = (props) => {

  const { getFieldDecorator, validateFields } = props.form;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleQuery = () => {
    props.dispatch({
      type: 'settingsManage/fetchRedeemCodeList',
      pagination,
    });
  };

  const handleTableChanged = (p) => {
    setPagination(p);
  };

  const eventHandlers = {
    handleNewRedeemCodeStart(e) {
      props.dispatch({
        type: 'settingsManage/setModalVisibility',
        payload: { modalVisibility: 'redeemCode' },
      });
    },
    handleRedeemCodeCancel(e) {
      props.dispatch({
        type: 'settingsManage/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    handleRedeemCodeSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'settingsManage/batchSaveRedeem',
          payload: values,
        });
        await props.dispatch({
          type: 'settingsManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const columns = [
    { title: '序号', dataIndex: 'sn' },
    { title: '兑换码', dataIndex: 'redeemCode' },
    { title: '硬币数量', dataIndex: 'coinCount' },
    { title: '使用状态', dataIndex: 'status' },
    { title: '创建时间', dataIndex: 'createdTime' },
    { title: '使用时间', dataIndex: 'usedTime' },
    { title: '用户id', dataIndex: 'userId' },
  ];

  useEffect(handleQuery, [pagination]);

  return (
    <React.Fragment>
      <Card title="兑换码"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
        extra={
          <React.Fragment>
            <Button type="dashed" onClick={eventHandlers.handleNewRedeemCodeStart}>
              <Icon type="plus" /> 新增
            </Button>
          </React.Fragment>
        }
      >
        <Table
          rowKey={record => record.id}
          columns={columns}
          onChange={handleTableChanged}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.settingsManage.redeemCodeTotal,
            showTotal: () => <span>总共 {props.settingsManage.redeemCodeTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          scroll={{ x: 'max-content' }}
          dataSource={props.settingsManage.redeemCodeList}
        />
      </Card>
      <Modal
        title="新增兑换码"
        width={600}
        visible={props.settingsManage.modalVisibility === 'redeemCode'}
        onCancel={eventHandlers.handleRedeemCodeCancel}
        onOk={eventHandlers.handleRedeemCodeSubmit}
        bodyStyle={{ paddingBottom: 12 }}
        footer={null}
      >
        <Form {...formItemLayout}>
          <Form.Item label="兑换码数量">
            {getFieldDecorator('editingRedeemCount', {
              rules: [{ required: true, message: '请输入兑换码' }],
            })(<Input />)}
          </Form.Item>
          <Form.Item label="硬币数量">
            {getFieldDecorator('editingCoinCount', {
              rules: [{ required: true, message: '请输入硬币数量' }],
            })(<InputNumber style={{ width: '100%' }} />)}
          </Form.Item>
          <Form.Item wrapperCol={{ span: 18, offset: 3 }} label="" colon={false}>
            <Row type="flex" justify="center">
              <Col><Button className={style.dlgBtn} type="primary" onClick={eventHandlers.handleRedeemCodeSubmit}>确认</Button></Col>
              <Col><Button className={style.dlgBtn} onClick={eventHandlers.handleRedeemCodeCancel}>取消</Button></Col>
            </Row>
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(RedeemCode);
