import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, Popconfirm, Select, Table, Modal, Input, Checkbox, Upload, InputNumber } from "antd";
import NumberWithUnit from "../../components/NumberWithUnit";

import style from "./style.scss";

const RewardsSettings = (props) => {

  const { getFieldDecorator, validateFields } = props.form;
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleQuery = () => {
    props.dispatch({ type: 'settingsManage/fetchRewardsList', pagination });
  };

  const handleTableChanged = (p) => {
    setPagination(p);
  };

  const eventHandlers = {
    handleNewRewardsStart(e) {
      props.dispatch({
        type: 'settingsManage/setModalVisibility',
        payload: { modalVisibility: 'rewards' },
      });
    },
    handleRewardsCancel(e) {
      props.dispatch({
        type: 'settingsManage/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    async handleRewardsDelete(record) {
      await props.dispatch({
        type: 'settingsManage/deleteReward',
        payload: record,
      });
      handleQuery();
    },
    handleRewardsSubmit(e) {
      validateFields(async (err, values) => {
        if (err) return;
        await props.dispatch({ type: 'settingsManage/saveNewReward', payload: values });
        await props.dispatch({
          type: 'settingsManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const columns = [
    { title: '序号', dataIndex: 'sn' },
    { title: '任务', dataIndex: 'task' },
    { title: '硬币数量', dataIndex: 'coinCount' },
    { title: '奖励间隔', dataIndex: 'rewardsInterval' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleRewardsDelete(record)}>
            <a>删除</a>
          </Popconfirm>
        </span>
      )
    },
  ];

  useEffect(handleQuery, [pagination]);

  const unitOptions = [
    { value: 'byTimes', text: '次' },
    { value: 'byHours', text: '小时' },
  ];

  const checkRewardInterval = (rule, value, callback) => {
    if (!value || value.number === undefined || !value.unit) {
      callback('请选择输入奖励间隔');
    } else {
      callback();
    }
  };

  return (
    <React.Fragment>
      <Card title="奖励设置"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
        extra={
          <Button type="dashed" onClick={eventHandlers.handleNewRewardsStart}>
            <Icon type="plus" /> 新增
          </Button>
        }
      >
        <Table
          rowKey={record => record.id}
          columns={columns}
          onChange={handleTableChanged}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.settingsManage.rewardsTotal,
            showTotal: () => <span>总共 {props.settingsManage.rewardsTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          scroll={{ x: 'max-content' }}
          dataSource={props.settingsManage.rewardsList}
        />
      </Card>
      <Modal
        title="添加奖励"
        width={600}
        visible={props.settingsManage.modalVisibility === 'rewards'}
        onCancel={eventHandlers.handleRewardsCancel}
        onOk={eventHandlers.handleRewardsSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="任务名称">
            {getFieldDecorator('editingTask', {
              rules: [{ required: true, message: '请输入任务名称'}],
            })(<Input />)}
          </Form.Item>
          <Form.Item label="硬币数量">
            {getFieldDecorator('editingCoinCount', {
              rules: [{ required: true, message: '请输入硬币数量'}],
            })(<InputNumber />)}
          </Form.Item>
          <Form.Item label="奖励间隔">
            {getFieldDecorator('editingRewardsInterval', {
              rules: [{ validator: checkRewardInterval }]
            })(
              <NumberWithUnit unitOptions={unitOptions} />
            )}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(RewardsSettings);
