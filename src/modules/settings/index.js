import React from "react";
import SettingsPage from "./SettingsPage";
import model from "./model";

export default {
  component: SettingsPage, model,
};
