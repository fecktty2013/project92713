import React, { useEffect, useState } from "react";
import classNames from "classnames";
import moment from "moment";
import { Breadcrumb, Icon, Button, Row, Col, Radio, DatePicker, Tabs, List, Card } from "antd";
import echarts from "echarts";

import style from "./style.scss";
import connectWithAuth from "../../core/connectWithAuth";

const OverViewPage = (props) => {

  const chartDiv = React.useRef(null);
  const videoChart = React.useRef(null);
  const photoChart = React.useRef(null);
  const audioChart = React.useRef(null);

  const [statType, setStateType] = useState("day");
  const [statDateRange, setStatDateRange] = useState();

  const [mediaStatType, setMediaStatType] = useState("day");
  const [mediaStatDateRange, setMediaStatDateRange] = useState();

  const [statSummaryInfo, setStatSummaryInfo] = useState({});
  const [entityStat, setEntityStat] = useState({});
  const [memberState, setMemberState] = useState({});
  const [noticeState, setNoticeState] = useState({});

  const queryStat = async () => {
    const summary = await props.dispatch({ type: 'overview/fetchSummaryInfo' });
    setStatSummaryInfo(summary.uv_stat || {});
    setEntityStat(summary.entity_stat) || {};
    setMemberState(summary.member_stat);
    setNoticeState(summary.notice_stat);
  };

  const queryInteractive = async () => {
    const summary = await props.dispatch({
      type: 'overview/fetchSummaryInteraction',
      payload: {
        dateRange: statDateRange || [],
        statType,
      }
    });
    drawStatChart(summary);
  };

  const importInteractive = async () => {
    const summary = await props.dispatch({
      type: 'overview/exportSummaryInteraction',
      payload: {
        dateRange: statDateRange || [],
        statType,
      }
    });
    window.open(summary.url, '_blank');
  };

  const queryMediaStat = async () => {
    const result = await props.dispatch({
      type: 'overview/fetchMediaStat',
      payload: {
        dateRange: mediaStatDateRange || [],
        statType: mediaStatType,
      }
    });
    drawVideoChart(result.media_stat);
    drawAudioChart(result.media_stat);
    drawPhotoChart(result.media_stat);
  };

  const handleStatTypeChanged = (e) => {
    setStateType(e.target.value);
  };

  const handleStatDateRangeChanged = (date, dateString) => {
    setStatDateRange(date);
  };

  const handleMediaStatTypeChanged = (e) => {
    setMediaStatType(e.target.value);
  };

  const handleMediaStatDateRangeChanged = (date, dateString) => {
    setMediaStatDateRange(date);
  };

  const getStatOption = (data) => {
    const options = {
      // color: ['blue', 'green'],
      title: {
        text: '实时统计',
        textStyle: {
          fontSize: 14,
          fontWeight: 500,
        },
        padding: [10, 20],
      },
      tooltip: {
        trigger: 'axis',
      },
      legend: {
        right: 40,
        top: 28,
        data: ['活跃用户', '评论次数', '注册用户数', '点赞次数', '用户充值', '用户消费', '用户收益'],
      },
      grid: {
        left: '40',
        right: '20',
        bottom: '40',
        containLabel: true
      },
      xAxis: {
        type: 'time',
        boundaryGap: false,
        splitLine: {
          show: false,
          interval: 'auto',
        },
        axisLine: {
          lineStyle: {
            color: '#ccc',
          },
        },
        axisLabel: {
          margin: 12,
          color: '#333',
        },
      },
      yAxis: {
        type: 'value',
        offset: 20,
        nameTextStyle: {
          align: 'left',
        },
        splitLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
          }
        },
        axisLine: {
          show: false,
        },
        axisTick: {
          show: false,
        },
      },
      dataZoom: [{
        type: 'inside',
        start: 0,
        end: 100,
      }, {
        start: 0,
        end: 100,
        handleIcon: 'M-292,322.2c-3.2,0-6.4-0.6-9.3-1.9c-2.9-1.2-5.4-2.9-7.6-5.1s-3.9-4.8-5.1-7.6c-1.3-3-1.9-6.1-1.9-9.3c0-3.2,0.6-6.4,1.9-9.3c1.2-2.9,2.9-5.4,5.1-7.6s4.8-3.9,7.6-5.1c3-1.3,6.1-1.9,9.3-1.9c3.2,0,6.4,0.6,9.3,1.9c2.9,1.2,5.4,2.9,7.6,5.1s3.9,4.8,5.1,7.6c1.3,3,1.9,6.1,1.9,9.3c0,3.2-0.6,6.4-1.9,9.3c-1.2,2.9-2.9,5.4-5.1,7.6s-4.8,3.9-7.6,5.1C-285.6,321.5-288.8,322.2-292,322.2z',
        handleSize: '80%',
        handleStyle: {
          color: '#fff',
          shadowBlur: 3,
          shadowColor: 'rgba(0, 0, 0, 0.6)',
          shadowOffsetX: 2,
          shadowOffsetY: 2
        }
      }],
      series: [
        {
          name: '活跃用户',
          type: 'line',
          data: (data?.active || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
          // data: [
          //   [new Date("2017-01-15"), '700'],+
          // ],
        }, {
          name: '评论次数',
          type: 'line',
          data: (data?.comment || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
        }, {
          name: '注册用户数',
          type: 'line',
          data: (data?.register || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
          // data: [
          //   [new Date("2017-01-13"), '800'],
          //   [new Date("2017-01-18"), '300'],
          // ],
        },
        {
          name: '点赞次数',
          type: 'line',
          data: (data?.thumb_up || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
        },
        {
          name: '用户充值',
          type: 'line',
          data: (data?.charge || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
        },
        {
          name: '用户消费',
          type: 'line',
          data: (data?.consume || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
        },
        {
          name: '用户收益',
          type: 'line',
          data: (data?.profit || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
        },
      ]
    };
    return options;
  };

  const getMediaChartOption = (name, data) => {
    const options = {
      color: ['#1890ff'],
      title: {
        text: '个数统计',
        textStyle: {
          fontSize: 14,
          fontWeight: 500,
        },
        padding: [10, 20],
      },
      tooltip: {
        trigger: 'axis',
      },
      grid: {
        left: '40',
        right: '20',
        bottom: '40',
        containLabel: true
      },
      xAxis: {
        type: 'time',
        // boundaryGap: true,
        // data: ['10月', '11月', '12月', '1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月'],
        splitLine: {
          show: false,
          interval: 'auto',
        },
        axisLine: {
          lineStyle: {
            color: '#ccc',
          },
        },
        axisTick: {
          alignWithLabel: true
        },
        axisLabel: {
          margin: 12,
          color: '#333',
        },
      },
      yAxis: {
        type: 'value',
        offset: 20,
        nameTextStyle: {
          align: 'left',
        },
        splitLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
          }
        },
        minInterval: 1,
        axisLine: {
          show: false,
        },
        axisTick: {
          show: false,
        },
      },
      series: [
        {
          name: name,
          type: 'bar',
          barWidth: '40%',
          data: (data || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
        }
      ],
    };
    return options;
  };

  const drawStatChart = (data) => {
    let option = getStatOption(data);
    if (chartDiv.current) {
      let lineChart = echarts.init(chartDiv.current);
      lineChart.setOption(option);
    }
    
  };

  const drawVideoChart = (data) => {
    let option = getMediaChartOption('视频个数', data?.video);
    let videoBarChart = echarts.init(videoChart.current);
    videoBarChart.setOption(option);
  };

  const drawAudioChart = (data) => {
    let option = getMediaChartOption('音频个数', data?.audio);
    let audioBarChart = echarts.init(audioChart.current);
    audioBarChart.setOption(option);
  };

  const drawPhotoChart = (data) => {
    let option = getMediaChartOption('相册个数', data?.photo_album);
    let photoBarChart = echarts.init(photoChart.current);
    photoBarChart.setOption(option);
  };

  useEffect(() => { queryStat(); }, []);

  useEffect(() => { queryInteractive(); }, [statType, statDateRange]);

  useEffect(() => { queryMediaStat(); }, [mediaStatType, mediaStatDateRange]);

  let contentSummary = [
    { category: '正常视频', total: entityStat.video_album_normal || 0 },
    { category: '冻结视频', total: entityStat.video_album_frozen || 0 },
    { category: '正常相册', total: entityStat.photo_album_normal || 0 },
    { category: '冻结相册', total: entityStat.photo_album_frozen || 0 },
    { category: '正常音频', total: entityStat.audio_album_normal || 0 },
    { category: '冻结音频', total: entityStat.audio_album_frozen || 0 },
    { category: '评论', total: entityStat.comment_verified + '/' + entityStat.comment_total },
  ];

  let memberSummary = [
    { category: '会员总数', total: memberState.member_total || 0 },
    { category: '冻结会员', total: memberState.member_frozen || 0 },
  ];

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>总览</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <div className={classNames('appRow', style.topRow)}>
          <div className={classNames('appCol', style.leftNav)}>
            <Icon style={{ fontSize: 10 }} type="left" />
          </div>
          <div className={classNames('appCol', style.chartCategory)}>
            <span>注册用户数</span>
            <span>{statSummaryInfo.register_num || 0}</span>
            <span>人</span>
          </div>
          <div className={classNames('appCol', style.chartCategory)}>
            <span>活跃用户</span>
            <span>{statSummaryInfo.active_num || 0}</span>
            <span>人</span>
          </div>
          <div className={classNames('appCol', style.chartCategory)}>
            <span>点赞次数</span>
            <span>{statSummaryInfo.thumb_up_num || 0}</span>
            <span>次</span>
          </div>
          <div className={classNames('appCol', style.chartCategory)}>
            <span>评论次数</span>
            <span>{statSummaryInfo.comment_num || 0}</span>
            <span>次</span>
          </div>
          <div className={classNames('appCol', style.chartCategory)}>
            <span>用户充值</span>
            <span>{statSummaryInfo.charge_num || 0}</span>
            <span>元</span>
          </div>
          <div className={classNames('appCol', style.chartCategory)}>
            <span>用户消费</span>
            <span>{statSummaryInfo.consume_num || 0}</span>
            <span>元</span>
          </div>
          <div className={classNames('appCol', style.chartCategory)}>
            <span>用户收益</span>
            <span>{statSummaryInfo.profit_num || 0}</span>
            <span>元</span>
          </div>
          <div className={classNames('appCol', style.rightNav)}>
            <Icon style={{ fontSize: 10 }} type="right" />
          </div>
        </div>
        <div className={style.statPanel}>
          <Row gutter={8} type="flex" className={style.statAction}>
            <Col>
              <Radio.Group onChange={handleStatTypeChanged} value={statType}>
                <Radio.Button value="day">今日</Radio.Button>
                <Radio.Button value="week">本周</Radio.Button>
                <Radio.Button value="month">本月</Radio.Button>
                <Radio.Button value="year">全年</Radio.Button>
                <Radio.Button value="all">所有</Radio.Button>
              </Radio.Group>
            </Col>
            <Col>
              <DatePicker.RangePicker onChange={handleStatDateRangeChanged} value={statDateRange} style={{ width: 240 }} />
            </Col>
            <Col>
              <Button onClick={importInteractive}>导出报表</Button>
            </Col>
          </Row>
          <div className={style.statChartContainer} ref={chartDiv} />
        </div>
        <div className={style.contentPanel}>
          <Row gutter={16} style={{ height: '100%' }}>
            <Col span={16}>
              <Tabs size="large" tabBarGutter={16}
                tabBarExtraContent={
                  <React.Fragment>
                    <Radio.Group onChange={handleMediaStatTypeChanged} value={mediaStatType}>
                      <Radio.Button value="day">今日</Radio.Button>
                      <Radio.Button value="week">本周</Radio.Button>
                      <Radio.Button value="month">本月</Radio.Button>
                      <Radio.Button value="year">全年</Radio.Button>
                      <Radio.Button value="all">所有</Radio.Button>
                    </Radio.Group>
                    <DatePicker.RangePicker onChange={handleMediaStatDateRangeChanged} value={mediaStatDateRange} style={{ width: 240 }} />
                  </React.Fragment>
                }>
                <Tabs.TabPane forceRender tab="视频" key="video">
                  <div ref={videoChart} className={style.barChartContainer} />
                </Tabs.TabPane>
                <Tabs.TabPane forceRender tab="相册" key="album">
                  <div ref={photoChart} className={style.barChartContainer} />
                </Tabs.TabPane>
                <Tabs.TabPane forceRender tab="音频" key="audio">
                  <div ref={audioChart} className={style.barChartContainer} />
                </Tabs.TabPane>
              </Tabs>
            </Col>
            <Col span={4}>
              <List size="small"
                className={style.summary}
                header={<div className={style.summaryHeader}>总计</div>}
                bordered={false}
                split={false}
                dataSource={contentSummary}
                renderItem={(item, idx) => (
                  <List.Item className={style.listItem}>
                    <span className={idx <= 2 ? style.starred : ''}>{idx + 1}</span>
                    <span>{item.category}</span>
                    <span>{item.total}</span>
                  </List.Item>
                )}
              />
            </Col>
            <Col span={4}>
              <List size="small"
                className={style.summary}
                header={<div className={style.summaryHeader}>会员总计</div>}
                bordered={false}
                split={false}
                dataSource={memberSummary}
                renderItem={(item, idx) => (
                  <List.Item className={style.listItem}>
                    <span className={idx < 2 ? style.starred : ''}>{idx + 1}</span>
                    <span>{item.category}</span>
                    <span>{item.total}</span>
                  </List.Item>
                )}
              />
            </Col>
          </Row>
        </div>
        <div className={style.notificationPanel}>
          <Card title="通知中心" size="default" bordered={false}>
            <p>有 <span className={style.notifyCount}>{noticeState.new_feedback}</span> 个新的反馈需要进行处理!</p>
            <p>添加了 <span className={style.notifyCount}>{noticeState.new_comment}</span> 条评论需要对其进行审核!</p>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default connectWithAuth(({ overview }) => ({ overview }))(OverViewPage);
