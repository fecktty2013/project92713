import $http from "../../core/http";

export function fetchSummaryInfo() {
  return $http.get("/summary/info");
}

export function fetchInteractiveInfo(payload) {
  return $http.get("/summary/interaction", {
    params: payload
  });
}

export function fetchMediaStat(payload) {
  return $http.get("/summary/media_stat", {
    params: payload,
  });
}
