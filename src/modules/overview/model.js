import * as service from './service';

const initialState = {
};

export default {
  namespace: 'overview',

  state: initialState,

  subscriptions: {},

  effects: {
    *fetchSummaryInfo({ payload }, { call, put }) {
      const httpResult = yield call(service.fetchSummaryInfo);
      return httpResult.data;
    },
    *fetchSummaryInteraction({ payload }, { call, put }) {
      const httpResult = yield call(service.fetchInteractiveInfo, {
        begin: payload.dateRange && payload.dateRange[0] && payload.dateRange[0].format('YYYY-MM-DD'),
        end: payload.dateRange && payload.dateRange[1] && payload.dateRange[1].format('YYYY-MM-DD'),
        to_export: 0,
        stat_type: (payload.dateRange && payload.dateRange[0] && payload.dateRange[1]) ? undefined : (payload.statType || 'all'),
      });
      return httpResult.data;
    },
    *exportSummaryInteraction({ payload }, { call, put }) {
      const httpResult = yield call(service.fetchInteractiveInfo, {
        begin: payload.dateRange && payload.dateRange[0] && payload.dateRange[0].format('YYYY-MM-DD'),
        end: payload.dateRange && payload.dateRange[1] && payload.dateRange[1].format('YYYY-MM-DD'),
        to_export: 1,
        stat_type: (payload.dateRange && payload.dateRange[0] && payload.dateRange[1]) ? undefined : (payload.statType || 'all'),
      });
      return httpResult.data;
    },
    *fetchMediaStat({ payload }, { call, put }) {
      const httpResult = yield call(service.fetchMediaStat, {
        begin: payload.dateRange && payload.dateRange[0] && payload.dateRange[0].format('YYYY-MM-DD'),
        end: payload.dateRange && payload.dateRange[1] && payload.dateRange[1].format('YYYY-MM-DD'),
        stat_type: (payload.dateRange && payload.dateRange[0] && payload.dateRange[1]) ? undefined : (payload.statType || 'all'),
      });
      return httpResult.data;
    }
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    }
  }
}

