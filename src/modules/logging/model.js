import * as service from './service';

const initialState = {
  logList: [],
  logTotal: 0,
};

export default {
  namespace: 'logging',

  state: initialState,

  subscriptions: {},

  effects: {
    *fetchLogList({ payload: { pagination } }, { call, put }) {
      const httpResult = yield call(service.fetchLogList, {
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const logList = httpResult.data.log_list.map((l, idx) => ({
        sn: idx + 1,
        id: l.log_id,
        userName: l.username,
        opType: l.module,
        desc: l.brief,
        opIp: l.remote_ip,
        opTime: l.create_time,
      }));
      const logTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { logList, logTotal } });
    },
    *getLogDetail({ payload }, { call, put }) {
      const httpResult = yield call(service.getLogDetail, {
        log_id: payload.id,
      });
      return httpResult.data.detail;
    }
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    }
  }
}

