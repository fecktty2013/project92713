import React, { useEffect } from "react";
import connectWithAuth from "../../core/connectWithAuth";
import { Breadcrumb, Card, Button, Icon, Modal } from "antd";
import LoggingTable from "./LoggingTable";

import style from "./style.scss";

const LoggingPage = (props) => {

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>日志管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <LoggingTable {...props} />
      </div>
    </div>
  );
};

export default connectWithAuth(({ logging }) => ({ logging }))(LoggingPage);
// export default LoggingPage;
