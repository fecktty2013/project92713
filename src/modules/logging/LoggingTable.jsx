import React, { useEffect, useState } from "react"
import { Card, Button, Icon, Form, Table, Modal } from "antd";
import style from "./style.scss";

const LoggingTable = (props) => {

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleTableChange = (pagination) => {
    console.log('table changed: ', pagination);
    setPagination(pagination);
  };

  const handleQuery = () => {
    props.dispatch({
      type: 'logging/fetchLogList',
      payload: { pagination },
    });
  };

  const viewDetail = async (record) => {
    const detail = await props.dispatch({
      type: 'logging/getLogDetail',
      payload: record,
    });
    Modal.info({
      title: '日志详情',
      content: (
        <div>
          <p>{detail}</p>
        </div>
      ),
      onOk() {},
    });
  };

  const logColumns = [
    {
      title: '序号',
      render: (text, record) => {
        return <span key={record.id}>{pagination.pageSize * (pagination.current - 1) + record.sn}</span>
      }
    },
    { title: '用户名', dataIndex: 'userName' },
    { title: '操作类型', dataIndex: 'opType' },
    { title: '描述', dataIndex: 'desc' },
    { title: '操作ip', dataIndex: 'opIp' },
    { title: '操作时间', dataIndex: 'opTime' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type="link" className={style.rowBtn} onClick={() => viewDetail(record)}>查看详情</Button>
        </span>
      )
    }
  ];

  useEffect(handleQuery, [pagination]);

  return <Card title="管理日志"
    bordered={false}
    headStyle={{ padding: '0 24px 0 0' }}
    style={{ width: '100%' }}
    bodyStyle={{ paddingBottom: 0, paddingTop: 12 }}
  >
    <Table
      size="middle"
      columns={logColumns}
      dataSource={props.logging.logList}
      onChange={handleTableChange}
      rowKey={record => record.id}
      pagination={{
        ...pagination,
        showSizeChanger: true,
        showQuickJumper: true,
        total: props.logging.logTotal,
        showTotal: () => <span>总共 {props.logging.logTotal} 条记录</span>,
        pageSizeOptions: ['5', '10', '20'],
      }}
    />
  </Card>
};

export default LoggingTable;
