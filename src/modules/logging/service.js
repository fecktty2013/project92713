import $http from "../../core/http";

export function fetchLogList(payload) {
  return $http.get("/operation_log/list", {
    params: payload
  });
}

export function getLogDetail(payload) {
  return $http.get("/operation_log/detail", {
    params: payload
  });
}
