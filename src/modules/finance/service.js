import $http, { APP_BASE_URL } from "../../core/http";

export function fetchMoneytypeList() {
  return $http.get("/fund/type/list");
}

export function fetchMoneyflowList(payload) {
  return $http.get("/fund/stream/list", {
    params: payload,
  });
}
