import { routerRedux } from 'dva/router';
import * as service from './service';

const initialState = {
  typeList: [],
  flowList: [],
  flowTotal: 0,
};

export default {
  namespace: 'financeManage',

  state: initialState,

  subscriptions: {
  },

  effects: {
    *fetchMoneyTypeList({ payload }, { call, put }) {
      const httpResult = yield call(service.fetchMoneytypeList);
      yield put({ type: 'stateChanged', payload: { typeList: httpResult.data.type_list } });
    },
    *fetchFlowList({ form, pagination } = {}, { put, call, select }) {
      console.log('money list', form);
      const httpRequest = {
        user_id: form.id,
        order_type: form.category && form.category[0],
        sub_type: form.category && form.category[1],
        begin: form.timestamp && form.timestamp[0] && form.timestamp[0].format('YYYY-MM-DD HH:mm:ss'),
        end: form.timestamp && form.timestamp[1] && form.timestamp[1].format('YYYY-MM-DD HH:mm:ss'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      };
      const httpResult = yield call(service.fetchMoneyflowList, httpRequest);
      const flowList = httpResult.data.stream_list.map((item, idx) => ({
        id: item.user_id + item.trade_time,
        sn: idx + 1,
        userId: item.user_id,
        userName: item.username,
        timestamp: item.trade_time,
        action: item.order_type_zh,
        coinCount: item.coin_num,
        detail: item.sub_type_zh,
        flowType: item.direction,
        flowSection: item.section,
      }));
      const flowTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { flowList, flowTotal } });
    },
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}