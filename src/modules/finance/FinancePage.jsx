import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, Breadcrumb, Cascader } from "antd";
import connectWithAuth from "../../core/connectWithAuth";

import style from "./style.scss";

const FinancePage = (props) => {

  const { getFieldDecorator, validateFields, getFieldsValue, setFieldsValue } = props.form;

  const { typeList, flowList } = props.financeManage;

  const typeListSouce = typeList.map((t, idx) => {
    const item = {
      value: t.order_type,
      label: t.order_type_zh
    };
    item.children = t.sub_order_list.map((st) => ({
      value: st.sub_order_type,
      label: st.sub_order_type_zh,
    }));
    return item;
  });

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleTypeQuery = () => {
    props.dispatch({
      type: 'financeManage/fetchMoneyTypeList'
    });
  };

  const handleQuery = (p) => {
    if (!p) {
      p = pagination;
    }
    validateFields((err, values) => {
      props.dispatch({
        type: 'financeManage/fetchFlowList',
        form: values,
        pagination: p,
      });
    });
  };

  const handleTableChange = (pagination) => {
    console.log('table changed: ', pagination);
    setPagination(pagination);
    handleQuery(pagination);
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const eventHandlers = {
    handlePrevDay() {
      const values = getFieldsValue();
      const timestamp = values.timestamp;
      if (!timestamp) return;
      const newTimeStamp = [
        timestamp[0].add(-1, 'days'),
        timestamp[1].add(-1, 'days')
      ];
      setFieldsValue({
        timestamp: newTimeStamp,
      });
      const defaultPagination = { current: 1, pageSize: 5 };
      setPagination(defaultPagination);
      props.dispatch({
        type: 'financeManage/fetchFlowList',
        form: {
          ...values,
          timestamp: newTimeStamp,
        },
        pagination: defaultPagination,
      });
    },
    handleNextDay() {
      const values = getFieldsValue();
      const timestamp = values.timestamp;
      if (!timestamp) return;
      const newTimeStamp = [
        timestamp[0].add(1, 'days'),
        timestamp[1].add(1, 'days')
      ];
      setFieldsValue({
        timestamp: newTimeStamp,
      });
      const defaultPagination = { current: 1, pageSize: 5 };
      setPagination(defaultPagination);
      props.dispatch({
        type: 'financeManage/fetchFlowList',
        form: {
          ...values,
          timestamp: newTimeStamp,
        },
        pagination: defaultPagination,
      });
    }
  };

  const columns = [
    { title: '序号', dataIndex: 'sn' },
    {
      title: '名称（ID）',
      render: (text, record) => (<span>{`${record.userName} (${record.userId})`}</span>)
    },
    { title: '时间', dataIndex: 'timestamp' },
    { title: '类型', dataIndex: 'action' },
    { title: '明细', dataIndex: 'detail' },
    { title: '硬币数量', dataIndex: 'coinCount' },
    { title: '方向', dataIndex: 'flowType' },
    { title: '涉及板块', dataIndex: 'flowSection' },
  ];

  useEffect(() => {
    handleTypeQuery();
    handleQuery(pagination);
  }, []);

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>资金管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <Card title="流水明细"
          bordered={false}
          headStyle={{ padding: '0 24px 0 0' }}
          style={{ width: '100%' }}
          bodyStyle={{ paddingBottom: 0, paddingTop: 0 }}
        >
          <Form layout="horizontal" style={{ marginBottom: 20 }}>
            <Row gutter={16} type="flex" align="bottom">
              <Col>
                <Form.Item label="id" className={style.formItem}>
                  {getFieldDecorator('id')(
                    <Input />
                  )}
                </Form.Item>
              </Col>
              <Col>
                <Form.Item label="类型" className={style.formItem}>
                  {getFieldDecorator('category')(
                    <Cascader options={typeListSouce} changeOnSelect />
                  )}
                </Form.Item>
              </Col>
              <Col>
                <Form.Item label="时间" className={style.formItem}>
                  {getFieldDecorator('timestamp')(
                    <DatePicker.RangePicker />
                  )}
                </Form.Item>
              </Col>
              <Col>
                <Button type="primary" className={style.actionBtn} onClick={() => handleQuery()}>搜索</Button>
                <Button type="primary" className={style.actionBtn} onClick={eventHandlers.handlePrevDay}>上一天</Button>
                <Button type="primary" className={style.actionBtn} onClick={eventHandlers.handleNextDay}>下一天</Button>
              </Col>
            </Row>
          </Form>
          <Table
            rowKey={record => record.id}
            columns={columns}
            scroll={{ x: 'max-content' }}
            dataSource={props.financeManage.flowList}
            pagination={{
              ...pagination,
              showSizeChanger: true,
              showQuickJumper: true,
              defaultPageSize: 5,
              total: props.financeManage.flowTotal,
              showTotal: () => <span>总共 {props.financeManage.flowTotal} 条记录</span>,
              pageSizeOptions: ['5', '10', '20'],
            }}
            onChange={handleTableChange}
          />
        </Card>
      </div>
    </div>
  )
};

export default connectWithAuth(({ financeManage }) => ({ financeManage }))(Form.create()(FinancePage));
