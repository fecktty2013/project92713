import React from "react";
import FinancePage from "./FinancePage";
import model from "./model";

export default {
  component: FinancePage, model,
};
