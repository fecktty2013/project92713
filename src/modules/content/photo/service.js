import $http from "../../../core/http";
import { MEDIA_TYPE, STATUS } from "../../../common/constants";

export function fetchPhotoList({ payload }) {
  return $http.get("/media/album/list", {
    params: {
      ...payload,
      media_type: MEDIA_TYPE.PHOTO,
    },
  });
}

export function savePhoto({ payload }) {
  return $http.post("/media/album", payload);
}

export function deletePhoto({ payload }) {
  return $http.post("/media/album/status", {
    ...payload,
    status: STATUS.DELETED,
  });
}

export function updatePhotoStatus({ payload }) {
  return $http.post("/media/album/status", payload);
}

export function importPhoto({ payload }) {
  var formData = new FormData();
  formData.append("file", payload.file);
  return $http.post("/media/album/photo_import", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    }
  });
}

export function getAlbumItems({ payload }) {
  return $http.get("/media/album/items", {
    params: {
      album_id: payload.id,
    }
  });
}

export function addPhotoItem({ payload }) {
  return $http.post("/media/album/photo", payload);
}

export function batchUpdatePhotoItem({ payload }) {
  return $http.post("/media/album/photo/batch", payload);
}

export function savePhotoLabel({ payload }) {
  return $http.post("/media/album/media_labels", payload);
}

export function fetchAlbumComments(payload) {
  return $http.get("/media/album/comment/list", {
    params: payload,
  });
}
