import React, { useEffect, useState } from "react";
import { Link } from "dva/router";
import connectWithAuth from "../../../core/connectWithAuth";
import PhotoEditPopup from "./PhotoEditPopup";
import { Breadcrumb, Button, Form, Row, Col, Input, Select, DatePicker, Table, Card, Icon, Modal, Popconfirm } from "antd";
import style from "./style.scss";
import { MEDIA_TYPE, STATUS } from "../../../common/constants";
import { BASE_URL } from "../../../core/http";
const uuid4 = require('uuid/v4');

const PhotoManagePage = (props) => {

  const fileInput = React.createRef();

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const [commentPagination, setCommentPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const [editingAlbumId, setEditingAlbumId] = useState(0);

  const { getFieldDecorator, resetFields, validateFields } = props.form;

  const resetEditingFields = () => {
    resetFields([
      "editTitle", "editPlays", "editFavorites", "editShares",
      "editDesc", "editLikes", "editDownloads", "editPublishDate",
      "editState", "editCategory", "editTags", "editCoverImg",
      "editPhotoCount", "editThumbnails", "editRealPhotos", "watchCoins", "downloadCoins",
    ]);
  };

  const handleTableChange = (pagination) => {
    setPagination(pagination);
  };

  const handleAlbumComments = (record, p) => {
    let id = 0;
    if (record) {
      setEditingAlbumId(record.id);
      id = record.id;
    } else {
      id = editingAlbumId;
    }
    props.dispatch({
      type: 'photoManage/fetchAlbumComments',
      form: { id },
      pagination: p || commentPagination,
    })
  };

  const handleCommentsTableChanged = (p) => {
    setCommentPagination(p);
    handleAlbumComments(null, p);
  };

  const queryCategory = () => {
    props.dispatch({
      type: 'app/fetchCategoryList',
      payload: {
        form: { mediaType: MEDIA_TYPE.PHOTO }
      }
    })
  };

  const queryTags = () => {
    props.dispatch({
      type: 'app/fetchTagList',
      payload: {
        form: { mediaType: MEDIA_TYPE.PHOTO }
      }
    })
  };

  const handleQuery = (e) => {
    props.form.validateFields((err, values) => {
      props.dispatch({
        type: 'photoManage/fetchPhotoList',
        payload: { form: values, pagination },
      })
    });
  };

  const handleDeleteRow = async (record) => {
    await props.dispatch({
      type: 'photoManage/deletePhoto',
      payload: record,
    });
    handleQuery();
  };

  const handleUpdateStatus = async (id, status) => {
    await props.dispatch({
      type: 'photoManage/updatePhotoStatus',
      payload: {
        id, status
      }
    });
    handleQuery();
  }

  const modalEvents = {
    newPhoto(e) {
      // reset form state
      resetEditingFields();
      props.dispatch({ type: 'photoManage/newPhotoStart' });
    },
    editPhoto(record) {
      // reset form state
      resetEditingFields();
      props.dispatch({ type: 'photoManage/editPhotoStart', payload: record });
      console.log('editing photo ', record);
    },
    showModal(e) {
      props.dispatch({ type: 'photoManage/setModalVisibility', payload: { modalVisibility: 'save' } });
    },
    hideModal(e) {
      props.dispatch({ type: 'photoManage/setModalVisibility', payload: { modalVisibility: '' } });
    },
    saveModal(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log('validation failed');
          return;
        }
        await props.dispatch({
          type: 'photoManage/savePhoto',
          payload: values,
        });
        props.dispatch({ type: 'photoManage/setModalVisibility', payload: { modalVisibility: '' } });
        handleQuery();
      });
    },
    closeModal(e) {

    }
  }

  const columns = [
    {
      title: '序号',
      render: (text, record) => {
        return <span key={record.id}>{pagination.pageSize * (pagination.current - 1) + record.sn}</span>
      }
    },
    {
      title: '标题',
      dataIndex: 'title',
    },
    {
      title: '发布时间',
      dataIndex: 'publishDate',
    },
    {
      title: '状态',
      dataIndex: 'state',
    },
    {
      title: '设置推荐',
      dataIndex: 'recommendLevel',
    },
    {
      title: '观看硬币数',
      dataIndex: 'watchCoins',
    },
    {
      title: '下载硬币数',
      dataIndex: 'downloadCoins',
    },
    {
      title: '盈利情况',
      dataIndex: 'earnings',
    },
    {
      title: '播放次数',
      dataIndex: 'plays',
    },
    {
      title: '收藏次数',
      dataIndex: 'favorites',
    },
    {
      title: '喜欢次数',
      dataIndex: 'likes',
    },
    {
      title: '评论次数',
      dataIndex: 'commentCount',
    },
    {
      title: '操作',
      render: (text, record) => {
        return (
          <span>
            <Button className={style.actionBtn} onClick={e => modalEvents.editPhoto(record)} type="link">编辑</Button>
            <Popconfirm title="确认删除吗？" onConfirm={() => handleDeleteRow(record)}>
              <a>删除</a>
            </Popconfirm>
            {
              record.stateVal == 1 ? 
                <Popconfirm title="确认冻结吗？" onConfirm={e => handleUpdateStatus(record.id, STATUS.FREEZED)}>
                  <Button className={style.actionBtn} type="link">冻结</Button>
                </Popconfirm> : undefined
            }
            {
              record.stateVal == 2 ? 
                <Popconfirm title="确认解冻吗？" onConfirm={e => handleUpdateStatus(record.id, STATUS.NORMAL)}>
                  <Button className={style.actionBtn} type="link">解冻</Button>
                </Popconfirm> : undefined
            }
            <Button className={style.actionBtn} onClick={e => handleAlbumComments(record)} type="link">查看评论</Button>
          </span>
        );
      },
    },
  ];

  const commentColumns = [
    {
      title: '评论内容',
      dataIndex: 'content',
    },
    {
      title: '评论用户',
      dataIndex: 'userName',
    },
    {
      title: '评论时间',
      dataIndex: 'publishTime',
    },
    {
      title: '评审状态',
      dataIndex: 'reviewStatus'
    }
  ];

  const uploadEvents = {
    handleUploadStart(e) {
      fileInput.current.click();
    },
    async handleUploadComplete(e) {
      console.log(Array.from(e.target.files));
      await props.dispatch({
        type: 'photoManage/importPhoto',
        payload: {
          file: e.target.files[0],
        }
      });
      Modal.success({
        title: '提示',
        content: '导入成功',
        okText: '确认',
        onOk: () => {
          handleQuery();
        }
      })
    },
  }

  useEffect(() => {
    handleQuery();
  }, [pagination]);

  useEffect(() => {
    queryCategory();
    queryTags();
  }, []);

  const isEditing = !!props.photoManage.editingId;

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>内容管理</Breadcrumb.Item>
          <Breadcrumb.Item>相册管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <input type="file"
        id="batchUploader"
        ref={fileInput}
        accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
        style={{ display: 'none' }}
        onChange={uploadEvents.handleUploadComplete}  onClick={(event) => { event.target.value = null; }} />
      <div className="pageBody">
        <Card title="相册列表"
          bordered={false}
          headStyle={{ padding: '0 24px 0 0' }}
          style={{ width: '100%' }}
          bodyStyle={{ paddingBottom: 0, paddingTop: 0 }}
          extra={
            <React.Fragment>
              <Button className={style.headBtn} type="dashed" onClick={modalEvents.newPhoto}>
                <Icon type="plus" /> 新增
              </Button>
              <Button className={style.headBtn} type="dashed" onClick={uploadEvents.handleUploadStart}>
                批量上传
              </Button>
              <a style={{ marginLeft: 16 }} href={`${BASE_URL}static/template/photo.xlsx?t=${uuid4()}`}>下载模板</a>
            </React.Fragment>
          }
        >
          <React.Fragment>
            <Form layout="horizontal">
              <Row type="flex" align="bottom" gutter={8}>
                <Col span={3}>
                  <Form.Item label="id" className={style.formItem}>
                    {getFieldDecorator('id')(
                      <Input className={style.input} placeholder="请输入id" />
                    )}
                  </Form.Item>
                </Col>
                <Col span={3}>
                  <Form.Item label="标题" className={style.formItem}>
                    {getFieldDecorator('title')(
                      <Input className={style.input} placeholder="请输入标题" />
                    )}
                  </Form.Item>
                </Col>
                <Col span={3}>
                  <Form.Item label="标签" className={style.formItem}>
                    {getFieldDecorator('tags')(
                      <Select placeholder="请选择标签">
                        {props.app.tagList.map(t => (
                          <Select.Option key={t.key} value={t.id}>{t.name}</Select.Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col span={3}>
                  <Form.Item label="分类" className={style.formItem}>
                    {getFieldDecorator('category')(
                      <Select placeholder="请选择分类">
                        {props.app.categoryList.map(c => (
                          <Select.Option key={c.key} value={c.key}>{c.categoryName}</Select.Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col span={3}>
                  <Form.Item label="状态" className={style.formItem}>
                    {getFieldDecorator('state')(
                      <Select placeholder="请选择状态">
                        <Select.Option value="all">所有</Select.Option>
                        <Select.Option value="1">正常</Select.Option>
                        <Select.Option value="2">冻结</Select.Option>
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col span={6}>
                  <Form.Item label="发布时间" className={style.formItem}>
                    {getFieldDecorator('publishDate')(
                      <DatePicker.RangePicker />
                    )}
                  </Form.Item>
                </Col>
                <Col span={3}>
                  <Button style={{ marginLeft: 12, marginBottom: 28 }} onClick={handleQuery} type="primary">查询</Button>
                </Col>
              </Row>
            </Form>
            <Table columns={columns} rowKey={record => record.sn}
              scroll={{ x: 'max-content' }}
              dataSource={props.photoManage.photoList}
              onChange={handleTableChange}
              pagination={{
                ...pagination,
                showSizeChanger: true,
                showQuickJumper: true,
                defaultPageSize: 5,
                total: props.photoManage.photoTotal,
                showTotal: () => <span>总共 {props.photoManage.photoTotal} 条记录</span>,
                pageSizeOptions: ['5', '10', '20'],
              }}
            />
            <Modal
              title={isEditing ? '相册编辑' : '新建相册'}
              width={968}
              footer={null}
              bodyStyle={{ marginLeft: -24 }}
              visible={props.photoManage.modalVisibility === 'save'}
              onCancel={modalEvents.hideModal}
              afterClose={modalEvents.closeModal}
            >
              <PhotoEditPopup {...props} eventHandlers={modalEvents} />
            </Modal>
            <Modal
              title="专辑评论列表"
              width={968}
              footer={null}
              visible={props.photoManage.modalVisibility === 'comments'}
              onCancel={modalEvents.hideModal}
              afterClose={modalEvents.closeModal}
            >
              <Table columns={commentColumns} rowKey={record => record.id}
                scroll={{ x: 'max-content' }}
                dataSource={props.photoManage.albumComments}
                onChange={handleCommentsTableChanged}
                pagination={{
                  ...commentPagination,
                  showSizeChanger: true,
                  showQuickJumper: true,
                  total: props.photoManage.albumCommentTotal,
                  showTotal: () => <span>总共 {props.photoManage.albumCommentTotal} 条记录</span>,
                  pageSizeOptions: ['5', '10', '20'],
                }}
              />
            </Modal>
          </React.Fragment>
        </Card>
      </div>
    </div>
  );
};

export default connectWithAuth(({ app, photoManage }) => ({ app, photoManage }))(Form.create()(PhotoManagePage));
