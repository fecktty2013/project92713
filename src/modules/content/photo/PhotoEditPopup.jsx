import React from "react";
import { Form, Row, Col, Input, DatePicker, Select, Button } from "antd";
import moment from 'moment';

const uuid4 = require('uuid/v4');

const PhotoEditPopup = (props) => {

  const { validateFields, getFieldDecorator, getFieldValue, setFieldsValue } = props.form;
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 },
    },
  };

  const { editingPhoto = {} } = props.photoManage;

  getFieldDecorator('defaultEpisodes', { initialValue: editingPhoto.editEpisodes || [] });
  const defaultEpisodes = getFieldValue('defaultEpisodes');

  const addEpisode = (e) => {
    const episodes = getFieldValue('defaultEpisodes');
    const nextEpisodes = episodes.concat({ id: uuid4() });
    setFieldsValue({
      defaultEpisodes: nextEpisodes,
    });
  };

  const removeEpisode = (idx) => {
    setFieldsValue({
      defaultEpisodes: defaultEpisodes.filter((e, i) => i !== idx),
    });
  }

  const save = (e) => {
    e.preventDefault();
    props.eventHandlers.saveModal(e);
  }

  const handleCategoryChanged = (value, e) => {
    const { setFieldsValue } = props.form;
    setFieldsValue({ editTags: [] });
  };

  const validateTagField = (rule, value, callback) => {
    const { getFieldValue } = props.form;
    if (!getFieldValue('editCategory')) {
      callback('请先选择分类');
      return;
    }
    if (!value || value.length === 0) {
      callback('请选择标签');
      return;
    }
    callback();
  };

  const filterTagList = () => {
    const { getFieldValue } = props.form;
    const selectedCategory = getFieldValue('editCategory');
    if (!selectedCategory)
      return [];
    return props.app.tagList.filter(t => t.categoryId == selectedCategory);
  };

  return (
    <Form {...formItemLayout}>
      <Row gutter={8}>
        <Col span={12}>
          <Form.Item label="标题">
            {getFieldDecorator('editTitle', { initialValue: editingPhoto.editTitle })(
              <Input placeholder="请输入标题" />
            )}
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label="播放">
            {getFieldDecorator('editPlays', { initialValue: editingPhoto.editPlays })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label="收藏">
            {getFieldDecorator('editFavorites', { initialValue: editingPhoto.editFavorites })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label="分享">
            {getFieldDecorator('editShares', { initialValue: editingPhoto.editShares })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="描述">
            {getFieldDecorator('editDesc', { initialValue: editingPhoto.editDesc })(
              <Input.TextArea style={{ lineHeight: 1.2 }} placeholder="请输入描述" rows={5} />
            )}
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label="点赞">
            {getFieldDecorator('editLikes', { initialValue: editingPhoto.editLikes })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label="下载">
            {getFieldDecorator('editDownloads', { initialValue: editingPhoto.editDownloads })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={24} />
        <Col span={12}>
          <Form.Item label="发布时间">
            {getFieldDecorator('editPublishDate', { initialValue: moment(editingPhoto.editPublishDate) })(
              <DatePicker style={{ width: '75%' }} />
            )}
          </Form.Item>
        </Col>
        <Col span={24} />
        <Col span={12}>
          <Form.Item label="相册状态">
            {getFieldDecorator('editState', { initialValue: editingPhoto.editState?.toString() })(
              <Select style={{ width: '50%' }}>
                <Select.Option value="1">正常</Select.Option>
                <Select.Option value="2">冻结</Select.Option>
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="分类">
            {getFieldDecorator('editCategory', {
              initialValue: editingPhoto.editCategory,
              rules: [{ required: true, message: '请选择分类' }],
            })(
              <Select placeholder="请选择分类" onChange={handleCategoryChanged}>
                {props.app.categoryList.map(c => (
                  <Select.Option key={c.key} value={c.key}>{c.categoryName}</Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="标签">
            {getFieldDecorator('editTags', {
              initialValue: editingPhoto.editCategoryList?.reduce((a, c) => a.concat(c.label_list.map(l => l.label_id)), []),
              rules: [{ validator: validateTagField }]
            })(
              <Select mode="multiple">
                {filterTagList().map(t => (
                  <Select.Option key={t.key} value={t.id}>{t.name}</Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col span={24} />
        <Col span={12}>
          <Form.Item label="封面图">
            {getFieldDecorator('editCoverImg', { initialValue: editingPhoto.editCoverImg })(
              <Input style={{ width: '75%' }} />
            )}
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="照片张数">
            {getFieldDecorator('editPhotoCount', { initialValue: editingPhoto.editPhotoCount })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={24} style={{ display: 'block' }} />
        <Col span={12}>
          <Form.Item label="缩略图地址">
            {getFieldDecorator('editThumbnails', { initialValue: editingPhoto.editThumbnails })(
              <Input.TextArea style={{ lineHeight: 1.2 }} placeholder="请输入缩略图路径（每行分开）" rows={10} />
            )}
          </Form.Item>
        </Col>
        <Col span={24} style={{ display: 'block' }} />
        <Col span={12}>
          <Form.Item label="大图地址">
            {getFieldDecorator('editRealPhotos', { initialValue: editingPhoto.editRealPhotos })(
              <Input.TextArea style={{ lineHeight: 1.2 }} placeholder="请输入大图路径（每行分开）" rows={10} />
            )}
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="观看硬币">
            {getFieldDecorator(`watchCoins`, { initialValue: editingPhoto.watchCoins })(
              <Input type="number" placeholder="观看硬币" />
            )}
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="下载硬币">
            {getFieldDecorator(`downloadCoins`, { initialValue: editingPhoto.downloadCoins })(
              <Input type="number" placeholder="下载硬币" />
            )}
          </Form.Item>
        </Col>
        <Col span={24} offset={3} style={{ display: 'block' }}>
          <Button type="primary" size="large" style={{ width: 240 }} onClick={save}>保存</Button>
        </Col>
      </Row>
    </Form>
  )

}

export default PhotoEditPopup;
