import { routerRedux } from 'dva/router';
import * as service from './service';
import { MEDIA_TYPE } from '../../../common/constants';

const initialState = {
  photoList: [],
  photoTotal: 0,

  modalVisibility: false,

  editingId: 0,
  editingPhoto: {},

  albumComments: [],
  albumCommentTotal: 0,
};

export default {
  namespace: 'photoManage',

  state: initialState,

  subscriptions: {
  },

  effects: {
    *fetchPhotoList({ payload: { form = {}, pagination } }, { put, call }) {
      console.log('query photo list ', form, pagination);
      const result = yield call(service.fetchPhotoList, {
        payload: {
          album_id: !form.id ? undefined : form.id,
          title: form.title,
          category_id: form.category,
          label_id: form.tags,
          status: form.state === 'all' ? undefined : form.state,
          begin: form.publishDate && form.publishDate[0] && form.publishDate[0].format('YYYY-MM-DD'),
          end: form.publishDate && form.publishDate[1] && form.publishDate[1].format('YYYY-MM-DD'),
          page_no: pagination.current - 1,
          page_size: pagination.pageSize,
        }
      });
      const data = result.data.album_list.map((resDataItem, idx) => ({
        sn: idx + 1,
        id: resDataItem.album_id,
        title: resDataItem.title,
        description: resDataItem.description,
        publishDate: resDataItem.publish_time,
        stateVal: resDataItem.status,
        state: resDataItem.status_txt,
        recommendLevel: resDataItem.weight,
        watchCoins: resDataItem.view_price,
        downloads: resDataItem.download_num,
        downloadCoins: resDataItem.download_price,
        categoryList: resDataItem.category_list,
        earnings: resDataItem.profit_coin,
        plays: resDataItem.view_num,
        count: resDataItem.count,
        favorites: resDataItem.collect_num,
        likes: resDataItem.thumbsup_num,
        shares: resDataItem.share_num,
        commentCount: resDataItem.comment_count,
        coverUrl: resDataItem.cover_url,
        duration: resDataItem.duration,
      }));
      yield put({ type: 'stateChanged', payload: { photoList: data, photoTotal: result.data.total_count } });
    },
    *newPhotoStart(action, { put }) {
      // reset editing photo
      yield put({ type: 'stateChanged', payload: { editingPhoto: {}, editingId: 0 } });
      yield put({ type: 'setModalVisibility', payload: { modalVisibility: 'save' } });
    },
    *editPhotoStart({ payload }, { put }) {
      console.log('edit photo start', payload);
      yield put({ type: 'getAlbumItems', payload });
      const editingPhoto = {
        editTitle: payload.title,
        editPlays: payload.plays,
        editFavorites: payload.favorites,
        editShares: payload.shares,
        editDesc: payload.description,
        editLikes: payload.likes,
        editCategoryList: payload.categoryList,
        editDownloads: payload.downloads,
        editPublishDate: payload.publishDate,
        editState: payload.stateVal,
        editStateTxt: payload.state,
        editCategory: payload.categoryList.length > 0 ? payload.categoryList[0].category_id : undefined,
        editTags: payload.tags,
        editCoverImg: payload.coverUrl,
        editCoverLength: payload.duration,
        editPhotoCount: payload.count,
        watchCoins: payload.watchCoins,
        downloadCoins: payload.downloadCoins,
        editEpisodes: [],
      };
      yield put({ type: 'stateChanged', payload: { editingPhoto, editingId: payload.id, modalVisibility: 'save' } });
    },
    *savePhoto({ payload }, { select, call, put, all }) {
      const photoState = yield select(state => state.photoManage);
      const editingId = photoState.editingId > 0 ? photoState.editingId : undefined;
      console.log('photo state', photoState);
      console.log('save photo', payload);
      let httpPayload = {
        album_id: editingId,
        media_type: MEDIA_TYPE.PHOTO,
        title: payload.editTitle,
        description: payload.editDesc,
        publish_time: payload.editPublishDate && payload.editPublishDate.format('YYYY-MM-DD'),
        cover_url: payload.editCoverImg,
        view_num: parseInt(payload.editPlays, 10) || 0,
        download_num: parseInt(payload.editDownloads, 10) || 0,
        share_num: parseInt(payload.editShares, 10) || 0,
        collect_num: parseInt(payload.editFavorites, 10) || 0,
        thumbsup_num: parseInt(payload.editLikes, 10) || 0,
        status: parseInt(payload.editState, 10) || 0,
        view_price: parseInt(payload.watchCoins, 10) || 0,
        download_price: parseInt(payload.downloadCoins, 10) || 0,
      };
      console.log('save photo http payload', httpPayload);
      const saveResult = yield call(service.savePhoto, { payload: httpPayload });
      const albumId = saveResult.data.album_id;

      const thumbnails = payload.editThumbnails.split('\n');
      const images = payload.editRealPhotos.split('\n');
      const addPhotos = [];
      for (var i=0; i<thumbnails.length&&i<images.length; i++) {
        addPhotos.push({
          image_url: images[i],
          thumbnail_url: thumbnails[i]
        });
      }
      yield call(service.batchUpdatePhotoItem, {
        payload: {
          album_id: albumId,
          photo_list: addPhotos,
        }
      })

      // save labels
      if (payload.editTags && payload.editTags.length > 0) {
        yield call(service.savePhotoLabel, {
          payload: {
            album_id: albumId,
            category_id: payload.editCategory,
            label_ids: payload.editTags,
          }
        });
      }

    },
    *deletePhoto({ payload }, { call, put }) {
      console.log('delete photo', payload);
      const httpPayload = {
        album_id: payload.id,
      };
      yield call(service.deletePhoto, { payload: httpPayload });
    },
    *updatePhotoStatus({ payload }, { call, put }) {
      console.log('update photo', payload);
      const httpPayload = {
        album_id: payload.id,
        status: payload.status
      };
      yield call(service.updatePhotoStatus, { payload: httpPayload });
    },
    *importPhoto({ payload }, { call, put }) {
      console.log('import photo', payload);
      yield call(service.importPhoto, {
        payload: {
          file: payload.file,
        }
      });
    },
    *fetchAlbumComments({ form, pagination }, { call, put }) {
      const httpResult = yield call(service.fetchAlbumComments, {
        album_id: form.id,
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const albumComments = httpResult.data.comment_list.map((itm, idx) => ({
        id: itm.comment_id,
        content: itm.content,
        userId: itm.user_id,
        userName: itm.username,
        publishTime: itm.publish_time,
        reviewStatus: itm.result_zh,
      }));
      const albumCommentTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { albumComments, modalVisibility: 'comments', albumCommentTotal }});
    },
    *getAlbumItems({ payload }, { select, call, put }) {
      const state = yield select(stt => stt.photoManage);
      const editingPhoto = state.editingPhoto;
      const result = yield call(service.getAlbumItems, { payload });
      const mappedEpisodes = result.data.item_list;
      const editThumbnails = mappedEpisodes.map(ep => ep.thumbnail_url).join('\n');
      const editRealPhotos = mappedEpisodes.map(ep => ep.image_url).join('\n');
      yield put({
        type: 'stateChanged',
        payload: {
          editingPhoto: {
            ...editingPhoto,
            editThumbnails,
            editRealPhotos
          }
        }
      });
    },
    *setModalVisibility({ payload: { modalVisibility } }, { put }) {
      yield put({ type: 'stateChanged', payload: { modalVisibility } });
    }
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}