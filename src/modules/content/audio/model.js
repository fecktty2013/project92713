import * as service from './service';
import { MEDIA_TYPE } from '../../../common/constants';
import cloneDeep from 'lodash/cloneDeep';

const initialState = {
  audioList: [],
  audioTotal: 0,

  modalVisibility: false,

  editingId: 0,
  editingAudio: {},
  originalEpisodes: [],

  albumComments: [],
  albumCommentTotal: 0,
};

export default {
  namespace: 'audioManage',

  state: initialState,

  subscriptions: {
  },

  effects: {
    *fetchAudioList({ payload: { form = {}, pagination } }, { put, call, select }) {
      console.log('query audio list ', form, pagination);
      const result = yield call(service.fetchAudioList, {
        payload: {
          album_id: !form.id ? undefined : form.id,
          title: form.title,
          category_id: form.category,
          label_id: form.tags,
          status: form.state === 'all' ? undefined : form.state,
          begin: form.publishDate && form.publishDate[0] && form.publishDate[0].format('YYYY-MM-DD HH:mm:ss'),
          end: form.publishDate && form.publishDate[1] && form.publishDate[1].format('YYYY-MM-DD HH:mm:ss'),
          page_no: pagination.current - 1,
          page_size: pagination.pageSize,
        }
      });
      const data = result.data.album_list.map((resDataItem, idx) => ({
        sn: idx + 1,
        id: resDataItem.album_id,
        title: resDataItem.title,
        description: resDataItem.description,
        publishDate: resDataItem.publish_time,
        stateVal: resDataItem.status,
        state: resDataItem.status_txt,
        recommendLevel: resDataItem.weight,
        watchCoins: resDataItem.view_price,
        downloads: resDataItem.download_num,
        categoryList: resDataItem.category_list,
        downloadCoins: resDataItem.download_price,
        earnings: resDataItem.profit_coin,
        plays: resDataItem.view_num,
        favorites: resDataItem.collect_num,
        likes: resDataItem.thumbsup_num,
        shares: resDataItem.share_num,
        commentCount: resDataItem.comment_count,
        coverUrl: resDataItem.cover_url,
        duration: resDataItem.duration,
        durationTxt: resDataItem.duration_txt,
      }));
      yield put({ type: 'stateChanged', payload: { audioList: data, audioTotal: result.data.total_count } });
    },
    *newAudioStart(action, { put }) {
      // reset editing audio
      yield put({ type: 'stateChanged', payload: { editingAudio: {}, editingId: 0 }});
      yield put({ type: 'setModalVisibility', payload: { modalVisibility: 'save' }});
    },
    *editAudioStart({ payload }, { put }) {
      console.log('edit audio start', payload);
      yield put({ type: 'getAlbumItems', payload });
      const editingAudio = {
        editTitle: payload.title,
        editPlays: payload.plays,
        editFavorites: payload.favorites,
        editShares: payload.shares,
        editDesc: payload.description,
        editLikes: payload.likes,
        editDownloads: payload.downloads,
        editPublishDate: payload.publishDate,
        editState: payload.stateVal,
        editStateTxt: payload.state,
        editCategory: payload.categoryList.length > 0 ? payload.categoryList[0].category_id : undefined,
        editTags: payload.tags,
        editCoverImg: payload.coverUrl,
        editCoverLength: payload.durationTxt,
        editCategoryList: payload.categoryList,
        editEpisodes: [],
      };
      yield put({ type: 'stateChanged', payload: { editingAudio, editingId: payload.id, modalVisibility: 'save' } });
    },
    *saveAudio({ payload }, { select, call, put, all }) {
      const audioState = yield select(state => state.audioManage);
      const editingId = audioState.editingId > 0 ? audioState.editingId : undefined;
      const originalEpisodes = audioState.originalEpisodes;
      console.log('audio state', audioState);
      console.log('save audio', payload);
      let httpPayload = {
        album_id: editingId,
        media_type: MEDIA_TYPE.AUDIO,
        title: payload.editTitle,
        description: payload.editDesc,
        publish_time: payload.editPublishDate && payload.editPublishDate.format('YYYY-MM-DD'),
        cover_url: payload.editCoverImg,
        view_num: parseInt(payload.editPlays, 10) || 0,
        download_num: parseInt(payload.editDownloads, 10) || 0,
        share_num: parseInt(payload.editShares, 10) || 0,
        collect_num: parseInt(payload.editFavorites, 10) || 0,
        thumbsup_num: parseInt(payload.editLikes, 10) || 0,
      };
      console.log('save audio http payload', httpPayload);
      const saveResult = yield call(service.saveAudio, { payload: httpPayload });
      const albumId = saveResult.data.album_id;
      payload.editEpisodes = payload.editEpisodes || [];
      // audio items to update
      const updatedItems = payload.editEpisodes
        .filter(ep => Number.isInteger(ep.id))
        .map(ep => ({
          audio_id: ep.id,
          title: ep.sn,
          duration: ep.audioLength,
          view_price: parseInt(ep.watchCoins, 10),
          download_price: parseInt(ep.downloadCoins, 10),
          location: ep.audioUrl,
        }));
      // audio items to add
      const addItems = payload.editEpisodes
        .filter(ep => !Number.isInteger(ep.id))
        .map(ep => ({
          title: ep.sn,
          duration: ep.audioLength,
          view_price: parseInt(ep.watchCoins, 10),
          download_price: parseInt(ep.downloadCoins, 10),
          location: ep.audioUrl,
        }));
      // audio items to delete
      const deleteItems = originalEpisodes
        .filter(ep => !payload.editEpisodes.some(e => e.id === ep.id));

      if (updatedItems.length > 0) {
        yield call(service.batchUpdateAudioItems, {
          payload: {
            album_id: albumId,
            audio_list: updatedItems,
          }
        })
      }
      if (addItems.length > 0) {
        yield all(addItems.map(item => call(service.addAudioItem, {
          payload: {
            album_id: albumId,
            ...item,
          }
        })));
      }
      if (deleteItems.length > 0) {
        yield all(deleteItems.map(item => call(service.deleteAudioItem, {
          payload: {
            audio_id: item.id,
          }
        })));
      }
      // save labels
      if (payload.editTags && payload.editTags.length > 0) {
        yield call(service.saveAudioLabels, {
          payload: {
            album_id: albumId,
            category_id: payload.editCategory,
            label_ids: payload.editTags,
          }
        })
      }
    },
    *deleteAudio({ payload }, { call, put }) {
      console.log('delete audio', payload);
      const httpPayload = {
        album_id: payload.id,
      };
      yield call(service.deleteAudio, { payload: httpPayload });
    },
    *updateAudioStatus({ payload }, { call, put }) {
      console.log('update audio', payload);
      const httpPayload = {
        album_id: payload.id,
        status: payload.status
      };
      yield call(service.updateAudioStatus, { payload: httpPayload });
    },
    *importAudio({ payload }, { call, put }) {
      console.log('import audio', payload);
      yield call(service.importAudio, {
        payload: {
          file: payload.file,
        }
      });
    },
    *fetchAlbumComments({ form, pagination }, { call, put }) {
      const httpResult = yield call(service.fetchAlbumComments, {
        album_id: form.id,
        page_no: pagination.current -  1,
        page_size: pagination.pageSize,
      });
      const albumComments = httpResult.data.comment_list.map((itm, idx) => ({
        id: itm.comment_id,
        content: itm.content,
        userId: itm.user_id,
        userName: itm.username,
        publishTime: itm.publish_time,
        reviewStatus: itm.result_zh,
      }));
      const albumCommentTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { albumComments, modalVisibility: 'comments', albumCommentTotal }});
    },
    *getAlbumItems({ payload }, { select, call, put }) {
      const state = yield select(stt => stt.audioManage);
      const editingAudio = state.editingAudio;
      const result = yield call(service.getAlbumItems, { payload });
      const mappedEpisodes = result.data.item_list.map((ep, idx) => ({
        id: ep.audio_id,
        albumId: ep.album_id,
        sn: ep.title,
        watchCoins: ep.view_price,
        downloadCoins: ep.download_price,
        audioUrl: ep.location,
        audioLength: ep.duration,

      }));
      yield put({
        type: 'stateChanged',
        payload: {
          editingAudio: {
            ...editingAudio,
            editEpisodes: mappedEpisodes,
          },
          originalEpisodes: cloneDeep(mappedEpisodes),
        }
      });
    },
    *setModalVisibility({ payload: { modalVisibility }}, { put }) {
      yield put({ type: 'stateChanged', payload: { modalVisibility } });
    }
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}