import $http from "../../../core/http";
import { MEDIA_TYPE, STATUS } from "../../../common/constants";

export function fetchAudioList({ payload }) {
  return $http.get("/media/album/list", {
    params: {
      ...payload,
      media_type: MEDIA_TYPE.AUDIO,
    },
  });
}

export function saveAudio({ payload }) {
  return $http.post("/media/album", payload);
}

export function deleteAudio({ payload }) {
  return $http.post("/media/album/status", {
    ...payload,
    status: STATUS.DELETED,
  });
}

export function updateAudioStatus({ payload }) {
  return $http.post("/media/album/status", payload);
}

export function importAudio({ payload }) {
  var formData = new FormData();
  formData.append("file", payload.file);
  return $http.post("/media/album/audio_import", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    }
  });
}

export function getAlbumItems({ payload }) {
  return $http.get("/media/album/items", {
    params: {
      album_id: payload.id,
    }
  });
}

export function addAudioItem({ payload }) {
  return $http.post("/media/album/audio", payload);
}

export function deleteAudioItem({ payload }) {
  return $http.delete("/media/album/audio", {
    params: payload,
  });
}

export function batchUpdateAudioItems({ payload }) {
  return $http.post("/media/album/audio/batch", payload);
}

export function saveAudioLabels({ payload }) {
  return $http.post("/media/album/media_labels", payload);
}

export function fetchAlbumComments(payload) {
  return $http.get("/media/album/comment/list", {
    params: payload,
  });
}
