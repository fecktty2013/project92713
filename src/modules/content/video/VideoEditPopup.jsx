import React from "react";
import { Form, Row, Col, Input, DatePicker, Select, Button } from "antd";
import moment from 'moment';

const uuid4 = require('uuid/v4');

const VideoEditPopup = (props) => {

  const { validateFields, getFieldDecorator, getFieldValue, setFieldsValue } = props.form;
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 },
    },
  };

  const { editingVideo = {} } = props.videoManage;

  getFieldDecorator('defaultEpisodes', { initialValue: editingVideo.editEpisodes || [] });
  const defaultEpisodes = getFieldValue('defaultEpisodes');

  const addEpisode = (e) => {
    const episodes = getFieldValue('defaultEpisodes');
    const nextEpisodes = episodes.concat({ id: uuid4() });
    setFieldsValue({
      defaultEpisodes: nextEpisodes,
    });
  };

  const removeEpisode = (idx) => {
    setFieldsValue({
      defaultEpisodes: defaultEpisodes.filter((e, i) => i !== idx),
    });
  }

  const save = (e) => {
    e.preventDefault();
    props.eventHandlers.saveModal(e);
  }

  const handleCategoryChanged = (value, e) => {
    const { resetFields, setFieldsValue } = props.form;
    setFieldsValue({ editTags: [] });
  };

  const validateTagField = (rule, value, callback) => {
    const { getFieldValue } = props.form;
    if (!getFieldValue('editCategory')) {
      callback('请先选择分类');
      return;
    }
    if (!value || value.length === 0) {
      callback('请选择标签');
      return;
    }
    callback();
  };

  const filterTagList = () => {
    const { getFieldValue } = props.form;
    const selectedCategory = getFieldValue('editCategory');
    if (!selectedCategory)
      return [];
    return props.app.tagList.filter(t => t.categoryId == selectedCategory);
  };

  return (
    <Form {...formItemLayout}>
      <Row gutter={8}>
        <Col span={12}>
          <Form.Item label="标题">
            {getFieldDecorator('editTitle', {
              rules: [
                {
                  required: true,
                  message: '视频标题不允许为空',
                }
              ],
              initialValue: editingVideo.editTitle
            })(
              <Input placeholder="请输入标题" />
            )}
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label="播放">
            {getFieldDecorator('editPlays', { initialValue: editingVideo.editPlays })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label="收藏">
            {getFieldDecorator('editFavorites', { initialValue: editingVideo.editFavorites })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label="分享">
            {getFieldDecorator('editShares', { initialValue: editingVideo.editShares })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="描述">
            {getFieldDecorator('editDesc', { initialValue: editingVideo.editDesc })(
              <Input.TextArea style={{ lineHeight: 1.2 }} placeholder="请输入描述" rows={5} />
            )}
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label="点赞">
            {getFieldDecorator('editLikes', { initialValue: editingVideo.editLikes })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label="下载">
            {getFieldDecorator('editDownloads', { initialValue: editingVideo.editDownloads })(
              <Input type="number" />
            )}
          </Form.Item>
        </Col>
        <Col span={24} />
        <Col span={12}>
          <Form.Item label="发布时间">
            {getFieldDecorator('editPublishDate', { initialValue: moment(editingVideo.editPublishDate) })(
              <DatePicker style={{ width: '75%' }} />
            )}
          </Form.Item>
        </Col>
        <Col span={24} />
        <Col span={12}>
          <Form.Item label="视频状态">
            {getFieldDecorator('editState', {
              initialValue: editingVideo.editState?.toString(),
              rules: [{ required: true, message: '状态不允许为空' }],
            })(
              <Select style={{ width: '50%' }}>
                <Select.Option value="1">正常</Select.Option>
                <Select.Option value="2">冻结</Select.Option>
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="分类">
            {getFieldDecorator('editCategory', {
              initialValue: editingVideo.editCategory,
              rules: [{ required: true, message: '请选择分类' }],
            })(
              <Select placeholder="请选择分类" onChange={handleCategoryChanged}>
                {props.app.categoryList.map(c => (
                  <Select.Option key={c.key} value={c.key}>{c.categoryName}</Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="标签">
            {getFieldDecorator('editTags', {
              initialValue: editingVideo.editCategoryList?.reduce((a,c) => a.concat(c.label_list.map(l => l.label_id)),[]),
              rules: [{ validator: validateTagField }],
            })(
              <Select mode="multiple">
                {filterTagList().map(t => (
                  <Select.Option key={t.key} value={t.id}>{t.name}</Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col span={24} />
        <Col span={12}>
          <Form.Item label="封面图">
            {getFieldDecorator('editCoverImg', { initialValue: editingVideo.editCoverImg })(
              <Input style={{ width: '75%' }} />
            )}
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="时长">
            {getFieldDecorator('editCoverLength', { initialValue: editingVideo.editCoverLength })(
              <Input style={{ width: '75%' }} />
            )}
          </Form.Item>
        </Col>
        <Col span={24} style={{ display: 'block' }}>
          <Row>
            <Col style={{ textAlign: 'right' }} span={3}>
              <label>选集：</label>
            </Col>
            <Col span={21}>
              {defaultEpisodes.map((episode, idx) => (
                <Row key={idx}>
                  {getFieldDecorator(`editEpisodes[${idx}].id`, { initialValue: episode.id })}
                  <Col span={1}>
                    <span>{idx+1}. </span>
                  </Col>
                  <Col>
                    <Row type="flex" justify="space-between" style={{ flexWrap: 'nowrap' }}>
                      <Col>
                        <Form.Item>
                          {getFieldDecorator(`editEpisodes[${idx}].sn`, { initialValue: episode.sn})(
                            <Input placeholder="集数" />
                          )}
                        </Form.Item>
                      </Col>
                      <Col>
                        <Form.Item>
                          {getFieldDecorator(`editEpisodes[${idx}].watchCoins`, { initialValue: episode.watchCoins })(
                            <Input type="number" placeholder="观看硬币数量" />
                          )}
                        </Form.Item>
                      </Col>
                      <Col>
                        <Form.Item>
                          {getFieldDecorator(`editEpisodes[${idx}].downloadCoins`, { initialValue: episode.downloadCoins })(
                            <Input type="number" placeholder="下载硬币数量" />
                          )}
                        </Form.Item>
                      </Col>
                      <Col>
                        <Form.Item>
                          {getFieldDecorator(`editEpisodes[${idx}].videoUrl`, { initialValue: episode.videoUrl })(
                            <Input placeholder="视频路径" />
                          )}
                        </Form.Item>
                      </Col>
                      <Col>
                        <Form.Item>
                          {getFieldDecorator(`editEpisodes[${idx}].videoLength`, { initialValue: episode.videoLength })(
                            <Input placeholder="视频时长" />
                          )}
                        </Form.Item>
                      </Col>
                      <Col>
                        <Button style={{ border: 'none', marginTop: 4 }} icon="minus-circle" onClick={e => removeEpisode(idx)} />
                      </Col>
                    </Row>
                  </Col>
                </Row>
              ))}
              <Row>
                <Col span={1} />
                <Col>
                  <Button onClick={addEpisode}>增加选集</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col span={24} offset={2} style={{ display: 'block', marginTop: 24 }}>
          <Button type="primary" size="large" loading={props.loading['videoManage/saveVideo']} style={{ width: 240 }} onClick={save}>保存</Button>
        </Col>
      </Row>
    </Form>
  )

}

export default VideoEditPopup;
