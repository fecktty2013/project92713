import $http from "../../../core/http";
import { MEDIA_TYPE, STATUS } from "../../../common/constants";

export function fetchVideoList({ payload }) {
  return $http.get("/media/album/list", {
    params: {
      ...payload,
      media_type: MEDIA_TYPE.VIDEO,
    },
  });
}

export function saveVideo({ payload }) {
  return $http.post("/media/album", payload);
}

export function deleteVideo({ payload }) {
  return $http.post("/media/album/status", {
    ...payload,
    status: STATUS.DELETED,
  });
}

export function updateVideoStatus({ payload }) {
  return $http.post("/media/album/status", payload);
}

export function importVideo({ payload }) {
  var formData = new FormData();
  formData.append("file", payload.file);
  return $http.post("/media/album/video_import", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    }
  });
}

export function getAlbumItems({ payload }) {
  return $http.get("/media/album/items", {
    params: {
      album_id: payload.id,
    }
  });
}

export function addVideoItem({ payload }) {
  return $http.post("/media/album/video", payload);
}

export function deleteVideoItem({ payload }) {
  return $http.delete("/media/album/video", {
    params: payload,
  });
}

export function batchUpdateVideoItems({ payload }) {
  return $http.post("/media/album/video/batch", payload);
}

export function saveVideoLables({ payload }) {
  return $http.post("/media/album/media_labels", payload);
}

export function fetchAlbumComments(payload) {
  return $http.get("/media/album/comment/list", {
    params: payload,
  });
}

export function getAlbumSortType() {
  return $http.get("/media/album/sort/condition");
}

export function setAlbumSortType(sortType) {
  return $http.post("/media/album/sort/condition", {
    sort_key: sortType,
  });
}
