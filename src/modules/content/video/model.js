import { routerRedux } from 'dva/router';
import * as service from './service';
import cloneDeep from 'lodash/cloneDeep';
import { MEDIA_TYPE } from '../../../common/constants';

const initialState = {
  videoList: [],
  videoTotal: 0,

  modalVisibility: '',

  editingId: 0,
  editingVideo: {},
  originalEpisodes: [],

  albumComments: [],
  albumCommentTotal: 0,

  sortType: undefined,
};

export default {
  namespace: 'videoManage',

  state: initialState,

  subscriptions: {
  },

  effects: {
    *fetchVideoList({ payload: { form = {}, pagination } }, { call, put, select }) {
      console.log('query video list ', form, pagination);
      const result = yield call(service.fetchVideoList, {
        payload: {
          album_id: !form.id ? undefined : form.id,
          title: form.title,
          category_id: form.category,
          label_id: form.tags,
          status: form.state === 'all' ? undefined : form.state,
          begin: form.publishDate && form.publishDate[0] && form.publishDate[0].format('YYYY-MM-DD HH:mm:ss'),
          end: form.publishDate && form.publishDate[1] && form.publishDate[1].format('YYYY-MM-DD HH:mm:ss'),
          page_no: pagination.current - 1,
          page_size: pagination.pageSize,
        }
      });
      const data = result.data.album_list.map((resDataItem, idx) => ({
        sn: idx + 1,
        id: resDataItem.album_id,
        title: resDataItem.title,
        description: resDataItem.description,
        publishDate: resDataItem.publish_time,
        stateVal: resDataItem.status,
        state: resDataItem.status_txt,
        recommendLevel: resDataItem.weight,
        watchCoins: resDataItem.view_price,
        downloads: resDataItem.download_num,
        downloadCoins: resDataItem.download_price,
        earnings: resDataItem.profit_coin,
        plays: resDataItem.view_num,
        favorites: resDataItem.collect_num,
        likes: resDataItem.thumbsup_num,
        shares: resDataItem.share_num,
        commentCount: resDataItem.comment_count,
        coverUrl: resDataItem.cover_url,
        duration: resDataItem.duration,
        durationTxt: resDataItem.duration_txt,
        categoryList: resDataItem.category_list,
      }));
      yield put({ type: 'stateChanged', payload: { videoList: data, videoTotal: result.data.total_count } });
    },
    *fetchSortType(action, { call, put }) {
      const httpResult = yield call(service.getAlbumSortType);
      const sortType = httpResult.data.sort_key;
      yield put({ type: 'stateChanged', payload: { sortType }});
    },
    *saveSortType({ sortType }, { call }) {
      yield call(service.setAlbumSortType, sortType);
    },
    *newVideoStart(action, { put }) {
      // reset editing video
      yield put({ type: 'stateChanged', payload: { editingVideo: {}, editingId: 0 } });
      yield put({ type: 'setModalVisibility', payload: { modalVisibility: 'save' } });
    },
    *editVideoStart({ payload }, { put }) {
      console.log('edit video start', payload);
      yield put({ type: 'getAlbumItems', payload });
      const editingVideo = {
        editTitle: payload.title,
        editPlays: payload.plays,
        editFavorites: payload.favorites,
        editShares: payload.shares,
        editDesc: payload.description,
        editLikes: payload.likes,
        editDownloads: payload.downloads,
        editPublishDate: payload.publishDate,
        editState: payload.stateVal,
        editStateTxt: payload.state,
        editCategory: payload.categoryList.length > 0 ? payload.categoryList[0].category_id : undefined,
        editTags: payload.tags,
        editCoverImg: payload.coverUrl,
        editCoverLength: payload.durationTxt,
        editCategoryList: payload.categoryList,
        editEpisodes: [],
      };
      yield put({ type: 'stateChanged', payload: { editingVideo, editingId: payload.id, modalVisibility: 'save' } });
    },
    *saveVideo({ payload }, { select, call, put, all }) {
      const videoState = yield select(state => state.videoManage);
      const editingId = videoState.editingId > 0 ? videoState.editingId : undefined;
      const originalEpisodes = videoState.originalEpisodes;
      console.log('video state', videoState);
      console.log('save video', payload);
      let httpPayload = {
        album_id: editingId,
        media_type: MEDIA_TYPE.VIDEO,
        title: payload.editTitle,
        description: payload.editDesc,
        publish_time: payload.editPublishDate && payload.editPublishDate.format('YYYY-MM-DD'),
        cover_url: payload.editCoverImg,
        view_num: parseInt(payload.editPlays, 10) || 0,
        download_num: parseInt(payload.editDownloads, 10) || 0,
        share_num: parseInt(payload.editShares, 10) || 0,
        collect_num: parseInt(payload.editFavorites, 10) || 0,
        thumbsup_num: parseInt(payload.editLikes, 10) || 0,
      };
      console.log('save video http payload', httpPayload);
      const saveResult = yield call(service.saveVideo, { payload: httpPayload });
      const albumId = saveResult.data.album_id;
      payload.editEpisodes = payload.editEpisodes || [];
      // video items to update
      const updatedItems = payload.editEpisodes
        .filter(ep => Number.isInteger(ep.id))
        .map(ep => ({
          video_id: ep.id,
          title: ep.sn,
          duration: ep.videoLength,
          view_price: parseInt(ep.watchCoins, 10),
          download_price: parseInt(ep.downloadCoins, 10),
          location: ep.videoUrl,
        }));
      // video items to add
      const addItems = payload.editEpisodes
        .filter(ep => !Number.isInteger(ep.id))
        .map(ep => ({
          title: ep.sn,
          duration: ep.videoLength,
          view_price: parseInt(ep.watchCoins, 10),
          download_price: parseInt(ep.downloadCoins, 10),
          location: ep.videoUrl,
        }));
      // video items to delete
      const deleteItems = originalEpisodes
        .filter(ep => !payload.editEpisodes.some(e => e.id === ep.id));

      if (updatedItems.length > 0) {
        yield call(service.batchUpdateVideoItems, {
          payload: {
            album_id: albumId,
            video_list: updatedItems
          }
        })
      }
      if (addItems.length > 0) {
        yield all(addItems.map(item => call(service.addVideoItem, {
          payload: {
            album_id: albumId,
            ...item,
          }
        })));
      }
      if (deleteItems.length > 0) {
        yield all(deleteItems.map(item => call(service.deleteVideoItem, {
          payload: {
            video_id: item.id,
          }
        })));
      }

      // save labels
      if (payload.editTags && payload.editTags.length > 0) {
        yield call(service.saveVideoLables, {
          payload: {
            album_id: albumId,
            category_id: payload.editCategory,
            label_ids: payload.editTags,
          }
        });
      }

    },
    *deleteVideo({ payload }, { call, put }) {
      console.log('delete video', payload);
      const httpPayload = {
        album_id: payload.id,
      };
      yield call(service.deleteVideo, { payload: httpPayload });
    },
    *updateVideoStatus({ payload }, { call, put }) {
      console.log('update video', payload);
      const httpPayload = {
        album_id: payload.id,
        status: payload.status
      };
      yield call(service.updateVideoStatus, { payload: httpPayload });
    },
    *importVideo({ payload }, { call, put }) {
      console.log('import video', payload);
      yield call(service.importVideo, {
        payload: {
          file: payload.file,
        }
      });
    },
    *fetchAlbumComments({ form, pagination }, { call, put }) {
      const httpResult = yield call(service.fetchAlbumComments, {
        album_id: form.id,
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const albumComments = httpResult.data.comment_list.map((itm, idx) => ({
        id: itm.comment_id,
        content: itm.content,
        userId: itm.user_id,
        userName: itm.username,
        publishTime: itm.publish_time,
        reviewStatus: itm.result_zh,
      }));
      const albumCommentTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { albumComments, modalVisibility: 'comments', albumCommentTotal }});
    },
    *getAlbumItems({ payload }, { select, call, put }) {
      const state = yield select(stt => stt.videoManage);
      const editingVideo = state.editingVideo;
      const result = yield call(service.getAlbumItems, { payload });
      const mappedEpisodes = result.data.item_list.map((ep, idx) => ({
        id: ep.video_id,
        albumId: ep.album_id,
        sn: ep.title,
        watchCoins: ep.view_price,
        downloadCoins: ep.download_price,
        videoUrl: ep.location,
        videoLength: ep.duration,

      }));
      yield put({
        type: 'stateChanged',
        payload: {
          editingVideo: {
            ...editingVideo,
            editEpisodes: mappedEpisodes,
          },
          originalEpisodes: cloneDeep(mappedEpisodes),
        }
      });
    },
    *setModalVisibility({ payload: { modalVisibility } }, { put }) {
      yield put({ type: 'stateChanged', payload: { modalVisibility } });
    }
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}