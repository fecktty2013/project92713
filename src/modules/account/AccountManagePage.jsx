import React from "react";
import { Breadcrumb, Row, Col } from "antd";
import AccountManage from "./AccountManage";
import RoleManage from "./RoleManage";

import connectWithAuth from "../../core/connectWithAuth";

import style from "./style.scss";

const AccountManagePage = (props) => {

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>账户管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <AccountManage {...props} />
        <RoleManage {...props} />
      </div>
    </div>
  )
};

export default connectWithAuth(({ accountManage }) => ({ accountManage }))(AccountManagePage);
