import $http from "../../core/http";

export function fetchAccountList(payload) {
  return $http.get("/account/admin/list", {
    params: payload
  });
}

export function saveAccount(payload) {
  return $http.post("/account/admin", payload);
}

export function deleteAccount(payload) {
  return $http.delete("/account/admin", {
    params: payload,
  });
}

export function fetchRoleList(payload) {
  return $http.get("/account/role/list", {
    params: payload,
  });
}

export function saveRole(payload) {
  return $http.post("/account/role", payload);
}

export function deleteRole(payload) {
  return $http.delete("/account/role", {
    params: payload,
  });
} 
