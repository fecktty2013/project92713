import { routerRedux } from 'dva/router';
import * as service from './service';

const initialState = {
  accountList: [],
  accountTotal: 0,
  editingAccount: {},

  allRoleList: [],

  roleList: [],
  roleTotal: 0,
  editingRole: {},

  modalVisibility: '',

};

export default {
  namespace: 'accountManage',

  state: initialState,

  subscriptions: {
  },

  effects: {
    *fetchAccountList({ payload: { form, pagination } = {} }, { put, call, select }) {
      const httpResult = yield call(service.fetchAccountList, {
        username: form.userName,
        phone: form.phoneNumber,
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const accountList = httpResult.data.admin_list.map((item, idx) => ({
        id: item.user_id,
        sn: idx + 1,
        userName: item.username,
        createTime: item.create_time,
        phoneNumber: item.phone,
        state: item.status_zh,
        stateVal: item.status,
        roleName: item.role,
        roleId: item.role_id,
      }));
      const accountTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { accountList, accountTotal } });
    },
    *editAccountStart({ payload }, { put }) {
      yield put({ type: 'stateChanged', payload });
    },
    *saveAccount({ payload }, { select, call, put }) {
      const accountState = yield select(state => state.accountManage);
      const httpRequest = {
        phone: payload.editPhoneNumber,
        status: parseInt(payload.editState, 10),
        username: payload.editUserName,
        role_id: payload.editRole,
        password: payload.editPassword,
      }
      if (accountState.editingAccount.id) {
        httpRequest.user_id = accountState.editingAccount.id;
      }
      yield call(service.saveAccount, httpRequest);
    },
    *updateAccountStatus({ payload: { record, status } }, { call, put }) {
      const httpRequest = {
        user_id: record.id,
        phone: record.phoneNumber,
        status: status,
        username: record.userName,
        role_id: record.roleId,
      }
      yield call(service.saveAccount, httpRequest);
    },
    *deleteAccount({ payload }, { call, put }) {
      yield call(service.deleteAccount, payload);
    },
    *fetchAllRoleList(action, { call, put }) {
      const httpResult = yield call(service.fetchRoleList, {
        page_no: 0,
        page_size: 100,
      });
      const allRoleList = httpResult.data.role_list
        .filter(item => item.status === 1)
        .map((item, idx) => ({
          id: item.role_id,
          sn: idx + 1,
          roleName: item.role_name,
          roleDesc: item.desc,
        }));
      yield put({ type: 'stateChanged', payload: { allRoleList } });
    },
    *fetchRoleList({ payload: { pagination } }, { put, call }) {
      const httpResult = yield call(service.fetchRoleList, {
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const roleList = httpResult.data.role_list
        .map((item, idx) => ({
          id: item.role_id,
          sn: idx + 1,
          roleName: item.role_name,
          roleDesc: item.desc,
          state: item.status_zh,
          stateVal: item.status,
          permission: item.permission,
        }));
      const roleTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { roleList, roleTotal } });
    },
    *editRoleStart({ payload }, { put }) {
      yield put({ type: 'stateChanged', payload });
    },
    *saveRole({ payload }, { select, call, put }) {
      const accountState = yield select(state => state.accountManage);
      const httpRequest = {
        role_name: payload.editRoleName,
        desc: payload.editRoleDesc,
        status: parseInt(payload.editState, 10),
        permission: payload.permission,
      }
      if (accountState.editingRole.id) {
        httpRequest.role_id = accountState.editingRole.id;
      }
      console.log('save role: ', httpRequest);
      yield call(service.saveRole, httpRequest);
    },
    *updateRoleStatus({ payload: { record, status } }, { call, put }) {
      const httpRequest = {
        role_id: record.id,
        role_name: record.roleName,
        desc: record.roleDesc,
        permission: record.permission,
        status,
      };
      yield call(service.saveRole, httpRequest);
    },
    *deleteRole({ payload }, { call, put }) {
      yield call(service.deleteRole, payload);
    },
    *setModalVisibility({ payload: { modalVisibility } }, { put }) {
      yield put({ type: 'stateChanged', payload: { modalVisibility } });
    },
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}