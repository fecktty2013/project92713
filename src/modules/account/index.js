import React from "react";
import AccountManagePage from "./AccountManagePage";
import model from "./model";

export default {
  component: AccountManagePage, model,
};
