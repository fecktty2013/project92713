import React, { useState, useEffect } from "react";

import style from "./style.scss";
import { Card, Button, Icon, Form, Row, Col, Popconfirm, Select, Table, Modal, Input, Checkbox, Upload, Radio } from "antd";

const AccountManage = (props) => {

  const { getFieldDecorator, validateFields, resetFields } = props.form;
  const { editingAccount, allRoleList } = props.accountManage;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const handleQuery = () => {
    validateFields((err, values) => {
      console.log('query: ', err, values);
      props.dispatch({
        type: 'accountManage/fetchAccountList',
        payload: { form: values, pagination }
      });
    });
  };

  const queryAllRoles = () => {
    props.dispatch({
      type: 'accountManage/fetchAllRoleList',
    });
  };

  const resetBasicForm = () => {
    resetFields([
      "editUserName", "editPhoneNumber", "editState"
    ]);
  };

  const eventHandlers = {
    handleNewAccountStart(e) {
      resetBasicForm();
      props.dispatch({
        type: 'accountManage/editAccountStart',
        payload: { modalVisibility: 'accounts', editingAccount: {} },
      });
    },
    handleEditAccountStart(record) {
      resetBasicForm();
      props.dispatch({
        type: 'accountManage/editAccountStart',
        payload: { modalVisibility: 'accounts', editingAccount: record },
      });
    },
    async handleAccountDelete(record) {
      await props.dispatch({
        type: 'accountManage/deleteAccount',
        payload: { user_id: record.id },
      });
      handleQuery();
    },
    handleAccountCancel(e) {
      props.dispatch({
        type: 'accountManage/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    async handleUpdateAccountStatus(record, status) {
      await props.dispatch({
        type: 'accountManage/updateAccountStatus',
        payload: { record, status },
      });
      handleQuery();
    },
    handleAccountSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'accountManage/saveAccount',
          payload: values,
        });
        await props.dispatch({
          type: 'accountManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const handleTableChange = (pagination) => {
    console.log('table changed: ', pagination);
    setPagination(pagination);
  };

  const columns = [
    {
      title: '序号',
      render: (text, record) => {
        return <span key={record.id}>{pagination.pageSize * (pagination.current - 1) + record.sn}</span>
      }
    },
    { title: '姓名', dataIndex: 'userName' },
    { title: '添加时间', dataIndex: 'createTime' },
    { title: '手机号码', dataIndex: 'phoneNumber' },
    { title: '角色', render: (text, record) => (
      <span>{record.roleId == 0 ? '超级管理员' : record.roleName}</span>
    ) },
    { title: '状态', dataIndex: 'state' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditAccountStart(record)}>编辑</Button>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleAccountDelete(record)}>
            <a>删除</a>
          </Popconfirm>
          {
            record.stateVal == 1 ?
              <Popconfirm title="确认禁用吗？" onConfirm={e => eventHandlers.handleUpdateAccountStatus(record, 2)}>
                <Button className={style.rowBtn} type="link">禁用</Button>
              </Popconfirm> : undefined
          }
          {
            record.stateVal == 2 ?
              <Popconfirm title="确认启用吗？" onConfirm={e => eventHandlers.handleUpdateAccountStatus(record, 1)}>
                <Button className={style.rowBtn} type="link">启用</Button>
              </Popconfirm> : undefined
          }
        </span>
      )
    },
  ];

  useEffect(handleQuery, [pagination]);

  useEffect(queryAllRoles, []);

  return (
    <React.Fragment>
      <Card title="账户管理"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0, paddingTop: 0 }}
        extra={
          <Button type="dashed" onClick={eventHandlers.handleNewAccountStart}>
            <Icon type="plus" /> 新增
          </Button>
        }
      >
        <Form layout="horizontal" style={{ marginBottom: 20 }}>
          <Row gutter={16} type="flex" align="bottom">
            <Col span={6}>
              <Form.Item label="员工姓名" className={style.formItem}>
                {getFieldDecorator('userName')(
                  <Input />
                )}
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="登录账户" className={style.formItem}>
                {getFieldDecorator('phoneNumber')(
                  <Input />
                )}
              </Form.Item>
            </Col>
            <Col span={6}>
              <Button type="primary" className={style.actionBtn} onClick={handleQuery}>查询</Button>
            </Col>
          </Row>
        </Form>
        <Table
          rowKey={record => record.id}
          columns={columns}
          onChange={handleTableChange}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.accountManage.accountTotal,
            showTotal: () => <span>总共 {props.accountManage.accountTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          scroll={{ x: 'max-content' }}
          dataSource={props.accountManage.accountList}
        />
      </Card>
      <Modal
        title="账户管理"
        width={600}
        visible={props.accountManage.modalVisibility === 'accounts'}
        onCancel={eventHandlers.handleAccountCancel}
        onOk={eventHandlers.handleAccountSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="姓名">
            {getFieldDecorator('editUserName', {
              rules: [{ required: true, message: '员工姓名不允许为空' }],
              initialValue: editingAccount.userName
            })(
              <Input />
            )}
          </Form.Item>
          <Form.Item label="手机号码">
            {getFieldDecorator('editPhoneNumber', {
              rules: [{ required: true, message: '请输入手机号码'}],
              initialValue: editingAccount.phoneNumber
            })(<Input />)}
          </Form.Item>
          <Form.Item label="密码">
            {getFieldDecorator('editPassword')(
              <Input.Password placeholder="输入新密码可以重置" />
            )}
          </Form.Item>
          <Form.Item label="状态">
            {getFieldDecorator('editState', {
              initialValue: editingAccount?.stateVal?.toString()
            })(
              <Radio.Group>
                <Radio.Button value="1">正常</Radio.Button>
                <Radio.Button value="2">禁用</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>
          <Form.Item label="角色">
            {getFieldDecorator('editRole', {
              initialValue: editingAccount?.roleId,
            })(
              <Select>
                {allRoleList.map(r => (
                  <Select.Option key={r.id} value={r.id}>{r.roleName}</Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(AccountManage);
