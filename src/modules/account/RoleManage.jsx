import React, { useState, useEffect } from "react";

import style from "./style.scss";
import { Card, Button, Icon, Form, Row, Col, Popconfirm, Select, Table, Modal, Input, Checkbox, Upload, Radio, Tree } from "antd";
import { menuTree } from "../../common/menus";

const { TreeNode } = Tree;

const RoleManage = (props) => {

  const { getFieldDecorator, validateFields, resetFields } = props.form;
  const { editingRole } = props.accountManage;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const [expandedKeys, setExpandedKeys] = useState([]);
  const [autoExpandParent, setAutoExpandParent] = useState(true);
  const [checkedKeys, setCheckedKeys] = useState([]);

  const onExpand = keys => {
    setExpandedKeys(keys);
    setAutoExpandParent(false);
  };

  const onCheck = keys => {
    setCheckedKeys(keys);
  }

  const renderTreeNodes = data => data.map(item => {
    if (item.children) {
      return (
        <TreeNode title={item.title} key={item.key} dataRef={item}>
          {renderTreeNodes(item.children)}
        </TreeNode>
      );
    }
    return <TreeNode key={item.key} {...item} />;
  });

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const resetForm = () => {
    resetFields(["editRoleName", "editRoleDesc"]);
  };

  const handleQuery = () => {
    props.dispatch({
      type: 'accountManage/fetchRoleList',
      payload: { pagination }
    });
  };

  const eventHandlers = {
    handleNewRoleStart(e) {
      setExpandedKeys([]);
      setCheckedKeys([]);
      props.dispatch({
        type: 'accountManage/editRoleStart',
        payload: { modalVisibility: 'roles', editingRole: {} },
      });
    },
    handleEditRoleStart(record) {
      setExpandedKeys([]);
      setCheckedKeys(record.permission);
      props.dispatch({
        type: 'accountManage/editRoleStart',
        payload: { modalVisibility: 'roles', editingRole: record },
      });
    },
    async handleRoleDelete(record) {
      await props.dispatch({
        type: 'accountManage/deleteRole',
        payload: { role_id: record.id },
      });
      handleQuery();
    },
    handleRoleCancel(e) {
      props.dispatch({
        type: 'accountManage/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    async handleUpdateRoleStatus(record, status) {
      await props.dispatch({
        type: 'accountManage/updateRoleStatus',
        payload: { record, status },
      });
      handleQuery();
    },
    handleRoleSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'accountManage/saveRole',
          payload: {
            ...values,
            permission: checkedKeys
          },
        });
        await props.dispatch({
          type: 'accountManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const handleTableChange = (pagination) => {
    console.log('table changed: ', pagination);
    setPagination(pagination);
  };

  const columns = [
    { title: '序号', dataIndex: 'sn' },
    { title: '角色名称', dataIndex: 'roleName' },
    { title: '角色描述', dataIndex: 'roleDesc' },
    { title: '状态', dataIndex: 'state' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditRoleStart(record)}>编辑</Button>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleRoleDelete(record)}>
            <a>删除</a>
          </Popconfirm>
          {
            record.stateVal == 1 ?
              <Popconfirm title="确认禁用吗？" onConfirm={e => eventHandlers.handleUpdateRoleStatus(record, 2)}>
                <Button className={style.rowBtn} type="link">禁用</Button>
              </Popconfirm> : undefined
          }
          {
            record.stateVal == 2 ?
              <Popconfirm title="确认启用吗？" onConfirm={e => eventHandlers.handleUpdateRoleStatus(record, 1)}>
                <Button className={style.rowBtn} type="link">启用</Button>
              </Popconfirm> : undefined
          }
        </span>
      )
    },
  ];

  useEffect(handleQuery, [pagination]);

  return (
    <React.Fragment>
      <Card title="角色管理"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0, paddingTop: 0 }}
        extra={
          <Button type="dashed" onClick={eventHandlers.handleNewRoleStart}>
            <Icon type="plus" /> 新增
          </Button>
        }
      >
        <Table
          rowKey={record => record.id}
          columns={columns}
          onChange={handleTableChange}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.accountManage.roleTotal,
            showTotal: () => <span>总共 {props.accountManage.roleTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          scroll={{ x: 'max-content' }}
          dataSource={props.accountManage.roleList}
        />
      </Card>
      <Modal
        title="角色管理"
        width={600}
        visible={props.accountManage.modalVisibility === 'roles'}
        onCancel={eventHandlers.handleRoleCancel}
        onOk={eventHandlers.handleRoleSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="角色名称">
            {getFieldDecorator('editRoleName', {
              rules: [{ required: true, message: '角色名称不允许为空' }],
              initialValue: editingRole.roleName,
            })(
              <Input />
            )}
          </Form.Item>
          <Form.Item label="角色描述">
            {getFieldDecorator('editRoleDesc', {
              initialValue: editingRole.roleDesc,
            })(<Input />)}
          </Form.Item>
          <Form.Item label="状态">
            {getFieldDecorator('editState', {
              initialValue: editingRole?.stateVal?.toString()
            })(
              <Radio.Group>
                <Radio.Button value="1">正常</Radio.Button>
                <Radio.Button value="2">禁用</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>
          <Form.Item label="权限">
            <Tree
              checkable
              onExpand={onExpand}
              expandedKeys={expandedKeys}
              autoExpandParent={autoExpandParent}
              onCheck={onCheck}
              checkedKeys={checkedKeys}
            >
              {renderTreeNodes(menuTree)}
            </Tree>
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(RoleManage);
