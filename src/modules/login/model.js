import { routerRedux } from 'dva/router';
import * as service from "./service";

const initialState = {
  loginSuccessful: false,
  firstTimeLogin: false,
  loginResponseMsg: '',
};

export default {
  namespace: 'login',

  state: initialState,

  subscriptions: {
    resetState({ dispatch }) {
      return () => dispatch({ type: 'loginStateChanged', payload: initialState });
    }
  },

  effects: {
    *login({ payload: { phoneNumber, password } }, { call, put, select }) {
      const result = yield call(service.login, phoneNumber, password);
      yield put({ type: 'app/updateToken', payload: {
          token: result.data?.access_token,
          adminId: result.data?.admin_id,
          userName: result.data?.username,
          isSuperUser: result.data?.is_super_user,
          permission: result.data?.permission,
        }
      });
      const loginSuccessful = result.errNo === 0;
      const firstTimeLogin = result.errNo === 100;
      const loginResponseMsg = result.errMsg;
      yield put({
        type: 'app/authenticateComplete', payload: {
          authenticated: loginSuccessful,
        }
      });
      yield put({
        type: 'loginStateChanged', payload: {
          loginSuccessful, firstTimeLogin, loginResponseMsg,
        }
      });

      return yield select(state => state.login);
    },
    *gotoModifyPassword({ payload: { pathname, search } }, { put, select }) {
      yield put({ type: 'loginStateChanged', payload: initialState });
      yield put(routerRedux.push({
        pathname, search,
      }));
    }
  },

  reducers: {
    loginStateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}