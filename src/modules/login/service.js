import $http from "../../core/http";

export function login(phone, password) {
  return $http.post("/auth/login", {
    phone, password,
  });
}