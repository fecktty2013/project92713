import React from "react";
import { connect } from "dva";
import { routerRedux } from "dva/router";
import { Layout, Form, Icon, Input, Button, Avatar, Alert, Modal, message } from "antd";
import style from "./style.scss";

const { Header, Content } = Layout;

const LoginPage = (props) => {

  const handleSubmit = async e => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {
        let loginState = await props.dispatch({ type: 'login/login', payload: values });
        if (!loginState.loginSuccessful && !loginState.firstTimeLogin) {
          message.error(loginState.loginResponseMsg);
        } else if (loginState.loginSuccessful) {
          let from = props.location.state?.from;
          props.dispatch(routerRedux.push({
            pathname: from?.pathname || '/',
            search: from?.search
          }));
        }
      }
    });
  };

  const redirectToModifyPassword = e => {
    props.dispatch({
      type: 'login/gotoModifyPassword',
      payload: {
        pathname: '/modifyPassword'
      }
    });
  };

  const { getFieldDecorator } = props.form;
  return (
    <div className={style.container}>
      <Layout className={style.layout}>
        <Header className={style.header}>欢迎登录后台管理系统</Header>
        <Content className={style.content}>
          <Avatar icon="lock" size="large" style={{ backgroundColor: '#f50057', marginBottom: 12 }} />
          <Form className={style.form} onSubmit={handleSubmit}>
            <Form.Item className={style.formItem}>
              {getFieldDecorator('phoneNumber', {
                rules: [{ required: true, message: '请输入手机号码！' }],
              })(
                <Input className={style.input} size="large"
                  placeholder="手机号码"
                />,
              )}
            </Form.Item>
            <Form.Item className={style.formItem}>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: '请输入密码！' }],
              })(
                <Input.Password className={style.input} size="large"
                  placeholder="密码"
                />,
              )}
            </Form.Item>
            <Form.Item style={{ textAlign: 'center' }}>
              <Button type="primary" size="large" htmlType="submit" className={style.btn}>
                登录
              </Button>
            </Form.Item>
          </Form>
        </Content>
      </Layout>
      <Modal
        width={320}
        visible={props.login.firstTimeLogin}
        closable={false}
        title="提示" centered
        onOk={redirectToModifyPassword}
        footer={[
          <Button 
            key="submit"
            block
            ghost
            type="primary"
            style={{ border: 'none', height: '100%' }}
            onClick={redirectToModifyPassword}>
            好的
          </Button>
        ]}
      >
        <p>检测到您是第一次登录，请修改密码</p>
      </Modal>
    </div>
  );
}

export default connect(({ login }) => ({ login }))(Form.create()(LoginPage));
