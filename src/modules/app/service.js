import $http from "../../core/http";

export function fetchCategoryList(form) {
  return $http.get("/media/category/list", {
    params: {
      media_type: form.mediaType,
    }
  });
}

export function fetchTagList(form) {
  return $http.get("/media/label/list", {
    params: {
      media_type: form.mediaType,
    }
  });
}
