import React from "react";
import { connect } from "dva";
import RootLayout from "../../layouts/RootLayout";
import model from './model';
import { ConfigProvider } from "antd";

const App = (props) => {
  const globalPages = [
    '/login', '/modifyPassword',
  ];
  const isGlobalPages = globalPages.includes(props.pathname);
  return (
    <ConfigProvider autoInsertSpaceInButton={false}>
      {isGlobalPages ? props.children : (
        <RootLayout>
          {props.children}
        </RootLayout>
      )}
    </ConfigProvider>
  );
};

const ConnectedApp = connect(store => ({
  pathname: store.app.location.pathname,
}))(App);

export default {
  component: ConnectedApp, model,
};
