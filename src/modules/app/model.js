import { routerRedux } from 'dva/router';
import cloneDeep from 'lodash/cloneDeep';
import { credential } from "../../core/http";
import * as service from "./service";

const allMenus = [
  {
    pageId: 'summary-summary',
    icon: 'dashboard',
    text: '总览',
    href: '/',
  },
  {
    pageId: 'user',
    icon: 'user',
    text: '用户管理',
    subMenus: [
      { pageId: 'user-user_summary', text: '用户总览', href: '/user/manage' },
      { pageId: 'user-head_img', text: '头像管理', href: '/user/avatars' },
      { pageId: 'user-user_invite', text: '分销管理', href: '/user/distribution' },
    ]
  },
  {
    pageId: 'media',
    icon: 'table',
    text: '内容管理',
    subMenus: [
      { pageId: 'media-video', text: '视频管理', href: '/content/video' },
      { pageId: 'media-audio', text: '音频管理', href: '/content/audio' },
      { pageId: 'media-photo', text: '相册管理', href: '/content/photo' },
    ]
  },
  {
    pageId: 'category-category',
    icon: 'tags',
    text: '分类管理',
    href: '/category',
  },
  {
    pageId: 'operation',
    icon: 'setting',
    text: '运营管理',
    subMenus: [
      { pageId: 'operation-base', text: '基础设置', href: '/operation/manage' },
      { pageId: 'operation-config', text: '运营设置', href: '/operation/settings' },
    ]
  },
  {
    pageId: 'fund-fund',
    icon: 'pay-circle',
    text: '资金管理',
    href: '/finance',
  },
  {
    pageId: 'config-config',
    icon: 'sliders',
    text: '设置管理',
    href: '/settings',
  },
  {
    pageId: 'comment',
    icon: 'message',
    text: '评论管理',
    subMenus: [
      { pageId: 'comment-management', text: '评论管理', href: '/comments' },
      { pageId: 'comment-robot', text: '机器人管理', href: '/comments/robots' },
    ]
  },
  {
    pageId: 'account-account',
    icon: 'team',
    text: '账户管理',
    href: '/accounts',
  },
  {
    pageId: 'log-log',
    icon: 'read',
    text: '日志管理',
    href: '/logging',
  },
  {
    pageId: 'system-system',
    icon: 'apartment',
    text: '系统管理',
    href: '/system',
  },
];

function getMenusByPermission(permission) {
  return cloneDeep(allMenus).filter(m => {
    return permission.indexOf(m.pageId) > -1 || (
      m.subMenus && m.subMenus.some(sm => permission.indexOf(sm.pageId) > -1)
    );
  }).map(m => {
    let menuItem = m;
    if (menuItem.subMenus) {
      menuItem.subMenus = menuItem.subMenus.filter(sm => permission.indexOf(sm.pageId) > -1);
    }
    return menuItem;
  });
}

export default {
  namespace: 'app',

  state: {
    sideDisplay: 'unfold',
    menus: credential.getIsSuperUser() ? allMenus : getMenusByPermission(credential.getPermission()),
    location: {},
    userName: credential.getUserName(),
    token: '',
    adminId: '',
    authenticated: !!credential.getToken(),

    categoryList: [],
    tagList: [],
  },

  subscriptions: {
    setupRouteLocation({ history, dispatch }) {
      history.listen(({ pathname, search }) => {
        dispatch({ type: 'routeLocationChanged', payload: { location: { pathname, search } } });
      });
    }
  },

  effects: {
    *navigateTo({ payload: { pathname, query } }, { put }) {
      yield put(routerRedux.push({ pathname, query }));
    },
    *updateToken({ payload: { token, adminId, userName, isSuperUser, permission } }, { put }) {
      // Other credential storage solutions
      credential.setAdminId(adminId);
      credential.setToken(token);
      credential.setUserName(userName);
      credential.setIsSuperUser(isSuperUser);
      credential.setPermission(permission);
      yield put({ type: 'tokenStateChanged', payload: {
        token, adminId, userName,
        menus: credential.getIsSuperUser() ? allMenus : getMenusByPermission(permission)
      } });
    },
    *logout({ payload }, { put }) {
      credential.setAdminId('');
      credential.setToken('');
      credential.setUserName('');
      yield put({
        type: 'stateChanged',
        payload: {
          token: '',
          adminId: '',
          authenticated: false,
        },
      });
      yield put(routerRedux.push({
        pathname: '/login',
      }));
    },
    *fetchCategoryList({ payload: { form = {} } = {} }, { call, put }) {
      const result = yield call(service.fetchCategoryList, form);
      const mappedData = result.data.category_list.map((d, idx) => ({
        key: d.category_id,
        sn: idx + 1,
        categoryName: d.name,
        type: d.media_type,
        order: d.seq_no,
        quantity: d.album_count,
      }));
      yield put({ type: 'stateChanged', payload: { categoryList: mappedData } });
    },
    *fetchTagList({ payload: { form = {} } = {} }, { call, put }) {
      const result = yield call(service.fetchTagList, form);
      const mappedData = result.data.label_list.map((d, idx) => ({
        key: d.label_id,
        id: d.label_id,
        name: d.label_name,
        mediaType: d.media_type,
        mediaTypeName: d.media_type_txt,
        categoryId: d.category_id,
        categoryName: d.category_name,
      }));
      yield put({ type: 'stateChanged', payload: { tagList: mappedData } });
    }
  },

  reducers: {
    sideDisplayChange(state, { payload: { sideDisplay } }) {
      return { ...state, sideDisplay };
    },
    routeLocationChanged(state, { payload: { location } }) {
      return { ...state, location };
    },
    tokenStateChanged(state, { payload: { token, adminId, userName, menus } }) {
      return { ...state, token, adminId, userName, menus };
    },
    authenticateComplete(state, { payload: { authenticated } }) {
      return { ...state, authenticated };
    },
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    }
  }
}