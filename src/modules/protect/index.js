import React from "react";
import { Button } from 'antd';
import connectWithAuth from "../../core/connectWithAuth";

const ProtectedPage = (props) => {
  return (<div>
    <h1>This is a protected page.</h1>
  </div>);
}

const ConnectedProtectedPageWithAuth = connectWithAuth()(ProtectedPage);

export default {
  component: ConnectedProtectedPageWithAuth
};
