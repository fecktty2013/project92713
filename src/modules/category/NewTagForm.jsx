import React from "react";
import { Form, Input, Select, InputNumber } from "antd";
import { MEDIA_TYPE } from "../../common/constants";

const NewTagForm = (props) => {

  const { getFieldDecorator } = props.form;

  const { editingTag } = props.categoryManage;

  return (
    <Form layout="vertical">
      <Form.Item label="分类名称">
        {getFieldDecorator('tagCategory', {
          rules: [
            { required: true, message: '请输入分类名称' },
          ],
          initialValue: editingTag.tagCategoryId,
        })(
          <Select>
            {props.app.categoryList.map(c => (
              <Select.Option key={c.key} value={c.key}>{c.categoryName}</Select.Option>
            ))}
          </Select>
        )}
      </Form.Item>
      <Form.Item label="所属类型">
        {getFieldDecorator('tagType', {
          rules: [
            { required: true, message: '请选择所属类型' },
          ],
          initialValue: editingTag.tagTypeVal,
        })(
          <Select>
            <Select.Option value={MEDIA_TYPE.VIDEO}>视频</Select.Option>
            <Select.Option value={MEDIA_TYPE.AUDIO}>音频</Select.Option>
            <Select.Option value={MEDIA_TYPE.PHOTO}>相册</Select.Option>
          </Select>
        )}
      </Form.Item>
      <Form.Item label="标签名称">
        {getFieldDecorator('tagName', {
          rules: [
            { required: true, message: '请输入标签名称' },
          ],
          initialValue: editingTag.tagName,
        })(<Input />)}
      </Form.Item>
    </Form>
  );
}

export default NewTagForm;
