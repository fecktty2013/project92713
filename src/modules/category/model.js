import * as service from './service';

const initialState = {
  categoryList: [],
  categoryTotal: 0,
  editingCategory: {},
  categoryPagination: {
  },
  categoryModalVisibility: false,
  tagList: [],
  tagTotal: 0,
  editingTag: {},
  tagPagination: {
  },
  tagModalVisibility: false,
};

export default {
  namespace: 'categoryManage',

  state: initialState,

  subscriptions: {},

  effects: {
    *fetchCategoryList({ payload: { form = {}, pagination = {} } = {} }, { call, put }) {
      const result = yield call(service.fetchCategoryList, form, pagination);
      const mappedData = result.data.category_list.map((d, idx) => ({
        key: d.category_id,
        sn: idx + 1,
        categoryName: d.name,
        type: d.media_type,
        order: d.seq_no,
        quantity: d.album_count,
      }));
      yield put({ type: 'stateChanged', payload: { categoryList: mappedData, categoryTotal: result.data.total_count } });
    },
    *saveCategoryRow({ payload: { key, row } }, { put, select }) {
      const categoryState = yield select(state => state.categoryManage);
      const categoryList = categoryState.categoryList;
      const findIndex = categoryList.findIndex(c => c.key === key);
      if (findIndex > -1) {
        let originalItem = categoryList[findIndex];
        categoryList[findIndex] = { ...originalItem, ...row };
      }
      yield put({ type: 'stateChanged', payload: { categoryList } });
    },
    *deleteCategoryRow({ payload: { form } }, { call, put, select }) {
      yield call(service.deleteCategory, form);
    },
    *newCategoryStart({ payload: { editingCategory = {} } }, { put }) {
      console.log('editing category', editingCategory);
      yield put({ type: 'stateChanged', payload: { categoryModalVisibility: true, editingCategory } });
    },
    *newCategorySave({ payload: { form } }, { call, put, select }) {
      const categoryState = yield select(state => state.categoryManage);
      yield call(service.saveCategory, {
        category_id: categoryState.editingCategory.key,
        media_type: parseInt(form.type, 10),
        name: form.categoryName,
      });
      yield put({ type: 'stateChanged', payload: { categoryModalVisibility: false } });
      yield put({ type: 'fetchCategoryList' });
    },
    *setModalVisibility({ payload: { modalField, visibility } }, { put }) {
      const payload = {
        [modalField]: visibility
      };
      yield put({ type: 'stateChanged', payload });
    },
    *fetchTagList({ payload: { form, pagination } }, { call, put }) {
      const result = yield call(service.fetchTagList, form, pagination);
      const tagList = result.data.label_list.map((item, idx) => ({
        id: item.label_id,
        sn: idx + 1,  
        tagCategoryId: item.category_id,
        tagCategoryName: item.category_name,
        tagTypeVal: item.media_type,
        tagType: item.media_type_txt,
        tagName: item.label_name,
        tagOrder: item.seq_no,
        tagQuantity: 0,
      }));
      yield put({ type: 'stateChanged', payload: { tagList, tagTotal: result.data.total_count } });
    },
    *saveTagRow({ payload: { key, row } }, { put, select }) {
      const categoryState = yield select(state => state.categoryManage);
      const tagList = categoryState.tagList;
      const findIndex = tagList.findIndex(c => c.key === key);
      if (findIndex > -1) {
        let originalItem = tagList[findIndex];
        tagList[findIndex] = { ...originalItem, ...row };
      }
      yield put({ type: 'stateChanged', payload: { tagList } });
    },
    *deleteTagRow({ payload }, { put, select, call }) {
      console.log('delete tag', payload);
      const categoryState = yield select(state => state.categoryManage);
      const tagList = categoryState.tagList;
      yield call(service.deleteTag, {
        label_id: payload.id,
      });
    },
    *newTagStart({ payload: { editingTag = {} } = {} }, { put }) {
      console.log('editing tag', editingTag);
      yield put({ type: 'stateChanged', payload: { tagModalVisibility: true, editingTag } });
    },
    *newTagSave({ payload: { form } }, { put, select, call }) {
      const categoryState = yield select(state => state.categoryManage);
      yield call(service.saveTag, {
        label_id: categoryState.editingTag.id,
        label_name: form.tagName,
        media_type: parseInt(form.tagType, 10),
        category_id: form.tagCategory,
        seq_no: form.tagOrder,
      });
      yield put({ type: 'stateChanged', payload: { tagModalVisibility: false } });
    },
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    }
  }
}

