import React, { useEffect, useState } from "react"
import { Card, Button, Icon, Form, Table, Modal, Popconfirm, Select } from "antd";
import NewTagForm from "./NewTagForm";
import { MEDIA_TYPE } from "../../common/constants";

const TagTable = (props) => {

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const { getFieldDecorator, resetFields, validateFields } = props.form;

  const { editingTag } = props.categoryManage;

  const resetTagFields = () => {
    resetFields([
      "tagCategory", "tagType", "tagName", "tagOrder", "tagQuantity"
    ]);
  };

  const handleQuery = (p) => {
    validateFields((err, values) => {
      props.dispatch({
        type: 'categoryManage/fetchTagList',
        payload: {
          form: values,
          pagination: p || pagination,
        },
      });
    });
  };

  const handleDeleteRow = async (record) => {
    await props.dispatch({
      type: 'categoryManage/deleteTagRow', payload: record
    });
    handleQuery();
  };

  const tagColumns = [
    { title: '序号', dataIndex: 'sn' },
    { title: '分类名称', dataIndex: 'tagCategoryName' },
    { title: '所属类型', dataIndex: 'tagType' },
    { title: '标签名称', dataIndex: 'tagName' },
    { title: '排序', dataIndex: 'tagOrder' },
    { title: '数量', dataIndex: 'tagQuantity' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type="link" onClick={() => {
            resetTagFields();
            props.dispatch({
              type: 'categoryManage/newTagStart', payload: {
                editingTag: record,
              }
            });
          }}>编辑</Button>
          <Popconfirm title="确认删除吗？" onConfirm={() => handleDeleteRow(record)}>
            <a>删除</a>
          </Popconfirm>
        </span>
      )
    }
  ];

  const handleNewTagStart = (e) => {
    resetTagFields();
    props.dispatch({ type: 'categoryManage/newTagStart' });
  };

  const handleNewTagSave = (e) => {
    props.form.validateFields(async (err, values) => {
      console.log(err, values);
      await props.dispatch({ type: 'categoryManage/newTagSave', payload: { form: values } });
      handleQuery();
    });
  };

  const handleDeleteTagRow = (form, key) => {
    props.dispatch({
      type: 'categoryManage/deleteTagRow',
      payload: { key },
    });
  };

  const handleTableChanged = (pagination) => {
    setPagination(pagination);
    handleQuery(pagination);
  };

  const handleSaveTagRow = async (key, row, done) => {
    console.log(key, row);
    await props.dispatch({
      type: 'categoryManage/saveTagRow',
      payload: { key, row },
    });
    done();
  };

  useEffect(() => {
    handleQuery(pagination);
  }, []);

  return <React.Fragment>
    <Card title="标签设置"
      bordered={false}
      headStyle={{ padding: '0 24px 0 0' }}
      style={{ width: '100%' }}
      bodyStyle={{ paddingBottom: 0, paddingTop: 12 }}
      extra={
        <Button type="dashed" onClick={handleNewTagStart}>
          <Icon type="plus" />新增
        </Button>
      }
    >
      <Form layout="inline" style={{ marginBottom: 8 }}>
        <Form.Item label="所属类型">
          {getFieldDecorator('mediaType')(
            <Select style={{ width: 140 }} placeholder="请选择类型">
              <Select.Option value="all">所有</Select.Option>
              <Select.Option value={MEDIA_TYPE.VIDEO}>视频</Select.Option>
              <Select.Option value={MEDIA_TYPE.PHOTO}>相册</Select.Option>
              <Select.Option value={MEDIA_TYPE.AUDIO}>电台</Select.Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item>
          <Button onClick={() => handleQuery()} type="primary">查询</Button>
        </Form.Item>
      </Form>
      <Table
        size="middle"
        columns={tagColumns}
        dataSource={props.categoryManage.tagList}
        rowKey={record => record.id}
        onChange={handleTableChanged}
        pagination={{
          ...pagination,
          showSizeChanger: true,
          defaultPageSize: 5,
          total: props.categoryManage.tagTotal,
          showTotal: () => <span>总共 {props.categoryManage.tagTotal} 条记录</span>,
          pageSizeOptions: ['5', '10', '20'],
        }}
      />
    </Card>
    <Modal
      visible={props.categoryManage.tagModalVisibility}
      title={!!editingTag?.id ? '编辑标签' : '新增标签'}
      okText="确认"
      cancelText="取消"
      onOk={handleNewTagSave}
      onCancel={() => props.dispatch({
        type: 'categoryManage/setModalVisibility',
        payload: {
          modalField: 'tagModalVisibility',
          visibility: false,
        }
      })}
    >
      <NewTagForm {...props} />
    </Modal>
  </React.Fragment>
};

export default Form.create()(TagTable);
