import React, { useEffect, useState } from "react"
import { Card, Button, Icon, Form, Table, Select, Modal, Popconfirm } from "antd";
import NewCategoryForm from "./NewCategoryForm";

import style from "./style.scss";
import { MEDIA_TYPE } from "../../common/constants";

const CategoryTable = (props) => {

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const { getFieldDecorator, resetFields, validateFields } = props.form;

  const handleDeleteRow = async (form) => {
    await props.dispatch({
      type: 'categoryManage/deleteCategoryRow',
      payload: { form },
    });
    handleQuery();
  };

  const categoryColumns = [
    { title: '序号', dataIndex: 'sn' },
    { title: '分类名称', dataIndex: 'categoryName' },
    {
      title: '所属类型',
      key: 'mediaType',
      render: (text, record) => {
        let typeDesc = '';
        if (record.type == '1')
          typeDesc = '视频';
        else if (record.type == '2')
          typeDesc = '相册';
        else if (record.type == '3')
          typeDesc = '音频';
        return <span>{typeDesc}</span>;
      }

    },
    { title: '排序', dataIndex: 'order' },
    { title: '数量', dataIndex: 'quantity' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type="link" onClick={() => {
            resetFields(["categoryName", "type"]);
            props.dispatch({
              type: 'categoryManage/newCategoryStart', payload: {
                editingCategory: record,
              }
            });
          }}>编辑</Button>
          <Popconfirm title="确认删除吗？" onConfirm={() => handleDeleteRow(record)}>
            <a>删除</a>
          </Popconfirm>
        </span>
      )
    }
  ];

  const handleNewCategoryStart = (e) => {
    props.dispatch({ type: 'categoryManage/newCategoryStart', payload: {} });
  };

  const handleNewCategorySave = (e) => {
    props.form.validateFields(async (err, values) => {
      console.log('new category save', values);
      if (err) {
        console.log('validation failed');
        return;
      }
      await props.dispatch({ type: 'categoryManage/newCategorySave', payload: { form: values } });
      await props.dispatch({
        type: 'categoryManage/setModalVisibility',
        payload: {
          modalField: 'categoryModalVisibility',
          visibility: false,
        }
      });
      handleQuery();
    });
  };

  const handleNewCategoryCancel = (e) => {
    props.dispatch({
      type: 'categoryManage/setModalVisibility',
      payload: {
        modalField: 'categoryModalVisibility',
        visibility: false,
      }
    });
  };

  const handleNewCategoryClose = (e) => {
    props.form.resetFields([
      "categoryName", "order", "quantity", "type"
    ]);
  };

  const handleSaveRow = async (key, row, done) => {
    console.log(key, row);
    await props.dispatch({
      type: 'categoryManage/saveCategoryRow',
      payload: { key, row },
    });
    done();
  };

  const handleQuery = (p) => {
    validateFields((err, values) => {
      props.dispatch({
        type: 'categoryManage/fetchCategoryList',
        payload: {
          form: values,
          pagination: p || pagination
        },
      });
    });
  };

  useEffect(() => {
    handleQuery(pagination);
  }, []);

  const handleTableChanged = (pagination) => {
    setPagination(pagination);
    handleQuery(pagination);
  };

  return <React.Fragment>
    <Card title="分类设置"
      bordered={false}
      headStyle={{ padding: '0 24px 0 0' }}
      style={{ width: '100%' }}
      bodyStyle={{ paddingBottom: 0, paddingTop: 12 }}
      extra={
        <Button type="dashed" onClick={handleNewCategoryStart}>
          <Icon type="plus" />新增
        </Button>
      }
    >
      <Form layout="inline" style={{ marginBottom: 8 }}>
        <Form.Item label="所属类型">
          {getFieldDecorator('mediaType')(
            <Select style={{ width: 140 }} placeholder="请选择类型">
              <Select.Option value="all">所有</Select.Option>
              <Select.Option value={MEDIA_TYPE.VIDEO}>视频</Select.Option>
              <Select.Option value={MEDIA_TYPE.PHOTO}>相册</Select.Option>
              <Select.Option value={MEDIA_TYPE.AUDIO}>电台</Select.Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item>
          <Button onClick={() => handleQuery()} type="primary">查询</Button>
        </Form.Item>
      </Form>
      <Table
        size="middle"
        columns={categoryColumns}
        dataSource={props.categoryManage.categoryList}
        rowKey={record => record.key}
        onChange={handleTableChanged}
        pagination={{
          ...pagination,
          showSizeChanger: true,
          defaultPageSize: 5,
          total: props.categoryManage.categoryTotal,
          showTotal: () => <span>总共 {props.categoryManage.categoryTotal} 条记录</span>,
          pageSizeOptions: ['5', '10', '20'],
        }}
      />
    </Card>
    <Modal
      visible={props.categoryManage.categoryModalVisibility}
      title={!!props.categoryManage.editingCategory.key ? '编辑分类' : '新增分类'}
      okText="确认"
      cancelText="取消"
      onOk={handleNewCategorySave}
      onCancel={handleNewCategoryCancel}
      afterClose={handleNewCategoryClose}
    >
      <NewCategoryForm {...props} />
    </Modal>
  </React.Fragment>
};

export default Form.create()(CategoryTable);
