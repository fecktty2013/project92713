import $http from "../../core/http";

export function fetchCategoryList(form, pagination) {
  return $http.get("/media/category/list", {
    params: {
      media_type: form.mediaType === 'all' ? undefined : form.mediaType,
      page_no: (pagination.current - 1) || 0,
      page_size: pagination.pageSize || 5,
    }
  });
}

export function saveCategory(form) {
  return $http.post("/media/category", form);
}

export function deleteCategory(form) {
  return $http.delete("/media/category", {
    params: {
      category_id: form.key
    }
  });
}

export function fetchTagList(form, pagination) {
  let payload = {};
  if (form.mediaType && form.mediaType !== 'all') {
    payload.media_type = form.mediaType;
  }
  return $http.get("/media/label/list", {
    params: {
      ...payload,
      page_no: (pagination.current - 1) || 0,
      page_size: pagination.pageSize || 5,
    },
  });
}

export function saveTag(form) {
  return $http.post("/media/label", form);
}

export function deleteTag(form) {
  return $http.delete("/media/label", {
    params: form
  });
}