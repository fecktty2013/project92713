import React, { useEffect } from "react";
import connectWithAuth from "../../core/connectWithAuth";
import { Breadcrumb, Card, Button, Icon, Modal } from "antd";
import CategoryTable from "./CategoryTable";
import TagTable from "./TagTable";

import style from "./style.scss";

const CategoryPage = (props) => {

  const queryCategory = () => {
    props.dispatch({
      type: 'app/fetchCategoryList',
      payload: {
        form: { mediaType: undefined }
      }
    })
  };

  useEffect(() => {
    queryCategory();
  }, []);

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>分类管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <CategoryTable {...props} />
        <TagTable {...props} />
      </div>
    </div>
  );
};

export default connectWithAuth(({ app, categoryManage }) => ({ app, categoryManage }))(CategoryPage);
// export default CategoryPage;
