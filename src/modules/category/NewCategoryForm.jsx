import React from "react";
import { Form, Input, Select, InputNumber } from "antd";

const NewCategoryForm = (props) => {

  const { getFieldDecorator } = props.form;

  const { categoryName, type, order, quantity } = props.categoryManage.editingCategory;

  return (
    <Form layout="vertical">
      <Form.Item label="分类名称">
        {getFieldDecorator('categoryName', {
          rules: [
            { required: true, message: '请输入分类名称' },
          ],
          initialValue: categoryName,
        })(<Input />)}
      </Form.Item>
      <Form.Item label="所属类型">
        {getFieldDecorator('type', {
          rules: [
            { required: true, message: '请选择所属类型'},
          ],
          initialValue: type ? String(type) : undefined,
        })(
          <Select>
            <Select.Option value="1">视频</Select.Option>
            <Select.Option value="2">相册</Select.Option>
            <Select.Option value="3">音频</Select.Option>
          </Select>
        )}
      </Form.Item>
    </Form>
  );
}

export default NewCategoryForm;
