import React from "react";
import ModifyPasswordPage from "./ModifyPasswordPage";
import model from "./model";

export default {
  component: ModifyPasswordPage, model,
};
