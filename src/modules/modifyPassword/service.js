import $http from "../../core/http";

export function modifyPassword(originPwd, newPwd) {
  return $http.post("/auth/password", {
    origin_pwd: originPwd,
    new_pwd: newPwd,
  });
}