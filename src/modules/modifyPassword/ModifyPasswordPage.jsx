import React from "react";
import { connect } from "dva";
import { routerRedux } from "dva/router";
import { Layout, Form, Icon, Input, Button, Avatar, message } from "antd";
import style from "./style.scss";

const { Header, Content } = Layout;

const ModifyPasswordPage = (props) => {

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields(async (err, values) => {
      if (!err) {
        let modifyPasswordState = await props.dispatch({ type: 'modifyPassword/doUpdate', payload: values });
        if (!modifyPasswordState.success) {
          message.error(modifyPasswordState.errorMsg);
        } else {
          props.dispatch(routerRedux.push("/login"));
        }
      }
    });
  };

  const validatePassword = (rule, value, callback) => {
    const { getFieldValue } = props.form;
    if (value && value === getFieldValue('originalPassword')) {
      callback('新密码不允许和旧密码重复');
    }
    callback();
  };

  const validatePasswordConfirmed = (rule, value, callback) => {
    const { getFieldValue } = props.form;
    if (value && value !== getFieldValue('newPassword')) {
      callback('两次密码输入不一致');
    }
    callback();
  };

  const { getFieldDecorator } = props.form;
  return (
    <div className={style.container}>
      <Layout className={style.layout}>
        <Header className={style.header}>修改密码</Header>
        <Content className={style.content}>
          <Avatar icon="idcard" size="large" style={{ backgroundColor: '#f50057', marginBottom: 12 }} />
          <Form className={style.form} onSubmit={handleSubmit}>
            <Form.Item className={style.formItem}>
              {getFieldDecorator('originalPassword', {
                rules: [{ required: true, message: '原始密码不允许为空' }],
              })(
                <Input.Password className={style.input} size="large"
                  placeholder="请输入原始密码！"
                />,
              )}
            </Form.Item>
            <Form.Item className={style.formItem}>
              {getFieldDecorator('newPassword', {
                rules: [
                  { required: true, message: '新密码不允许为空' },
                  { validator: validatePassword }
                ],
              })(
                <Input.Password className={style.input} size="large"
                  placeholder="请输入新密码，不要和原始密码重复"
                />,
              )}
            </Form.Item>
            <Form.Item className={style.formItem}>
              {getFieldDecorator('newPasswordConfirmed', {
                rules: [
                  { required: true, message: '新密码不允许为空' },
                  { validator: validatePasswordConfirmed }
                ],
              })(
                <Input.Password className={style.input} size="large"
                  placeholder="请再次输入新密码"
                />,
              )}
            </Form.Item>
            <Form.Item style={{ textAlign: 'center' }}>
              <Button type="primary" size="large" htmlType="submit" className={style.btn}>
                确认修改
              </Button>
            </Form.Item>
          </Form>
        </Content>
      </Layout>
    </div>
  );
}

export default connect()(Form.create()(ModifyPasswordPage));
