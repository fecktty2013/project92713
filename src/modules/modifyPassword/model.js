import { routerRedux } from 'dva/router';
import * as service from './service';

export default {
  namespace: 'modifyPassword',

  state: {
    success: false,
    errorMsg: '',
  },

  subscriptions: {
    
  },

  effects: {
    *doUpdate({ payload: { originalPassword, newPassword } }, { call, put, select }) {
      const result = yield call(service.modifyPassword, originalPassword, newPassword);

      const success = result.errNo === 0;
      const errorMsg = result.errMsg;

      if (success) {
        yield put({ type: 'app/updateToken', payload: {
          token: result.data?.access_token,
          adminId: result.data?.admin_id,
          userName: result.data?.username,
        }})
      }

      yield put({ type: 'modifyPasswordComplete', payload: { success, errorMsg } });

      return yield select(state => state.modifyPassword);
    },
  },

  reducers: {
    modifyPasswordComplete(state, { payload: { success, errorMsg } }) {
      return { ...state, success, errorMsg };
    },
  }
}