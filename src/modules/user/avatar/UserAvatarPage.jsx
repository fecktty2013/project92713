import React, { useEffect, useState } from "react";
import connectWithAuth from "../../../core/connectWithAuth";
import { Breadcrumb, List, Icon, Button, Radio, Row, Col, Modal, Upload, Popconfirm, Form, message } from "antd";
import style from "./style.scss";

const UserAvatarPage = (props) => {

  const { getFieldDecorator, validateFields, resetFields } = props.form;

  const { avatarList, avatarTotal, visibility, uploadingImg } = props.userAvatar;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 10,
  });

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const handleQuery = () => {
    props.dispatch({
      type: 'userAvatar/fetchAvatarList',
      pagination,
    });
  };

  const normFile = e => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  const handleUploadStart = (e) => {
    props.dispatch({
      type: 'userAvatar/addAvatarStart',
      pagination,
    });
  };

  const handleImgBeforeUpload = (file) => {
    let reader = new FileReader();
    reader.addEventListener('load', () => {
      props.dispatch({
        type: 'userAvatar/uploadImg',
        payload: {
          uploadingImg: { uid: file.uid, name: file.name, status: 'done', url: reader.result, file }
        }
      });
    });
    reader.readAsDataURL(file);
    return false;
  };

  const handleUploadComplete = (e) => {
    // 
  };

  const handleUploadCancel = (e) => {
    props.dispatch({
      type: 'userAvatar/setModalVisibility',
      visibility: false,
    });
  };

  const handleUploadSubmit = (e) => {
    if (!uploadingImg.url) {
      message.error('还没有上传图片，请先上传图片');
      return;
    }
    validateFields(async (err, values) => {
      if (err) {
        console.log(err);
        return;
      }
      await props.dispatch({
        type: 'userAvatar/saveAvatar',
        payload: values,
      });
      await props.dispatch({
        type: 'userAvatar/setModalVisibility',
        payload: { visibility: false },
      });
      handleQuery();
    });
  };

  const handleDelete = async (id) => {
    await props.dispatch({
      type: 'userAvatar/deleteAvatar',
      payload: { id },
    });
    handleQuery();
  };

  useEffect(handleQuery, [pagination]);

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>用户管理</Breadcrumb.Item>
          <Breadcrumb.Item>头像管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <Row>
          <Col>
            <Button type="primary" style={{ width: 120, marginBottom: 16 }} onClick={handleUploadStart}>增加</Button>
          </Col>
        </Row>
        <List
          grid={{ gutter: 16, column: 6, lg: 8 }}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            onChange: (p, s) => {
              console.log(p, s);
              setPagination({ current: p, pageSize: s});
            },
            onShowSizeChange: (p, s) => {
              console.log(p, s);
              setPagination({ current: p, pageSize: s});
            },
            total: avatarTotal,
            showTotal: () => <span>总共 {avatarTotal} 条记录</span>,
            pageSizeOptions: ['10', '20', '30'],
          }}
          dataSource={avatarList}
          renderItem={item => (
            <List.Item>
              <div key={item.id} className={style.listItem}
                style={{ backgroundImage: `url(${item.url})`, backgroundSize: 'cover' }}>
                <div className={style.overlay} />
                <Popconfirm
                  title="确认删除吗？"
                  onConfirm={() => handleDelete(item.id)}
                  okText="确认"
                  cancelText="取消"
                >
                  <Icon type="close-circle" theme="outlined" className={style.badge} />
                </Popconfirm>
              </div>
            </List.Item>
      )}
    />
        <Modal
          title="新增用户头像"
          width={400}
          visible={visibility}
          bodyStyle={{
            paddingLeft: 80
          }}
          onOk={handleUploadSubmit}
          onCancel={handleUploadCancel}
        >
          <Form.Item label="头像图片">
            {getFieldDecorator('editImg', {
              valuePropName: 'fileList',
              getValueFromEvent: normFile,
            })(
              <Upload name="editImg"
                listType="picture-card"
                className={style.imgUploader}
                showUploadList={false}
                action={null}
                accept={null}
                withCredentials
                beforeUpload={handleImgBeforeUpload}
                onChange={handleUploadComplete}
              >
                {uploadingImg && uploadingImg.previewUrl ?
                  <img src={uploadingImg.previewUrl} />
                  :
                  <div>
                    <Icon type="plus" />
                    <div className="ant-upload-text">上传</div>
                  </div>
                }
              </Upload>
            )}
          </Form.Item>
          <Form.Item label="性别">
            {getFieldDecorator('editSex', {
              initialValue: "0",
            })(
              <Radio.Group>
                <Radio.Button value="1">男</Radio.Button>
                <Radio.Button value="2">女</Radio.Button>
                <Radio.Button value="0">未知</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>
        </Modal>
      </div>
    </div>
  );
}

export default connectWithAuth(({ userAvatar }) => ({ userAvatar }))(Form.create()(UserAvatarPage));
