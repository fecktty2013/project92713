import React from "react";
import UserAvatarPage from "./UserAvatarPage";
import model from "./model";

export default {
  component: UserAvatarPage, model,
};
