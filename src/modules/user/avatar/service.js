import $http, { APP_BASE_URL } from "../../../core/http";

export function fetchAvatarList(payload) {
  return $http.get("/user/head_img/list", {
    params: payload,
  });
}

export function addNewAvatar(payload) {
  return $http.post("/user/head_img", payload);
}

export function deleteAvatar(payload) {
  return $http.delete("/user/head_img", {
    params: payload,
  });
}

export function uploadImage(file) {
  const formData = new FormData();
  formData.append("file", file);

  return $http.post("/api/upload_img", formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    'baseURL': APP_BASE_URL,
  });
}

export function fetchAdvertList(payload) {
  return $http.get("/operation/advert/list", {
    params: payload,
  });
}
