import { routerRedux } from 'dva/router';
import * as service from './service';

const initialState = {
  avatarList: [],
  avatarTotal: 0,

  visibility: false,

  uploadingImg: {},
};

export default {
  namespace: 'userAvatar',

  state: initialState,

  subscriptions: {
    setup({ history }) {
    }
  },

  effects: {
    *uploadImg({ payload: { uploadingImg } }, { call, put }) {
      const uploadResult = yield call(service.uploadImage, uploadingImg.file);
      yield put({
        type: 'stateUpdated', payload: {
          uploadingImg: {
            ...uploadingImg,
            url: uploadResult.data.url,
            previewUrl: uploadingImg.url,
          },
        }
      });
    },
    *addAvatarStart({ payload }, { put }) {
      yield put({
        type: 'stateUpdated', payload: {
          uploadingImg: {},
          visibility: true,
        }
      });
    },
    *fetchAvatarList({ pagination }, { put, call, select }) {
      const httpResult = yield call(service.fetchAvatarList, {
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const avatarList = httpResult.data.head_img_list.map((item, idx) => ({
        id: item.head_id,
        url: item.url,
      }));
      const avatarTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: { avatarList, avatarTotal } });
    },
    *saveAvatar({ payload }, { call, put, select }) {
      const avatarState = yield select(state => state.userAvatar);
      const httpRequest = {
        sex: parseInt(payload.editSex, 10),
        url: avatarState.uploadingImg.url,
      }
      yield call(service.addNewAvatar, httpRequest);
    },
    *deleteAvatar({ payload }, { call, put }) {
      yield call(service.deleteAvatar, { head_img_id: payload.id });
    },
    *setModalVisibility({ visibility }, { put }) {
      yield put({ type: 'stateUpdated', payload: { visibility }});
    }
  },

  reducers: {
    stateUpdated(state, { payload }) {
      return { ...state, ...payload };
    }
  }
}