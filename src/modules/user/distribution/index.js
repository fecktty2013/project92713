import React from "react";
import UserDistributionPage from "./UserDistributionPage";
import model from "./model";

export default {
  component: UserDistributionPage, model,
};
