import React, { useEffect, useState } from "react";
import connectWithAuth from "../../../core/connectWithAuth";
import { Breadcrumb, Select, Form, Card, Input, Table } from "antd";
import style from "./style.scss";

const userColumns = [
  {
    title: '序号',
    dataIndex: 'sn',
  },
  {
    title: '名称(ID)',
    render: (text, record) => (
      <span>
        <span>{record.userName}</span>
        <span> ({record.userId})</span>
      </span>
    )
  },
  {
    title: '上一级名称(ID)',
    render: (text, record) => (
      <span>
        <span>{record.parentUserName}</span>
        <span> ({record.parentId})</span>
      </span>
    )
  },
];

const revenueColumns = [
  {
    title: '序号',
    dataIndex: 'sn',
  },
  {
    title: '名称(ID)',
    render: (text, record) => (
      <span>
        <span>{record.userName}</span>
        <span> ({record.id})</span>
      </span>
    )
  },
  {
    title: '一级收益',
    dataIndex: 'revenue1',
  },
  {
    title: '二级收益',
    dataIndex: 'revenue2',
  },
  {
    title: '三级收益',
    dataIndex: 'revenue3',
  },
  {
    title: '总收益',
    dataIndex: 'totalRevenue',
  },
]

const UserDistributionPage = (props) => {
  const { getFieldDecorator, getFieldValue } = props.form;

  const [pagination, setPagination] = useState({
    current: 1, pageSize: 5,
  });

  const [pagination2, setPagination2] = useState({
    current: 1, pageSize: 5,
  });

  const handleTableChanged = (p) => {
    setPagination(p);
  };

  const handleTableChanged2 = (p) => {
    setPagination2(p);
  };
  
  const handleSearchUser = (e) => {
    props.dispatch({
      type: 'userDistribution/fetchDistUserList',
      form: {
        searchKey1: getFieldValue('searchKey1'),
        searchValue1: e,
      },
      pagination,
    });
  };

  const handleSearchRevenue = (e) => {
    props.dispatch({
      type: 'userDistribution/fetchDistRevenueList',
      form: {
        searchKey2: getFieldValue('searchKey2'),
        searchValue2: e,
      },
      pagination: pagination2,
    });
  };

  useEffect(() => {
    handleSearchUser();
  }, [pagination]);

  useEffect(() => {
    handleSearchRevenue();
  }, [pagination2]);

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>用户管理</Breadcrumb.Item>
          <Breadcrumb.Item>分销管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <Card title="分销关系"
          bordered={false}
          headStyle={{ padding: 0 }}
          style={{ width: '100%' }}
          bodyStyle={{ paddingBottom: 0, paddingTop: 12 }}
        >
          <Form layout="inline" style={{ marginBottom: 8 }}>
            <Form.Item>
              {getFieldDecorator('searchKey1', {
                initialValue: 'id',
              })(
                <Select style={{ width: 80 }}>
                  <Select.Option value="id">ID</Select.Option>
                  <Select.Option value="nickName">昵称</Select.Option>
                  <Select.Option value="phoneNumber">手机号</Select.Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item>
              <Input.Search onSearch={handleSearchUser} enterButton="上级搜索" />
            </Form.Item>
          </Form>
          <Table columns={userColumns} rowKey={record => record.sn}
            dataSource={props.userDistribution.distUserList}
            onChange={handleTableChanged}
            pagination={{
              ...pagination,
              showSizeChanger: true,
              showQuickJumper: true,
              defaultPageSize: 5,
              total: props.userDistribution.distUserTotal,
              showTotal: () => <span>总共 {props.userDistribution.distUserTotal} 条记录</span>,
              pageSizeOptions: ['5', '10', '20'],
            }} />
        </Card>
        <Card title="分销收益"
          bordered={false}
          headStyle={{ padding: 0 }}
          style={{ width: '100%' }}
          bodyStyle={{ paddingBottom: 0, paddingTop: 12 }}
        >
          <Form layout="inline" style={{ marginBottom: 8 }}>
            <Form.Item>
              {getFieldDecorator('searchKey2', {
                initialValue: 'id',
              })(
                <Select style={{ width: 80 }}>
                  <Select.Option value="id">ID</Select.Option>
                  <Select.Option value="nickName">昵称</Select.Option>
                  <Select.Option value="phoneNumber">手机号</Select.Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item>
              <Input.Search onSearch={handleSearchRevenue} enterButton="查询" />
            </Form.Item>
          </Form>
          <Table columns={revenueColumns} rowKey={record => record.sn}
            onChange={handleTableChanged2}
            dataSource={props.userDistribution.distRevenueList} pagination={{
              ...pagination2,
              showSizeChanger: true,
              showQuickJumper: true,
              defaultPageSize: 5,
              total: props.userDistribution.distRevenueTotal,
              showTotal: () => <span>总共 {props.userDistribution.distRevenueTotal} 条记录</span>,
              pageSizeOptions: ['5', '10', '20'],
            }} />
        </Card>
      </div>
    </div>
  );
}

export default connectWithAuth(({ userDistribution }) => ({ userDistribution }))(Form.create()(UserDistributionPage));
