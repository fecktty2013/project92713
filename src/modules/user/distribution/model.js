import { routerRedux } from 'dva/router';
import * as service from './service';

const mockDistRevenueList = [
  {
    sn: '1',
    id: '500',
    revenue1: 100,
    revenue2: 200,
    revenue3: 300,
    totalRevenue: 600,
  },
];

const initialState = {
  distUserList: [],
  distUserTotal: 0,

  distRevenueList: [],
  distRevenueTotal: 0,
};

export default {
  namespace: 'userDistribution',

  state: initialState,

  subscriptions: {
    setup({ history }) {
      // return history.mapRouteToAction([
      //   { path: '/user/distribution', action: 'fetchAll' },
      // ]);
    }
  },

  effects: {
    *fetchAll(action, { put }) {
      yield put({ type: 'fetchDistRevenueList', pagination: { current: 1, pageSize: 5 } });
    },
    *fetchDistUserList({ form = {}, pagination = {} }, { put, select, call }) {
      const mappingKeys = {
        'id': 'user_id',
        'nickName': 'username',
        'phoneNumber': 'phone',
      };

      let httpRequest = {
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      };
      if (form.searchKey1 && mappingKeys[form.searchKey1] && form.searchValue1) {
        httpRequest[mappingKeys[form.searchKey1]] = form.searchValue1;
      }

      const httpResult = yield call(service.fetchDistUserList, httpRequest);

      const distUserList = httpResult.data.retail_list.map((itm, idx) => ({
        sn: idx + 1,
        id: itm.invitation_id,
        userId: itm.user_id,
        userName: itm.username,
        parentId: itm.inviter,
        parentUserName: itm.inviter_username,
      }));
      const distUserTotal = httpResult.data.total_count;

      yield put({
        type: 'stateUpdated',
        payload: {
          distUserList, distUserTotal,
        }
      });
    },
    *fetchDistRevenueList({ form = {}, pagination = {} }, { put, call, select }) {
      const mappingKeys = {
        'id': 'user_id',
        'nickName': 'username',
        'phoneNumber': 'phone',
      };

      let httpRequest = {
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      };
      if (form.searchKey2 && mappingKeys[form.searchKey2] && form.searchValue2) {
        httpRequest[mappingKeys[form.searchKey2]] = form.searchValue2;
      }

      const httpResult = yield call(service.fetchDistRevenueList, httpRequest);

      const distRevenueList = httpResult.data.invite_coin_list.map((itm, idx) => ({
        sn: idx + 1,
        id: itm.user_id,
        userName: itm.username,
        revenue1: itm.lv1_coin,
        revenue2: itm.lv2_coin,
        revenue3: itm.lv3_coin,
        totalRevenue: itm.total_coin,
      }));
      const distRevenueTotal = httpResult.data.total_count;

      yield put({
        type: 'stateUpdated',
        payload: {
          distRevenueList, distRevenueTotal,
        }
      });
    }
  },

  reducers: {
    stateUpdated(state, { payload }) {
      return { ...state, ...payload };
    }
  }
}