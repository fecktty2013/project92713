import $http, { APP_BASE_URL } from "../../../core/http";

export function fetchDistUserList(payload) {
  return $http.get("/user/spread/list", {
    params: payload,
  });
}

export function fetchDistRevenueList(payload) {
  return $http.get("/user/invite_coin/list", {
    params: payload,
  })
}
