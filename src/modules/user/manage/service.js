import $http, { APP_BASE_URL } from "../../../core/http";

export function fetchUserList(payload) {
  return $http.get("/user/list", {
    params: payload,
  });
}

export function deleteUser(payload) {
  return $http.delete("/user", {
    params: payload,
  });
}

export function fetchUserLogList(payload) {
  return $http.get("/user/login/list", {
    params: payload,
  });
}


