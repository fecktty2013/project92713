import React from "react";
import { Link } from "dva/router";
import { Button, Icon, Card, Table } from "antd";
import style from "./style.scss";

const columns = [
  {
    title: 'id',
    dataIndex: 'id',
  },
  {
    title: '登录日期',
    dataIndex: 'loginTime',
  },
  {
    title: '登录ip',
    dataIndex: 'loginIp',
  },
  {
    title: '设备',
    dataIndex: 'device',
  },
];

const UserLogRecord = (props) => {

  return (
    <Card title="登录记录"
      bordered={false}
      headStyle={{ padding: 0 }}
      style={{ width: '100%' }}
    >
      <Table columns={columns} rowKey={record => record.id}
        scroll={{ x: 'max-content' }}
        dataSource={props.userManage.userLogList}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          defaultPageSize: 5,
          showTotal: () => <span>总共 {props.userManage.userLogPagination.total} 条记录</span>,
          pageSizeOptions: ['5', '10', '20'],
        }}
      />
    </Card>
  );
}

export default UserLogRecord;
