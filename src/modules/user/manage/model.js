import { routerRedux } from 'dva/router';
import * as service from './service';

const initialState = {
  userList: [],
  userTotal: 0,
  editingUser: {},

  pagination: { current: 1, pageSize: 5 },

  userLogList: [],
  userLogTotal: 0,
};

export default {
  namespace: 'userManage',

  state: initialState,

  subscriptions: {
    setup({ history }) {
      // return history.mapRouteToAction([
      //   { path: '/user/manage', action: 'fetchUserList' },
      // ]);
    }
  },

  effects: {
    *fetchUserList({ form = {}, pagination = { current: 1, pageSize: 5} }, { put, call }) {
      let httpRequest = {
        register_begin: form.registerDateRange && form.registerDateRange[0].format('YYYY-MM-DD'),
        register_end: form.registerDateRange && form.registerDateRange[1].format('YYYY-MM-DD'),
        sex: form.gender === 'all' ? undefined : form.gender,
        status: form.state === 'all' ? undefined : form.state,
        active_type: form.activityLevel,
        charge_low: form.totalCharge?.start,
        charge_high: form.totalCharge?.end,
        consume_low: form.totalConsume?.start,
        consume_high: form.totalConsume?.end,
        coin_low: form.coinCount?.start,
        coin_high: form.coinCount?.end,
        profit_low: form.totalRevenue?.start,
        profit_high: form.totalRevenue?.end,
        video_low: form.videoCount?.start,
        video_high: form.videoCount?.end,
        audio_low: form.podcastCount?.start,
        audio_high: form.podcastCount?.end,
        photo_low: form.albumCount?.start,
        photo_high: form.albumCount?.end,
        comment_low: form.commentCount?.start,
        comment_high: form.commentCount?.end,
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      };
      let searchMappings = {
        'id': 'user_id',
        'nickName': 'username',
        'phoneNumber': 'phone'
      };
      if (form.queryBy && searchMappings[form.queryBy]) {
        httpRequest[searchMappings[form.queryBy]] = form.queryValue;
      }
      const httpResult = yield call(service.fetchUserList, httpRequest);
      const userList = httpResult.data.user_list.map((itm, idx) => ({
        id: itm.user_id,
        nickName: itm.username,
        phoneNumber: itm.phone,
        gender: itm.sex,
        state: itm.status_zh,
        stateVal: itm.status,
        coinAmount: itm.coin_amount,
        totalRevenue: itm.profit_num,
        totalConsume: itm.consume_num,
        videoCount: itm.video_num,
        albumCount: itm.photo_num,
        podcastCount: itm.audio_num,
        commentCount: itm.comment_num,
        registerTime: itm.register_time,
        lastLoginTime: itm.last_login_time,
      }));
      const userTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { userList, userTotal, pagination } });
    },
    *deleteUser({ payload }, { put, call }) {
      yield call(service.deleteUser, {
        user_id: payload.id,
      });
    },
    *fetchLogList({ form, pagination }, { put, call }) {
      const userLogResult = {
        data: [
          { id: '67890', loginTime: '2019/01/01/ 13:01:02', loginIp: '162.356.353.22', device: '手机型号 品牌  IMEI 等信息'},
          { id: '108783', loginTime: '2019/01/01/ 13:01:02', loginIp: '162.356.353.22', device: '手机型号 品牌  IMEI 等信息'},
          { id: '54321', loginTime: '2019/01/01/ 13:01:02', loginIp: '162.356.353.22', device: '手机型号 品牌  IMEI 等信息'},
        ],
        total: 223,
      };
      const httpResult = service.fetchUserLogList({
        user_id: form.id,
      });
    }
  },

  reducers: {
    userListUpdated(state, { payload: { userList } }) {
      return { ...state, userList };
    },
    userListPaginationUpdated(state, { payload: { userListPagination } }) {
      return { ...state, userListPagination };
    },
    userLogListUpdated(state, { payload: { userLogList }}) {
      return { ...state, userLogList };
    },
    userLogPaginationUpdated(state, { payload: { userLogPagination }}) {
      return { ...state, userLogPagination };
    },
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    }
  }
}