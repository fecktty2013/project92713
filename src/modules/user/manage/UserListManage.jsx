import React, { useEffect, useState } from "react";
import { Link } from "dva/router";
import { Button, Icon, Card, Table, Modal, Form, Input, Radio, Popconfirm } from "antd";
import style from "./style.scss";

const UserListManage = (props) => {

  const [newUserDisplay, setNewUserDisplay] = useState(false);

  const { pagination } = props.userManage;

  const eventHandlers = {
    handleNewUserEvent(e) {
      setNewUserDisplay(true);
    },
    handleCancelNewUserEvent(e) {
      setNewUserDisplay(false);
    },
    handleNewUserSubmit(e) {
      props.form.validateFields((err, values) => {
        if (!err) {
          console.log(values);
          setNewUserDisplay(false);
          props.form.resetFields();
        }
      })
    },
    async handleUserDelete(record) {
      await props.dispatch({
        type: 'userManage/deleteUser',
        payload: record,
      });
      handleQuery();
    }
  };

  const validators = {
    validatePasswordConfirmed(rule, value, callback) {
      const { getFieldValue } = props.form;
      if (value && value !== getFieldValue('password')) {
        callback('两次密码输入不一致');
      }
      callback();
    }
  }

  const { getFieldDecorator } = props.form;
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const columns = [
    {
      title: 'id',
      dataIndex: 'id',
    },
    {
      title: '昵称',
      dataIndex: 'nickName',
    },
    {
      title: '手机号码',
      dataIndex: 'phoneNumber',
    },
    {
      title: '性别',
      dataIndex: 'gender',
    },
    {
      title: '状态',
      dataIndex: 'state',
    },
    {
      title: '硬币余额',
      dataIndex: 'coinAmount',
    },
    {
      title: '累计收益',
      dataIndex: 'totalRevenue',
    },
    {
      title: '累计消费',
      dataIndex: 'totalConsume',
    },
    {
      title: '视频',
      dataIndex: 'videoCount',
    },
    {
      title: '相册',
      dataIndex: 'albumCount',
    },
    {
      title: '电台',
      dataIndex: 'podcastCount',
    },
    {
      title: '评论',
      dataIndex: 'commentCount',
    },
    {
      title: '注册时间',
      dataIndex: 'registerTime',
    },
    {
      title: '最后登录时间',
      dataIndex: 'lastLoginTime',
    },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Link className={style.rowBtn} to={`/user/${record.id}/edit`}>编辑</Link>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleUserDelete(record)}>
            <a className={style.rowBtn}>删除</a>
          </Popconfirm>
        </span>
      )
    }
  ];

  const handleQuery = (p) => {
    console.log('query');
    props.form.validateFields((err, values) => {
      props.dispatch({
        type: 'userManage/fetchUserList', form: values, pagination: p || pagination,
      });
    });
  };

  const handleTableChanged = (p) => {
    handleQuery(p);
  };

  useEffect(handleQuery, []);

  return (
    <React.Fragment>
      <Card title="用户管理"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Table columns={columns} rowKey={record => record.id}
          scroll={{ x: 'max-content' }}
          dataSource={props.userManage.userList}
          onChange={handleTableChanged}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.userManage.userTotal,
            showTotal: () => <span>总共 {props.userManage.userTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
        />
      </Card>
      <Modal
        title="新增用户"
        width={600}
        visible={newUserDisplay}
        onCancel={eventHandlers.handleCancelNewUserEvent}
        onOk={eventHandlers.handleNewUserSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="名称">
            {getFieldDecorator('newUserName', {
              rules: [{ required: true, message: '用户名不允许为空' }],
            })(
              <Input placeholder="请输入" />
            )}
          </Form.Item>
          <Form.Item label="密码">
            {getFieldDecorator('newPassword', {
              rules: [{ required: true, message: '密码不允许为空' }],
            })(
              <Input.Password placeholder="请输入密码" />
            )}
          </Form.Item>
          <Form.Item label="密码确认">
            {getFieldDecorator('newPasswordConfirmed', {
              rules: [{ validator: validators.validatePasswordConfirmed }],
            })(
              <Input.Password placeholder="请再次输入密码" />
            )}
          </Form.Item>
          <Form.Item label="性别">
            {getFieldDecorator('newGender', {
              initialValue: 'male',
            })(
              <Radio.Group>
                <Radio.Button value="1">男</Radio.Button>
                <Radio.Button value="2">女</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>
          <Form.Item label="手机号码">
            {getFieldDecorator('newPhoneNumber', {
              rules: [{ required: true, message: '手机号码不允许为空' }],
            })(
              <Input />
            )}
          </Form.Item>
          <Form.Item label="状态">
            {getFieldDecorator('newState', {
              initialValue: 'normal',
            })(
              <Radio.Group>
                <Radio.Button value="1">正常</Radio.Button>
                <Radio.Button value="2">冻结</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  );
}

export default UserListManage;
