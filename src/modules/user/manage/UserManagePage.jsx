import React from "react";
import connectWithAuth from "../../../core/connectWithAuth";
import { Breadcrumb, Form } from "antd";
import UserQueryForm from "./UserQueryForm";
import UserListManage from "./UserListManage";
import style from "./style.scss";

const UserManagePage = (props) => {
  
  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>用户管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <UserQueryForm {...props} />
        <UserListManage {...props} />
      </div>
    </div>
  );
}

export default connectWithAuth(({ userManage }) => ({ userManage }))(Form.create()(UserManagePage));
