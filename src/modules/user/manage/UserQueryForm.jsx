import React from "react";
import NumberRange from "../../../components/NumberRange";
import { Row, Col, Input, Select, DatePicker, Form } from "antd";
import style from "./style.scss"
import { ACTIVITY_TYPE } from "../../../common/constants";

const UserQueryForm = (props) => {

  const handleSearch = searchKeyword => {
    props.form.validateFields((err, values) => {
      props.dispatch({
        type: 'userManage/fetchUserList', form: {
          ...values,
        }
      });
    });
  };

  const { getFieldDecorator } = props.form;

  return (
    <Form layout="horizontal" style={{ marginBottom: 20 }}>
      <Row gutter={16}>
        <Col span={24} style={{ display: 'block' }}>
          <Row style={{ width: '40%' }} gutter={8}>
            <Col span={8}>
              <Form.Item className={style.formItem}>
                {getFieldDecorator('queryBy', {
                  initialValue: 'id',
                })(
                  <Select style={{ width: '100%' }}>
                    <Select.Option value="id">ID</Select.Option>
                    <Select.Option value="nickName">昵称</Select.Option>
                    <Select.Option value="phoneNumber">手机号</Select.Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col span={16}>
              <Form.Item className={style.formItem}>
                {getFieldDecorator('queryValue', {
                })(
                  <Input.Search onSearch={handleSearch} enterButton="搜索" />
                )}
              </Form.Item>
            </Col>
          </Row>
        </Col>
        <Col span={6}>
          <Form.Item label="注册时间" className={style.formItem}>
            {getFieldDecorator('registerDateRange')(
              <DatePicker.RangePicker />
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="性别" className={style.formItem}>
            {getFieldDecorator('gender')(
              <Select placeholder="请选择性别" style={{ width: '100%' }}>
                <Select.Option value="all">所有</Select.Option>
                <Select.Option value="1">男</Select.Option>
                <Select.Option value="2">女</Select.Option>
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="状态" className={style.formItem}>
            {getFieldDecorator('state')(
              <Select placeholder="请选择状态" style={{ width: '100%' }}>
                <Select.Option value="all">所有</Select.Option>
                <Select.Option value="1">正常</Select.Option>
                <Select.Option value="2">冻结</Select.Option>
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="活跃度" className={style.formItem}>
            {getFieldDecorator('activityLevel')(
              <Select placeholder="请选择活跃度" style={{ width: '100%' }}>
                <Select.Option value={ACTIVITY_TYPE.LAST_7_DAYS}>最近7天已登录</Select.Option>
                <Select.Option value={ACTIVITY_TYPE.LAST_30_DAYS}>最近30天已登录</Select.Option>
                <Select.Option value={ACTIVITY_TYPE.LAST_365_DAYS}>最近365天已登录</Select.Option>
                <Select.Option value={ACTIVITY_TYPE.NO_7_DAYS}>最近7天没有登录</Select.Option>
                <Select.Option value={ACTIVITY_TYPE.NO_30_DAYS}>最近30天没有登录</Select.Option>
                <Select.Option value={ACTIVITY_TYPE.NO_365_DAYS}>最近365天没有登录</Select.Option>
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="累计充值" className={style.formItem}>
            {getFieldDecorator('totalCharge')(
              <NumberRange />
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="累计消费" className={style.formItem}>
            {getFieldDecorator('totalConsume')(
              <NumberRange />
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="硬币数量" className={style.formItem}>
            {getFieldDecorator('coinCount')(
              <NumberRange />
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="累计收益" className={style.formItem}>
            {getFieldDecorator('totalRevenue')(
              <NumberRange />
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="视频" className={style.formItem}>
            {getFieldDecorator('videoCount')(
              <NumberRange />
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="电台" className={style.formItem}>
            {getFieldDecorator('podcastCount')(
              <NumberRange />
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="相册" className={style.formItem}>
            {getFieldDecorator('albumCount')(
              <NumberRange />
            )}
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="评论" className={style.formItem}>
            {getFieldDecorator('commentCount')(
              <NumberRange />
            )}
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
}

export default UserQueryForm;
