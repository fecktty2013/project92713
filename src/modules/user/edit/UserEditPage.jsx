import React, { useEffect, useState } from "react";
import connectWithAuth from "../../../core/connectWithAuth";
import classNames from "classnames";
import { Breadcrumb, Modal } from "antd";
import { Row, Col, Input, Select, Checkbox, Form, Button } from "antd";
import LogRecordPopupList from "./LogRecordPopupList";
import WatchRecordPopupList from "./WatchRecordPopupList";
import CommentPopupList from "./CommentPopupList";
import LikedPopupList from "./LikedPopupList";
import ChargePopupList from "./ChargePopupList";
import ConsumePopupList from "./ConsumePopupList";
import InvitePopupList from "./InvitePopupList";
import RevenuePopupList from "./RevenuePopupList";
import FavoritePopupList from "./FavoritePopupList";
import ChangePopupList from "./ChangePopupList";
import style from "./style.scss";
import { routerRedux } from "dva/router";

const userAvatar = require("../../../../static/images/u6942.png")

const UserEditPage = (props) => {

  const { userId } = props.match.params;
  const { getFieldsValue } = props.form;
  const { userDetail = {} } = props.userEdit;

  const handleQuery = () => {
    props.dispatch({
      type: 'userEdit/fetchUserDetail',
      payload: { user_id: userId }
    });
  };

  const eventHandlers = {
    async submit() {
      const values = getFieldsValue();
      await props.dispatch({
        type: 'userEdit/saveUser',
        payload: {
          user_id: userId,
          phone: values.phoneNumber,
          username: values.nickName,
          password: values.password,
          status: parseInt(values.state, 10),
          set_robot: values.setAsRobot ? 1 : 0,
          sex: values.gender,
        }
      });
      Modal.success({
        title: '提示',
        content: '保存成功',
        okText: '确认',
      });
    },
    cancel() {
      props.dispatch(routerRedux.push({ pathname: '/user/manage' }));
    }
  }

  const popupEvents = {
    hideAll(e) {
      props.dispatch({ type: 'userEdit/hidePopupVisibility' });
    },
    openLogRecordPopup() {
      const values = getFieldsValue();
      props.dispatch({
        type: 'userEdit/openLogRecordList',
        payload: { userId, ...values },
      });
    },
    closeLogRecordPopup(e) {
      props.dispatch({
        type: 'userEdit/closePopupDialog',
        payload: {
          listField: 'logRecordList',
          totalField: 'logRecordTotal',
        }
      });
    },
    openWatchRecordPopup() {
      const values = getFieldsValue();
      props.dispatch({
        type: 'userEdit/openWatchRecordList',
        payload: { userId, ...values },
      });
    },
    closeWatchRecordPopup(e) {
      props.dispatch({
        type: 'userEdit/closePopupDialog',
        payload: {
          listField: 'logRecordList',
          totalField: 'logRecordTotal',
        }
      });
    },
    openCommentPopup() {
      const values = getFieldsValue();
      props.dispatch({
        type: 'userEdit/openCommentList',
        payload: { userId, ...values },
      });
    },
    closeCommentPopup(e) {
      props.dispatch({
        type: 'userEdit/closePopupDialog',
        payload: {
          listField: 'commentList',
          totalField: 'commentTotal',
        }
      });
    },
    openLikedPopup() {
      const values = getFieldsValue();
      props.dispatch({
        type: 'userEdit/openLikeList',
        payload: { userId, ...values },
      });
    },
    closeLikedPopup(e) {
      props.dispatch({
        type: 'userEdit/closePopupDialog',
        payload: {
          listField: 'likeList',
          totalField: 'likeTotal',
        }
      });
    },
    openChargePopup() {
      const values = getFieldsValue();
      props.dispatch({
        type: 'userEdit/openChargeList',
        payload: { userId, ...values },
      });
    },
    closeChargePopup(e) {
      props.dispatch({
        type: 'userEdit/closePopupDialog',
        payload: {
          listField: 'chargeList',
          totalField: 'chargeTotal',
        }
      });
    },
    openConsumePopup() {
      const values = getFieldsValue();
      props.dispatch({
        type: 'userEdit/openConsumeList',
        payload: { userId, ...values },
      });
    },
    closeConsumePopup(e) {
      props.dispatch({
        type: 'userEdit/closePopupDialog',
        payload: {
          listField: 'consumeList',
          totalField: 'consumeTotal',
        }
      });
    },
    openInvitePopup(e) {
      const values = getFieldsValue();
      props.dispatch({
        type: 'userEdit/openInviteList',
        payload: { userId, values },
      });
    },
    closeInvitePopup(e) {
      props.dispatch({
        type: 'userEdit/closePopupDialog',
        payload: {
          listField: 'inviteList',
          totalField: 'inviteTotal',
        }
      });
    },
    openRevenuePopup() {
      const values = getFieldsValue();
      props.dispatch({
        type: 'userEdit/openRevenueList',
        payload: { userId, ...values },
      });
    },
    closeRevenuePopup(e) {
      props.dispatch({
        type: 'userEdit/closePopupDialog',
        payload: {
          listField: 'revenueList',
          totalField: 'revenueTotal',
        }
      });
    },
    openFavoritePopup() {
      const values = getFieldsValue();
      props.dispatch({
        type: 'userEdit/openFavoriteList',
        payload: { userId, ...values },
      });
    },
    closeFavoritePopup(e) {
      props.dispatch({
        type: 'userEdit/closePopupDialog',
        payload: {
          listField: 'favoriteList',
          totalField: 'favoriteTotal',
        }
      });
    },
    openChangePopup(e) {
      const values = getFieldsValue();
      props.dispatch({
        type: 'userEdit/openChangeList',
        payload: { userId, ...values },
      });
    },
    closeChangePopup(e) {
      props.dispatch({
        type: 'userEdit/closePopupDialog',
        payload: {
          listField: 'changeList',
          totalField: 'changeTotal',
        }
      });
    },
  };

  const { getFieldDecorator } = props.form;

  const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 18 },
  };

  useEffect(handleQuery, []);

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>用户管理</Breadcrumb.Item>
          <Breadcrumb.Item>用户详细</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <Form layout="horizontal" {...formItemLayout} style={{ marginBottom: 20 }}>
          <Row gutter={16} style={{ width: '85%' }}>
            <Col span={12}>
              <Form.Item label="手机号码" className={style.formItem}>
                {getFieldDecorator('phoneNumber', {
                  initialValue: userDetail.phone,
                })(
                  <Input placeholder="请输入" className={style.editItem} />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="名称" className={style.formItem}>
                {getFieldDecorator('nickName', {
                  initialValue: userDetail.username,
                })(
                  <Input placeholder="请输入" className={style.editItem} />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="密码" className={style.formItem}>
                {getFieldDecorator('password')(
                  <Input.Password placeholder="输入新密码可以立即更改" className={style.editItem} />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="用户余额" className={style.formItem}>
                <Input type="number" value={userDetail.coin_amount} disabled placeholder="请输入" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="状态" className={style.formItem}>
                {getFieldDecorator('state', { initialValue: userDetail.status ?.toString() })(
                  <Select placeholder="请选择状态" className={classNames(style.editItem, style.selectItem)}>
                    <Select.Option value="1">正常</Select.Option>
                    <Select.Option value="2">冻结</Select.Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="性别" className={style.formItem}>
                {getFieldDecorator('gender', { initialValue: userDetail.sex ?.toString() })(
                  <Select placeholder="请选择性别" className={classNames(style.editItem, style.selectItem)}>
                    <Select.Option value="1">男</Select.Option>
                    <Select.Option value="2">女</Select.Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="头像" className={style.formItem}>
                <img src={userDetail.head_img} style={{ maxWidth: 150 }} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="用户总览" className={style.formItem}>
                <span className="ant-form-text">登录次数：{userDetail.login_count}次 |  独立ip总数：{userDetail.ip_num}个 | 设备总数：{userDetail.device_num}个</span>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="邀请码" className={style.formItem}>
                <Input value={userDetail.user_invitation} disabled placeholder="请输入" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="机器人" className={style.formItem}>
                {getFieldDecorator('setAsRobot', { valuePropName: 'checked', initialValue: !!userDetail.is_robot })(
                  <Checkbox>设置为机器人</Checkbox>
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="登录记录" className={style.formItem}>
                <Input value={userDetail.login_count} disabled suffix={
                  <Button icon="table" size="small" className={style.suffixBtn} onClick={popupEvents.openLogRecordPopup} />
                } />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="观看详细" className={style.formItem}>
                <Input value={userDetail.view_num} disabled suffix={
                  <Button icon="table" size="small" className={style.suffixBtn} onClick={popupEvents.openWatchRecordPopup} />
                } />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="评论详细" className={style.formItem}>
                <Input value={userDetail.comment_num ?.toString()} disabled suffix={
                  <Button icon="table" size="small" className={style.suffixBtn} onClick={popupEvents.openCommentPopup} />
                } />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="点赞详细" className={style.formItem}>
                <Input value={userDetail.thumbsup_num} disabled suffix={
                  <Button icon="table" size="small" className={style.suffixBtn} onClick={popupEvents.openLikedPopup} />
                } />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="充值" className={style.formItem}>
                <Input value={userDetail.charge_num} disabled suffix={
                  <Button icon="table" size="small" className={style.suffixBtn} onClick={popupEvents.openChargePopup} />
                } />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="消费" className={style.formItem}>
                <Input value={userDetail.consume_num} disabled suffix={
                  <Button icon="table" size="small" className={style.suffixBtn} onClick={popupEvents.openConsumePopup} />
                } />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="邀请人数" className={style.formItem}>
                <Input value={userDetail.invite_num} disabled suffix={
                  <Button icon="table" size="small" className={style.suffixBtn} onClick={popupEvents.openInvitePopup} />
                } />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="收益" className={style.formItem}>
                <Input value={userDetail.profit_num} disabled suffix={
                  <Button icon="table" size="small" className={style.suffixBtn} onClick={popupEvents.openRevenuePopup} />
                } />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="收藏" className={style.formItem}>
                <Input value={userDetail.collect_num} disabled suffix={
                  <Button icon="table" size="small" className={style.suffixBtn} onClick={popupEvents.openFavoritePopup} />
                } />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="变更详细" className={style.formItem}>
                <Input value={userDetail.modify_num} disabled suffix={
                  <Button icon="table" size="small" className={style.suffixBtn} onClick={popupEvents.openChangePopup} />
                } />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24} style={{ textAlign: 'center' }}>
              <Button className={style.actionBtn} size="large" type="primary" htmlType="submit" onClick={eventHandlers.submit}>
                提交
            </Button>
              <Button className={style.actionBtn} size="large" style={{ marginLeft: 18 }} onClick={eventHandlers.cancel}>
                取消
            </Button>
            </Col>
          </Row>
        </Form>
        <Modal
          title="登录详细"
          width={750}
          footer={null}
          visible={props.userEdit.modalVisibility === 'logRecord'}
          onCancel={popupEvents.hideAll}
          afterClose={popupEvents.closeLogRecordPopup}
        >
          <LogRecordPopupList {...props} userId={userId} />
        </Modal>
        <Modal
          title="观看详细"
          width={750}
          footer={null}
          visible={props.userEdit.modalVisibility === 'watchRecord'}
          bodyStyle={{ paddingTop: 8, paddingBottom: 16 }}
          onCancel={popupEvents.hideAll}
          afterClose={popupEvents.closeWatchRecordPopup}
        >
          <WatchRecordPopupList {...props} userId={userId} eventHandlers={popupEvents} />
        </Modal>
        <Modal
          title="评论详细"
          width={750}
          footer={null}
          visible={props.userEdit.modalVisibility === 'comment'}
          bodyStyle={{ paddingTop: 8, paddingBottom: 16 }}
          onCancel={popupEvents.hideAll}
          afterClose={popupEvents.closeCommentPopup}
        >
          <CommentPopupList {...props} userId={userId} eventHandlers={popupEvents} />
        </Modal>
        <Modal
          title="点赞详细"
          width={750}
          footer={null}
          visible={props.userEdit.modalVisibility === 'like'}
          bodyStyle={{ paddingTop: 8, paddingBottom: 16 }}
          onCancel={popupEvents.hideAll}
          afterClose={popupEvents.closeLikedPopup}
        >
          <LikedPopupList {...props} userId={userId} eventHandlers={popupEvents} />
        </Modal>
        <Modal
          title="充值详细"
          width={750}
          footer={null}
          visible={props.userEdit.modalVisibility === 'charge'}
          bodyStyle={{ paddingTop: 8, paddingBottom: 16 }}
          onCancel={popupEvents.hideAll}
          afterClose={popupEvents.closeChargePopup}
        >
          <ChargePopupList {...props} userId={userId} eventHandlers={popupEvents} />
        </Modal>
        <Modal
          title="消费详细"
          width={750}
          footer={null}
          visible={props.userEdit.modalVisibility === 'consume'}
          bodyStyle={{ paddingTop: 8, paddingBottom: 16 }}
          onCancel={popupEvents.hideAll}
          afterClose={popupEvents.closeChargePopup}
        >
          <ConsumePopupList {...props} userId={userId} eventHandlers={popupEvents} />
        </Modal>
        <Modal
          title="邀请记录"
          width={1024}
          footer={null}
          visible={props.userEdit.modalVisibility === 'invite'}
          onCancel={popupEvents.hideAll}
          afterClose={popupEvents.closeInvitePopup}
        >
          <InvitePopupList {...props} userId={userId} eventHandlers={popupEvents} />
        </Modal>
        <Modal
          title="收益详细"
          width={750}
          footer={null}
          visible={props.userEdit.modalVisibility === 'revenue'}
          bodyStyle={{ paddingTop: 8, paddingBottom: 16 }}
          onCancel={popupEvents.hideAll}
          afterClose={popupEvents.closeRevenuePopup}
        >
          <RevenuePopupList {...props} userId={userId} eventHandlers={popupEvents} />
        </Modal>
        <Modal
          title="收藏详细"
          width={750}
          footer={null}
          visible={props.userEdit.modalVisibility === 'favorite'}
          bodyStyle={{ paddingTop: 8, paddingBottom: 16 }}
          onCancel={popupEvents.hideAll}
          afterClose={popupEvents.closeFavoritePopup}
        >
          <FavoritePopupList {...props}  userId={userId} eventHandlers={popupEvents} />
        </Modal>
        <Modal
          title="变更详细"
          width={850}
          footer={null}
          visible={props.userEdit.modalVisibility === 'change'}
          onCancel={popupEvents.hideAll}
          afterClose={popupEvents.closeChangePopup}
        >
          <ChangePopupList {...props} userId={userId} eventHandlers={popupEvents} />
        </Modal>
      </div>
    </div>
  );
}

const mapState = ({ userEdit }) => ({ userEdit });

const WrapFormPage = Form.create()(UserEditPage);

export default connectWithAuth(mapState)(WrapFormPage);
