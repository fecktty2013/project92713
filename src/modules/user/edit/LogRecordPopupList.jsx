import React, { useState } from "react";
import { Table } from "antd";

const columns = [
  {
    title: 'id',
    dataIndex: 'id',
  },
  {
    title: '登录日期',
    dataIndex: 'loginTime',
  },
  {
    title: '登录ip',
    dataIndex: 'loginIp',
  },
  {
    title: '设备',
    dataIndex: 'device',
  }
];

const LogRecordPopupList = (props) => {

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const { userId } = props.match.params;

  const { logRecordList, logRecordTotal } = props.userEdit;

  const handleTableChanged = (pagination) => {
    props.dispatch({
      type: 'userEdit/openLogRecordList',
      payload: { userId, pagination },
    })
    setPagination(pagination);
  };

  return (
    <Table columns={columns} rowKey={record => record.id}
      scroll={{ x: 'max-content' }}
      dataSource={logRecordList}
      onChange={handleTableChanged}
      pagination={{
        ...pagination,
        showSizeChanger: true,
        showQuickJumper: true,
        defaultPageSize: 5,
        total: logRecordTotal,
        showTotal: () => <span>总共 {logRecordTotal} 条记录</span>,
        pageSizeOptions: ['5', '10', '20'],
      }} />
  );
};

export default LogRecordPopupList;
