import React, { useEffect, useState } from "react";
import { Table, Form, Select, DatePicker, Button } from "antd";

const columns = [
  {
    title: 'id',
    dataIndex: 'id',
  },
  {
    title: '类型',
    dataIndex: 'revenueType',
  },
  {
    title: '收益金额',
    dataIndex: 'amount',
  },
  {
    title: '对方id',
    dataIndex: 'personId',
  },
  {
    title: '时间',
    dataIndex: 'time',
  },
];

const RevenuePopupList = (props) => {

  const { getFieldDecorator, getFieldsValue } = props.form;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleQuery = (p) => {
    if (!p) {
      p = pagination;
    }
    const values = getFieldsValue();
    props.dispatch({
      type: 'userEdit/openRevenueList',
      payload: {
        userId: props.userId,
        ...values,
        pagination: p,
      }
    })
  };

  const handleTableChanged = (p) => {
    setPagination(p);
    handleQuery(p);
  };

  const handleFilterFetch = e => {
    handleQuery();
  };

  useEffect(() => {
    props.dispatch({ type: 'userEdit/fetchRevenueTypes' });
  }, []);
  
  return (
    <React.Fragment>
      <Form layout="inline" style={{ marginBottom: 8 }}>
      <Form.Item>
          {getFieldDecorator('revenueTypeFilter')(
            <Select style={{ width: 140 }} placeholder="请选择收益类型">
              {props.userEdit.revenueTypeList.map((itm) => (
                <Select.Option key={itm.type} value={itm.type}>{itm.typeDesc}</Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('revenueTimeRangeFilter')(
            <DatePicker.RangePicker style={{ width: 250 }} />
          )}
        </Form.Item>
        <Form.Item>
          <Button onClick={handleFilterFetch} type="primary">确定</Button>
        </Form.Item>
      </Form>
      <Table columns={columns} rowKey={record => record.id}
        scroll={{ x: 'max-content' }}
        dataSource={props.userEdit.revenueList}
        onChange={handleTableChanged}
        pagination={{
          ...pagination,
          showSizeChanger: true,
          showQuickJumper: true,
          defaultPageSize: 5,
          total: props.userEdit.revenueTotal,
          showTotal: () => <span>总共 {props.userEdit.revenueTotal} 条记录</span>,
          pageSizeOptions: ['5', '10', '20'],
        }} />
    </React.Fragment>
  );
};

export default RevenuePopupList;
