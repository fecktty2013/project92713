import { routerRedux } from 'dva/router';
import * as service from './service';

const initialState = {

  userDetail: {},

  modalVisibility: '',

  logRecordList: [],
  logRecordTotal: 0,

  watchRecordList: [],
  watchRecordTotal: 0,

  commentList: [],
  commentTotal: 0,

  likeList: [],
  likeTotal: 0,

  chargeList: [],
  chargeTotal: 0,

  consumeList: [],
  consumeTotal: 0,

  inviteList: [],
  inviteTotal: 0,

  revenueList: [],
  revenueTotal: 0,
  revenueTypeList: [],

  favoriteList: [],
  favoriteTotal: 0,

  changeList: [],
  changeTotal: 0,
};

export default {
  namespace: 'userEdit',

  state: initialState,

  subscriptions: {
    
  },

  effects: {
    *fetchUserDetail({ payload }, { call, put }) {
      const httpResult = yield call(service.getUserDetail, payload);
      const userDetail = httpResult.data.user;
      yield put({ type: 'stateUpdated', payload: { userDetail }});
    },
    *saveUser({ payload }, { call, put }) {
      yield call(service.saveUser, payload);
    },
    *openLogRecordList({ payload: { userId, pagination = { current: 1, pageSize: 5 } } }, { call, put, select }) {
      const httpResult = yield call(service.fetchUserLoginList, {
        user_id: userId,
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const logRecordList = httpResult.data.login_list.map((itm, idx) => ({
        id: itm.user_id,
        loginTime: itm.login_time,
        loginIp: itm.remote_ip,
        device: itm.client_type,
        imeiCode: itm.imei,
      }));
      const logRecordTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: {
        modalVisibility: 'logRecord',
        logRecordList, logRecordTotal,
      }});
    },
    *openWatchRecordList({ payload: { userId, watchTypeFilter, watchTimeRangeFilter, pagination = { current: 1, pageSize: 5 } } }, { call, put, select }) {
      const httpResult = yield call(service.fetchUserBrowseList, {
        user_id: userId,
        media_type: watchTypeFilter,
        begin: watchTimeRangeFilter && watchTimeRangeFilter[0] && watchTimeRangeFilter[0].format('YYYY-MM-DD'),
        end: watchTimeRangeFilter && watchTimeRangeFilter[1] && watchTimeRangeFilter[1].format('YYYY-MM-DD'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const watchRecordList = httpResult.data.access_list.map((itm, idx) => ({
        id: itm.user_id + idx.toString(),
        type: itm.media_type_zh,
        name: itm.title,
        ip: 'IP',
        watchTime: itm.create_time,
      }));
      const watchRecordTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: {
        watchRecordList, watchRecordTotal,
        modalVisibility: 'watchRecord',
      }});
    },
    *openCommentList({ payload: { userId, commentTypeFilter, commentTimeRangeFilter, pagination = { current: 1, pageSize: 5 } } }, { call, put, select }) {
      const httpResult = yield call(service.fetchUserCommentList, {
        user_id: userId,
        media_type: commentTypeFilter === 'all' ? undefined : commentTypeFilter,
        begin: commentTimeRangeFilter && commentTimeRangeFilter[0] && commentTimeRangeFilter[0].format('YYYY-MM-DD'),
        end: commentTimeRangeFilter && commentTimeRangeFilter[1] && commentTimeRangeFilter[1].format('YYYY-MM-DD'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const commentList = httpResult.data.comment_list.map((itm, idx) => ({
        id: itm.user_id + idx.toString(),
        type: itm.media_type_zh,
        name: itm.album_title,
        ip: 'IP',
        comment: itm.content,
        commentTime: itm.publish_time,
      }));
      const commentTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: {
        commentList, commentTotal,
        modalVisibility: 'comment',
      }});
    },
    *openLikeList({ payload: { userId, likeTypeFilter, likeTimeRangeFilter, pagination = { current: 1, pageSize: 5 } } }, { call, put, select }) {
      const httpResult = yield call(service.fetchUserThumbsupList, {
        user_id: userId,
        media_type: likeTypeFilter === 'all' ? undefined : likeTypeFilter,
        begin: likeTimeRangeFilter && likeTimeRangeFilter[0] && likeTimeRangeFilter[0].format('YYYY-MM-DD'),
        end: likeTimeRangeFilter && likeTimeRangeFilter[1] && likeTimeRangeFilter[1].format('YYYY-MM-DD'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const likeList = httpResult.data.access_list.map((itm, idx) => ({
        id: itm.user_id + idx.toString(),
        type: itm.media_type_zh,
        name: itm.title,
        ip: 'IP',
        likeTime: itm.create_time,
      }));
      const likeTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: {
        likeList, likeTotal,
        modalVisibility: 'like'
      }});
    },
    *openChargeList({ payload: { userId, chargeTypeFilter, chargeStatusFilter, chargeTimeRangeFilter, pagination = { current: 1, pageSize: 5 } } }, { call, put, select }) {
      const httpResult = yield call(service.fetchUserChargeList, {
        user_id: userId,
        order_type: chargeTypeFilter === 'all' ? undefined : chargeTypeFilter,
        begin: chargeTimeRangeFilter && chargeTimeRangeFilter[0] && chargeTimeRangeFilter[0].format('YYYY-MM-DD'),
        end: chargeTimeRangeFilter && chargeTimeRangeFilter[1] && chargeTimeRangeFilter[1].format('YYYY-MM-DD'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const chargeList = httpResult.data.charge_list.map((itm, idx) => ({
        id: itm.user_id + idx.toString(),
        chargeType: itm.order_type,
        amount: itm.gain_coin,
        orderNo: itm.order_no,
        chargeStatus: itm.result,
        ip: 'IP',
        chargeTime: itm.charge_time,
      }));
      const chargeTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: {
        chargeList, chargeTotal,
        modalVisibility: 'charge',
      }});
    },
    *openConsumeList({ payload: { userId, typeFilter, consumeActionFilter, consumeTypeFilter, consumeTimeRangeFilter, pagination = { current: 1, pageSize: 5 } } }, { call, put, select }) {
      const httpResult = yield call(service.fetchUserConsumeList, {
        user_id: userId,
        media_type: consumeTypeFilter === 'all' ? undefined : consumeTypeFilter,
        buy_type: consumeActionFilter === 'all' ? undefined : consumeActionFilter,
        begin: consumeTimeRangeFilter && consumeTimeRangeFilter[0] && consumeTimeRangeFilter[0].format('YYYY-MM-DD'),
        end: consumeTimeRangeFilter && consumeTimeRangeFilter[1] && consumeTimeRangeFilter[1].format('YYYY-MM-DD'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const consumeList = httpResult.data.consume_list.map((itm, idx) => ({
        id: itm.user_id + idx.toString(),
        name: itm.title,
        type: itm.media_type_zh,
        amount: itm.coin_num,
        consumeType: itm.type_title,
        ip: 'IP',
        consumeTime: itm.trade_time,
      }));
      const consumeTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: {
        consumeList, consumeTotal,
        modalVisibility: 'consume',
      }});
    },
    *openInviteList({ payload: { userId, pagination = { current: 1, pageSize: 5} } }, { call, put, select }) {
      const httpResult = yield call(service.fetchUserInviteList, {
        user_id: userId,
        page_no: pagination.current -  1,
        page_size: pagination.pageSize,
      });
      const inviteList = httpResult.data.invite_list.map((itm, idx) => ({
        id: itm.invitation_id,
        personRegisterTime: itm.create_time,
        personId: itm.user_id,
        personIp: 'IP',
        phoneNumber: itm.phone,
        device: itm.imei,
        isNewRegistered: itm.status == '1' ? '是' : '否',
        remark: itm.reason,
      }));
      const inviteTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: {
        inviteList, inviteTotal,
        modalVisibility: 'invite',
      }});
    },
    *openRevenueList({ payload: { userId, revenueTypeFilter, revenueTimeRangeFilter, pagination = { current: 1, pageSize: 5 } } }, { call, put, select }) {
      const httpResult = yield call(service.fetchUserProfitList, {
        user_id: userId,
        profit_type: revenueTypeFilter === 'all' ? undefined : revenueTypeFilter,
        begin: revenueTimeRangeFilter && revenueTimeRangeFilter[0] && revenueTimeRangeFilter[0].format('YYYY-MM-DD'),
        end: revenueTimeRangeFilter && revenueTimeRangeFilter[1] && revenueTimeRangeFilter[1].format('YYYY-MM-DD'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      })
      const revenueList = httpResult.data.profit_list.map((itm, idx) => ({
        id: itm.user_id + idx.toString(),
        revenueType: itm.sub_type_zh,
        amount: itm.coin_num,
        personId: itm.user_id,
        time: itm.trade_time,
      }));
      const revenueTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: {
        revenueList, revenueTotal,
        modalVisibility: 'revenue',
      }});
    },
    *fetchRevenueTypes({ payload }, { put, call }) {
      const httpResult = yield call(service.fetchUserTypeList);
      const revenueTypeList = httpResult.data.profit_type_list.map((itm) => ({
        type: itm.profit_type,
        typeDesc: itm.type_zh,
      }));
      yield put({ type: 'stateUpdated', payload: { revenueTypeList } });
    },
    *openFavoriteList({ payload: { userId, favoriteTypeFilter, favoriteTimeRangeFilter, pagination = { current: 1, pageSize: 5 } } }, { call, put, select }) {
      const httpResult = yield call(service.fetchUserCollectList, {
        user_id: userId,
        media_type: favoriteTypeFilter,
        begin: favoriteTimeRangeFilter && favoriteTimeRangeFilter[0] && favoriteTimeRangeFilter[0].format('YYYY-MM-DD'),
        end: favoriteTimeRangeFilter && favoriteTimeRangeFilter[1] && favoriteTimeRangeFilter[1].format('YYYY-MM-DD'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const favoriteList = httpResult.data.access_list.map((itm, idx) => ({
        id: itm.user_id + idx.toString(),
        type: itm.media_type_zh,
        name: itm.title,
        ip: 'IP',
        time: itm.create_time,
      }));
      const favoriteTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: {
        favoriteList, favoriteTotal,
        modalVisibility: 'favorite',
      }});
    },
    *openChangeList({ payload: { userId, pagination = { current: 1, pageSize: 5 } } }, { call, put, select }) {
      const httpResult = yield call(service.fetchUserModifyList, {
        user_id: userId,
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const changeList = httpResult.data.modify_list.map((itm, idx) => ({
        id: itm.user_id + idx.toString(),
        time: itm.create_time,
        action: itm.modify_type,
        item: itm.fields,
        valueBefore: itm.original,
        valueAfter: itm.content,
        operator: itm.operator,
      }));
      const changeTotal = httpResult.data.total_count;
      yield put({ type: 'stateUpdated', payload: {
        changeList, changeTotal,
        modalVisibility: 'change'
      }});
    },
    *closePopupDialog({ payload: { listField, totalField } }, { put }) {
      const payload = {
        [listField]: [],
        [totalField]: 0,
      };
      yield put({ type: 'stateUpdated', payload });
    },
    *hidePopupVisibility({}, { put }) {
      const payload = { modalVisibility: '' };
      yield put({ type: 'stateUpdated', payload });
    }
  },

  reducers: {
    logRecordUpdated(state, { payload: { logRecordList, logRecordTotal } }) {
      return { ...state, logRecordList, logRecordTotal };
    },
    stateUpdated(state, { payload }) {
      return { ...state, ...payload };
    }
  }
}