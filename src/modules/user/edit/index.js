import React from "react";
import UserEditPage from "./UserEditPage";
import model from "./model";

export default {
  component: UserEditPage, model,
};
