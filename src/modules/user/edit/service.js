import $http, { APP_BASE_URL } from "../../../core/http";

export function getUserDetail(payload) {
  return $http.get("/user", {
    params: payload,
  });
}

export function saveUser(payload) {
  return $http.post("/user", payload);
}

export function fetchUserLoginList(payload) {
    return $http.get("/user/login/list", {
        params: payload,
    });
}

export function fetchUserCommentList(payload) {
    return $http.get("/user/comment/list", {
        params: payload,
    });
}

export function fetchUserChargeList(payload) {
    return $http.get("/user/charge/list", {
        params: payload,
    });
}

export function fetchUserThumbsupList(payload) {
    return $http.get("/user/thumbsup/list", {
        params: payload,
    });
}

export function fetchUserCollectList(payload) {
    return $http.get("/user/collect/list", {
        params: payload,
    });
}

export function fetchUserBrowseList(payload) {
    return $http.get("/user/view/list", {
        params: payload,
    });
}

export function fetchUserConsumeList(payload) {
    return $http.get("/user/consume/list", {
        params: payload,
    });
}

export function fetchUserProfitList(payload) {
    return $http.get("/user/profit/list", {
        params: payload,
    });
}

export function fetchUserTypeList(payload) {
    return $http.get("/user/profit/type/list");
}

export function fetchUserModifyList(payload) {
    return $http.get("/user/modify/list", {
        params: payload,
    });
}

export function fetchUserInviteList(payload) {
    return $http.get("/user/invite/list", {
        params: payload,
    });
}
