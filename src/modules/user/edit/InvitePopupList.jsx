import React, { useState } from "react";
import { Table } from "antd";

const columns = [
  {
    title: 'id',
    dataIndex: 'id',
  },
  {
    title: '对方id',
    dataIndex: 'personId',
  },
  {
    title: '手机号码',
    dataIndex: 'phoneNumber',
  },
  {
    title: '设备信息',
    dataIndex: 'device',
  },
  {
    title: '是否新用户注册',
    dataIndex: 'isNewRegistered',
  },
  {
    title: '失败原因备注',
    dataIndex: 'remark',
  },
];

const InvitePopupList = (props) => {

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleTableChanged = (p) => {
    setPagination(p);
    props.dispatch({
      type: 'userEdit/openInviteList',
      payload: {
        userId: props.userId,
        pagination: p,
      }
    })
  };

  return (
    <Table columns={columns} rowKey={record => record.id}
      scroll={{ x: 'max-content' }}
      dataSource={props.userEdit.inviteList}
      onChange={handleTableChanged}
      pagination={{
        ...pagination,
        showSizeChanger: true,
        showQuickJumper: true,
        defaultPageSize: 5,
        total: props.userEdit.inviteTotal,
        showTotal: () => <span>总共 {props.userEdit.inviteTotal} 条记录</span>,
        pageSizeOptions: ['5', '10', '20'],
      }} />
  );
};

export default InvitePopupList;
