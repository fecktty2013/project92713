import React, { useState } from "react";
import { Table } from "antd";

const columns = [
  {
    title: 'id',
    dataIndex: 'id',
  },
  {
    title: '操作时间',
    dataIndex: 'time',
  },
  {
    title: '类型',
    dataIndex: 'action',
  },
  {
    title: '项目',
    dataIndex: 'item',
  },
  {
    title: '更变前',
    dataIndex: 'valueBefore',
  },
  {
    title: '更变后',
    dataIndex: 'valueAfter',
  },
  {
    title: '操作人',
    dataIndex: 'operator',
  },
  {
    title: '设备',
    dataIndex: 'device',
  },
];

const ChangePopupList = (props) => {

  const [pagination, setPagination] = useState({
    current: 1, pageSize: 5
  });

  const handleTableChanged = (p) => {
    setPagination(p);
    props.dispatch({
      type: 'userEdit/openChangeList',
      payload: {
        userId: props.userId,
        pagination: p,
      }
    })
  };

  return (
    <Table columns={columns} rowKey={record => record.id}
      scroll={{ x: 'max-content' }}
      dataSource={props.userEdit.changeList}
      onChange={handleTableChanged}
      pagination={{
        ...pagination,
        showSizeChanger: true,
        showQuickJumper: true,
        defaultPageSize: 5,
        total: props.userEdit.changeTotal,
        showTotal: () => <span>总共 {props.userEdit.changeTotal} 条记录</span>,
        pageSizeOptions: ['5', '10', '20'],
      }} />
  );
};

export default ChangePopupList;
