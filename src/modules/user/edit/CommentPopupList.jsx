import React, { useState } from "react";
import { Table, Form, Select, DatePicker, Button } from "antd";

const columns = [
  {
    title: 'id',
    dataIndex: 'id',
  },
  {
    title: '类型',
    dataIndex: 'type',
  },
  {
    title: '名称',
    dataIndex: 'name',
  },
  {
    title: '添加内容',
    dataIndex: 'comment',
  },
  {
    title: '时间',
    dataIndex: 'commentTime',
  },
];

const CommentPopupList = (props) => {

  const { getFieldDecorator, getFieldsValue } = props.form;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleQuery = (p) => {
    if (!p) {
      p = pagination;
    }
    const values = getFieldsValue();
    props.dispatch({
      type: 'userEdit/openCommentList',
      payload: {
        userId: props.userId,
        ...values,
        pagination: p,
      }
    })
  };

  const handleTableChanged = (p) => {
    handleQuery(p);
    setPagination(p);
  };

  const handleFilterFetch = e => {
    handleQuery();
  };
  
  return (
    <React.Fragment>
      <Form layout="inline" style={{ marginBottom: 8 }}>
        <Form.Item>
          {getFieldDecorator('commentTypeFilter')(
            <Select style={{ width: 120 }} placeholder="请选择类型">
              <Select.Option value="0">所有</Select.Option>
              <Select.Option value="1">视频</Select.Option>
              <Select.Option value="2">相册</Select.Option>
              <Select.Option value="3">电台</Select.Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('commentTimeRangeFilter')(
            <DatePicker.RangePicker style={{ width: 250 }} />
          )}
        </Form.Item>
        <Form.Item>
          <Button onClick={handleFilterFetch} type="primary">确定</Button>
        </Form.Item>
      </Form>
      <Table columns={columns} rowKey={record => record.id}
        scroll={{ x: 'max-content' }}
        dataSource={props.userEdit.commentList}
        onChange={handleTableChanged}
        pagination={{
          ...pagination,
          showSizeChanger: true,
          showQuickJumper: true,
          defaultPageSize: 5,
          total: props.userEdit.commentTotal,
          showTotal: () => <span>总共 {props.userEdit.commentTotal} 条记录</span>,
          pageSizeOptions: ['5', '10', '20'],
        }} />
    </React.Fragment>
  );
};

export default CommentPopupList;
