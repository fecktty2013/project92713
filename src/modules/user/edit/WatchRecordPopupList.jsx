import React, { useState } from "react";
import { Table, Form, Select, DatePicker, Button } from "antd";

const columns = [
  {
    title: 'id',
    dataIndex: 'id',
  },
  {
    title: '类型',
    dataIndex: 'type',
  },
  {
    title: '名称',
    dataIndex: 'name',
  },
  {
    title: '时间',
    dataIndex: 'watchTime',
  },
];

const WatchRecordPopupList = (props) => {

  const { getFieldDecorator, getFieldsValue } = props.form;
  const { watchRecordList, watchRecordTotal } = props.userEdit;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleQuery = (p) => {
    if (!p) {
      p = pagination;
    }
    const values = getFieldsValue();
    props.dispatch({
      type: 'userEdit/openWatchRecordList',
      payload: {
        userId: props.userId,
        ...values,
        pagination: p,
      }
    });
  };

  const handleTableChanged = (pagination) => {
    setPagination(pagination);
    handleQuery(pagination);
  };

  const handleFilterFetch = e => {
    handleQuery();
  };
  
  return (
    <React.Fragment>
      <Form layout="inline" style={{ marginBottom: 8 }}>
        <Form.Item>
          {getFieldDecorator('watchTypeFilter')(
            <Select style={{ width: 120 }} placeholder="请选择类型">
              <Select.Option value="0">所有</Select.Option>
              <Select.Option value="1">视频</Select.Option>
              <Select.Option value="2">相册</Select.Option>
              <Select.Option value="3">电台</Select.Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('watchTimeRangeFilter')(
            <DatePicker.RangePicker style={{ width: 250 }} />
          )}
        </Form.Item>
        <Form.Item>
          <Button onClick={handleFilterFetch} type="primary">确定</Button>
        </Form.Item>
      </Form>
      <Table columns={columns} rowKey={record => record.id}
        scroll={{ x: 'max-content' }}
        dataSource={watchRecordList}
        onChange={handleTableChanged}
        pagination={{
          ...pagination,
          showSizeChanger: true,
          showQuickJumper: true,
          defaultPageSize: 5,
          total: watchRecordTotal,
          showTotal: () => <span>总共 {watchRecordTotal} 条记录</span>,
          pageSizeOptions: ['5', '10', '20'],
        }} />
    </React.Fragment>
  );
};

export default WatchRecordPopupList;
