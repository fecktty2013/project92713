import React, { useState } from "react";
import { Table, Form, Select, DatePicker, Button } from "antd";

const columns = [
  {
    title: 'id',
    dataIndex: 'id',
  },
  {
    title: '充值类型',
    dataIndex: 'chargeType',
  },
  {
    title: '金额',
    dataIndex: 'amount',
  },
  {
    title: '订单号',
    dataIndex: 'orderNo',
  },
  {
    title: '充值状态',
    dataIndex: 'chargeStatus',
  },
  {
    title: '充值时间',
    dataIndex: 'chargeTime',
  },
];

const LikedPopupList = (props) => {

  const { getFieldDecorator, getFieldsValue } = props.form;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleQuery = (p) => {
    if (!p) {
      p = pagination;
    }
    const values = getFieldsValue();
    props.dispatch({
      type: 'userEdit/openChargeList',
      payload: {
        userId: props.userId,
        ...values,
        pagination: p,
      }
    })
  };

  const handleTableChanged = (p) => {
    setPagination(p);
    handleQuery(p);
  };

  const handleFilterFetch = e => {
    handleQuery();
  };
  
  return (
    <React.Fragment>
      <Form layout="inline" style={{ marginBottom: 8 }}>
        <Form.Item>
          {getFieldDecorator('chargeTypeFilter')(
            <Select style={{ width: 140 }} placeholder="请选择充值类型">
              <Select.Option value="all">所有</Select.Option>
              <Select.Option value="charge">在线充值</Select.Option>
              <Select.Option value="exchange">兑换码</Select.Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('chargeTimeRangeFilter')(
            <DatePicker.RangePicker style={{ width: 250 }} />
          )}
        </Form.Item>
        <Form.Item>
          <Button onClick={handleFilterFetch} type="primary">确定</Button>
        </Form.Item>
      </Form>
      <Table columns={columns} rowKey={record => record.id}
        scroll={{ x: 'max-content' }}
        dataSource={props.userEdit.chargeList}
        onChange={handleTableChanged}
        pagination={{
          ...pagination,
          showSizeChanger: true,
          showQuickJumper: true,
          defaultPageSize: 5,
          total: props.userEdit.chargeTotal,
          showTotal: () => <span>总共 {props.userEdit.chargeTotal} 条记录</span>,
          pageSizeOptions: ['5', '10', '20'],
        }} />
    </React.Fragment>
  );
};

export default LikedPopupList;
