import { routerRedux } from 'dva/router';
import * as service from "./service";

const initialState = {
  positionList: [],
  jumpTypeList: [],

  adsList: [],
  adsUploadImg: null,
  editingAds: {},
  adsTotal: 0,

  modalVisibility: '',

  hotSearchList: [],
  hotSearchTotal: 0,
  editingHotSearch: {},

  paymentList: [],
  editingPayment: {},

  shareSettings: '',
};

export default {
  namespace: 'opManage',

  state: initialState,

  subscriptions: {
  },

  effects: {
    *fetchCommonList(action, { put, call }) {
      const result = yield call(service.fetchAdvertCommon);
      yield put({
        type: 'stateChanged',
        payload: {
          positionList: result.data.position_list,
          jumpTypeList: result.data.jump_type_list,
        }
      });
    },

    *fetchAdsList({ payload: { form, pagination } = {} }, { put, select, call }) {
      const httpResult = yield call(service.fetchAdvertList, {
        position: form.adsLocation,
        begin: form.adsTimeRange && form.adsTimeRange[0] && form.adsTimeRange[0].format('YYYY-MM-DD HH:mm:ss'),
        end: form.adsTimeRange && form.adsTimeRange[1] && form.adsTimeRange[1].format('YYYY-MM-DD HH:mm:ss'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const result = httpResult.data.advert_list.map((advert, idx) => ({
        sn: idx + 1,
        id: advert.advert_id,
        createdTime: advert.create_time,
        detail: advert.description,
        imgUrl: advert.image,
        link: advert.target,
        location: advert.position_zh,
        locationVal: advert.position,
        jumpType: advert.jump_type_zh,
        jumpTypeVal: advert.jump_type,
        state: advert.status_zh,
        stateVal: advert.status,
        publishTime: advert.publish_time,
      }));
      yield put({ type: 'stateChanged', payload: { adsList: result, adsTotal: httpResult.data.total_count } });
    },
    *editAdsStart({ payload }, { put }) {
      let adsUploadImg = {};
      if (payload.editingAds.imgUrl) {
        adsUploadImg.url = payload.editingAds.imgUrl;
      }
      yield put({
        type: 'stateChanged', payload: {
          ...payload,
          adsUploadImg,
        }
      });
    },
    *setModalVisibility({ payload: { modalVisibility } }, { put }) {
      yield put({ type: 'stateChanged', payload: { modalVisibility } });
    },
    *uploadAdsImg({ payload: { adsUploadImg } }, { call, put }) {
      const uploadResult = yield call(service.uploadImage, adsUploadImg.file);
      yield put({
        type: 'stateChanged', payload: {
          adsUploadImg: {
            ...adsUploadImg,
            url: uploadResult.data.url,
            previewUrl: adsUploadImg.url,
          },
        }
      });
    },
    *saveAdvert({ payload }, { call, put, select }) {
      const opManageState = yield select(state => state.opManage);
      const httpPayload = {
        position: payload.editingAdsLocation,
        description: '',
        status: payload.editingState ? 1 : 2,
        jump_type: payload.editingJumpType,
        target: payload.editingAdsUrl,
        image: opManageState.adsUploadImg.url,
        publish_time: payload.editingPublishTime.format('YYYY-MM-DD HH:mm:ss')
      }
      if (opManageState.editingAds.id) {
        httpPayload.advert_id = opManageState.editingAds.id;
      }
      yield call(service.saveAdvert, httpPayload);
    },
    *updateAdvertStatus({ payload }, { call, put, select }) {
      const httpPayload = {
        advert_id: payload.id,
        status: payload.stateVal,
        position: payload.locationVal,
        description: payload.detail,
        jump_type: payload.jumpTypeVal,
        target: payload.link,
        image: payload.imgUrl,
        publish_time: payload.publishTime
      }
      yield call(service.saveAdvert, httpPayload);
    },
    *deleteAdvert({ payload }, { call, put }) {
      yield call(service.deleteAdvert, payload);
    },
    *uploadHotSearchImg({ payload: { hotSearchUploadImg } }, { put }) {
      yield put({ type: 'stateChanged', payload: { hotSearchUploadImg } })
    },
    *fetchHotSearchList({ form, pagination } = {}, { put, call }) {
      const httpResult = yield call(service.getHotSearchList, {
        begin: form.hotSearchTimeRange && form.hotSearchTimeRange[0] && form.hotSearchTimeRange[0].format('YYYY-MM-DD HH:mm:ss'),
        end: form.hotSearchTimeRange && form.hotSearchTimeRange[1] && form.hotSearchTimeRange[1].format('YYYY-MM-DD HH:mm:ss'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });

      const resultList = httpResult.data.hot_word_list.map((hotWord, idx) => ({
        id: hotWord.hot_word_id,
        sn: idx + 1,
        createdTime: hotWord.publish_time,
        hotSearchText: hotWord.word,
        order: hotWord.seq_no,
        state: hotWord.status_zh,
        stateVal: hotWord.status,
        effectiveTime: hotWord.publish_time,
      }));
      yield put({ type: 'stateChanged', payload: { hotSearchList: resultList, hotSearchTotal: httpResult.data.total_count } });
    },
    *editHotSearchStart({ payload }, { put }) {
      yield put({ type: 'stateChanged', payload });
    },
    *saveHotSearch({ payload }, { call, select }) {
      const opManageState = yield select(state => state.opManage);
      const httpPayload = {
        word: payload.editingHotSearchText,
        status: payload.editingHotSearchState ? 1 : 2,
        publish_time: payload.editingEffectiveTime.format('YYYY-MM-DD HH:mm:ss')
      }
      if (opManageState.editingHotSearch.id) {
        httpPayload.hot_word_id = opManageState.editingHotSearch.id;
      }
      yield call(service.saveHotSearch, httpPayload);
    },
    *deleteHotSearch({ payload }, { call, put }) {
      yield call(service.deleteHotSearch, payload);
    },
    *updateHotSearchStatus({ payload }, { call, put }) {
      yield call(service.updateHotSearchStatus, payload);
    },
    *fetchPaymentList(action, { put, call }) {
      const httpResult = yield call(service.getPaymentList);
      const paymentList = httpResult.data.media_period_list.map((item, idx) => ({
        id: item.media_period_id,
        sn: idx + 1,
        type: item.media_type_zh,
        typeVal: item.media_type,
        state: item.status_zhm,
        stateVal: item.status,
        watchTimeLength: item.view_period,
        downloadTimeLength: item.download_period,
      }));
      yield put({ type: 'stateChanged', payload: { paymentList } });
    },
    *editPaymentStart({ payload }, { put }) {
      yield put({ type: 'stateChanged', payload });
    },
    *savePayment({ payload }, { call, select }) {
      const opManageState = yield select(state => state.opManage);
      const httpPayload = {
        media_type: payload.editingType,
        status: payload.editingState ? 1 : 2,
        view_period: payload.editingWatchTimeLength,
        download_period: payload.editingDownloadTimeLength,
      }
      if (opManageState.editingPayment.id) {
        httpPayload.media_period_id = opManageState.editingPayment.id;
      }
      yield call(service.savePayment, httpPayload);
    },
    *deletePayment({ payload }, { call, put }) {
      yield call(service.deletePayment, payload);
    },
    *updatePaymentStatus({ payload }, { call, put }) {
      yield call(service.updatePaymentStatus, payload);
    },
    *fetchShareSettings(action, { put, call }) {
      const httpResult = yield call(service.getShare);
      const shareSettings = httpResult.data.share_content;
      yield put({ type: 'stateChanged', payload: { shareSettings } });
    },
    *saveShare({ payload }, { call, put }) {
      yield call(service.saveShare, payload);
    }
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}