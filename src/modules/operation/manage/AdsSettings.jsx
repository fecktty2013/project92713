import React, { useState, useEffect } from "react";
import moment from "moment";
import style from "./style.scss";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, Popconfirm } from "antd";
import { ADV_STATUS } from "../../../common/constants";

const AdsSettings = (props) => {

  const { getFieldDecorator, validateFields, resetFields, getFieldsValue, setFieldsValue } = props.form;
  const { editingAds = {}, adsUploadImg, positionList, jumpTypeList } = props.opManage;

  const [loading, setLoading] = useState(false);
  const [uploadingFile, setUploadingFile] = useState(null);
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const handleQuery = (p) => {
    if (!p) {
      p = pagination;
    }
    validateFields((err, values) => {
      props.dispatch({
        type: 'opManage/fetchAdsList', payload: {
          form: values,
          pagination: p,
        }
      });
    });
  };

  const handlePrevDay = () => {
    const values = getFieldsValue();
    const timeRange = values.adsTimeRange;
    if (!timeRange) return;
    const newTimeRange = [
      timeRange[0].add(-1, 'days'),
      timeRange[1].add(-1, 'days')
    ];
    setFieldsValue({
      adsTimeRange: newTimeRange,
    });
    const defaultPagination = { current: 1, pageSize: 5 };
    setPagination(defaultPagination);
    props.dispatch({
      type: 'opManage/fetchAdsList', payload: {
        form: {
          ...values,
          adsTimeRange: newTimeRange,
        },
        pagination: defaultPagination,
      }
    });
  };

  const handleNextDay = () => {
    const values = getFieldsValue();
    const timeRange = values.adsTimeRange;
    if (!timeRange) return;
    const newTimeRange = [
      timeRange[0].add(1, 'days'),
      timeRange[1].add(1, 'days')
    ];
    setFieldsValue({
      adsTimeRange: newTimeRange,
    });
    const defaultPagination = { current: 1, pageSize: 5 };
    setPagination(defaultPagination);
    props.dispatch({
      type: 'opManage/fetchAdsList', payload: {
        form: {
          ...values,
          adsTimeRange: newTimeRange,
        },
        pagination: defaultPagination,
      }
    });
  };

  const resetForm = () => {
    resetFields([
      "editingAdsLocation", "editingJumpType",
      "editingAdsUrl", "editingState", "editingAdsPhoto", "editingPublishTime"
    ]);
  };

  const eventHandlers = {
    handleNewAdsStart(e) {
      resetForm();
      props.dispatch({
        type: 'opManage/editAdsStart',
        payload: { modalVisibility: 'ads', editingAds: {} },
      });
    },
    handleEditAdsStart(record) {
      resetForm();
      props.dispatch({
        type: 'opManage/editAdsStart',
        payload: { modalVisibility: 'ads', editingAds: record },
      });
    },
    async handleDeleteAds(record) {
      await props.dispatch({
        type: 'opManage/deleteAdvert',
        payload: { advert_id: record.id },
      });
      handleQuery();
    },
    async handleUpdateStatus(record, status) {
      const newRecord = {
        ...record,
        stateVal: status,
      };
      await props.dispatch({
        type: 'opManage/updateAdvertStatus',
        payload: newRecord,
      });
      handleQuery();
    },
    handleAdsCancel(e) {
      props.dispatch({
        type: 'opManage/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    handleAdsSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'opManage/saveAdvert',
          payload: values,
        });
        await props.dispatch({
          type: 'opManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
    handleImgBeforeUpload(file) {
      let reader = new FileReader();
      reader.addEventListener('load', () => {
        props.dispatch({
          type: 'opManage/uploadAdsImg',
          payload: {
            adsUploadImg: { uid: file.uid, name: file.name, status: 'done', url: reader.result, file }
          }
        });
      });
      reader.readAsDataURL(file);
      setUploadingFile(file);
      return false;
    },
    handleImgUpload(e) {

    },
  };

  const normFile = e => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  const columns = [
    {
      title: '序号',
      render: (text, record) => {
        return <span key={record.id}>{pagination.pageSize * (pagination.current - 1) + record.sn}</span>
      }
    },
    { title: '创建时间', dataIndex: 'createdTime' },
    { title: '广告详情', dataIndex: 'detail' },
    { title: '链接', dataIndex: 'link' },
    { title: '广告位置', dataIndex: 'location' },
    { title: '跳转类型', dataIndex: 'jumpType' },
    { title: '状态', dataIndex: 'state' },
    { title: '投放时间', dataIndex: 'publishTime' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleDeleteAds(record)}>
            <a>删除</a>
          </Popconfirm>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditAdsStart(record)}>编辑</Button>
          {
            record.stateVal == 1 ?
              <Popconfirm title="确认禁用吗？" onConfirm={e => eventHandlers.handleUpdateStatus(record, ADV_STATUS.DISABLED)}>
                <Button className={style.rowBtn} type="link">禁用</Button>
              </Popconfirm> : undefined
          }
          {
            record.stateVal == 2 ?
              <Popconfirm title="确认启用吗？" onConfirm={e => eventHandlers.handleUpdateStatus(record, ADV_STATUS.ENABLED)}>
                <Button className={style.rowBtn} type="link">启用</Button>
              </Popconfirm> : undefined
          }
        </span>
      )
    },
  ];

  const handleTableChange = (pagination) => {
    console.log('table changed: ', pagination);
    setPagination(pagination);
    handleQuery(pagination);
  };

  useEffect(() => handleQuery(pagination), []);

  return (
    <React.Fragment>
      <Card title="广告设置"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0, paddingTop: 0 }}
        extra={
          <Button type="dashed" onClick={eventHandlers.handleNewAdsStart}>
            <Icon type="plus" /> 新增
          </Button>
        }
      >
        <Form layout="horizontal" style={{ marginBottom: 20 }}>
          <Row gutter={16} type="flex" align="bottom">
            <Col span={6}>
              <Form.Item label="时间" className={style.formItem}>
                {getFieldDecorator('adsTimeRange')(
                  <DatePicker.RangePicker />
                )}
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="广告位置" className={style.formItem}>
                {getFieldDecorator('adsLocation')(
                  <Select placeholder="请选择广告位置" style={{ width: '100%' }}>
                    {
                      positionList.map(p => (
                        <Select.Option key={p.position} value={p.position}>{p.position_desc}</Select.Option>
                      ))
                    }
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col span={6}>
              <Button type="primary" className={style.actionBtn} onClick={() => handleQuery()}>搜索</Button>
              <Button type="primary" className={style.actionBtn} onClick={handlePrevDay}>上一天</Button>
              <Button type="primary" className={style.actionBtn} onClick={handleNextDay}>下一天</Button>
            </Col>
          </Row>
        </Form>
        <Table
          rowKey={record => record.id}
          columns={columns}
          onChange={handleTableChange}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.opManage.adsTotal,
            showTotal: () => <span>总共 {props.opManage.adsTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          scroll={{ x: 'max-content' }}
          dataSource={props.opManage.adsList}
        />
      </Card>
      <Modal
        title="广告设置"
        width={600}
        visible={props.opManage.modalVisibility === 'ads'}
        onCancel={eventHandlers.handleAdsCancel}
        onOk={eventHandlers.handleAdsSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="广告位置">
            {getFieldDecorator('editingAdsLocation', {
              rules: [{ required: true, message: '请选择广告位置' }],
              initialValue: editingAds.locationVal,
            })(
              <Select placeholder="请选择广告位置" style={{ width: '100%' }}>
                {
                  positionList.map(p => (
                    <Select.Option key={p.position} value={p.position}>{p.position_desc}</Select.Option>
                  ))
                }
              </Select>
            )}
          </Form.Item>
          <Form.Item label="跳转类型">
            {getFieldDecorator('editingJumpType', {
              rules: [{ required: true, message: '请选择跳转类型' }],
              initialValue: editingAds.jumpTypeVal,
            })(
              <Select placeholder="请选择跳转类型" style={{ width: '100%' }}>
                {
                  jumpTypeList.map(p => (
                    <Select.Option key={p.jump_type} value={p.jump_type}>{p.jump_type_zh}</Select.Option>
                  ))
                }
              </Select>
            )}
          </Form.Item>
          <Form.Item label="链接">
            {getFieldDecorator('editingAdsUrl', {
              rules: [{ required: true, message: '请输入广告链接' }],
              initialValue: editingAds.link,
            })(<Input />)}
          </Form.Item>
          <Form.Item label="状态">
            {getFieldDecorator('editingState', { initialValue: editingAds.stateVal == 1 })(
              <Checkbox>启用</Checkbox>
            )}
          </Form.Item>
          <Form.Item label="广告图片">
            {getFieldDecorator('editingAdsPhoto', {
              valuePropName: 'fileList',
              getValueFromEvent: normFile,
            })(
              <Upload name="editingAdsPhoto"
                listType="picture-card"
                className={style.imgUploader}
                showUploadList={false}
                action={null}
                accept={null}
                withCredentials
                beforeUpload={eventHandlers.handleImgBeforeUpload}
                onChange={eventHandlers.handleImgUpload}
              >
                {adsUploadImg && adsUploadImg.previewUrl ?
                  <img src={adsUploadImg.previewUrl} />
                  :
                  <div>
                    <Icon type={loading ? 'loading' : 'plus'} />
                    <div className="ant-upload-text">上传</div>
                  </div>
                }
              </Upload>
            )}
          </Form.Item>
          <Form.Item label="投放时间">
            {getFieldDecorator('editingPublishTime', {
              initialValue: moment(editingAds.publishTime),
            })(<DatePicker />)}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(AdsSettings);
