import React, { useEffect } from "react";
import { Breadcrumb, Row, Col } from "antd";
import AdsSettings from "./AdsSettings";
import HotSearchSettings from "./HotSearchSettings";
import PaymentSettings from "./PaymentSettings";
import ShareSettings from "./ShareSettings";

import connectWithAuth from "../../../core/connectWithAuth";

import style from "./style.scss";

const OpManagePage = (props) => {

  useEffect(() => {
    props.dispatch({ type: 'opManage/fetchCommonList' });
  }, []);

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>运营管理</Breadcrumb.Item>
          <Breadcrumb.Item>基础设置</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <AdsSettings {...props} />
        <HotSearchSettings {...props} />
        <Row gutter={8}>
          <Col span={10}>
            <PaymentSettings {...props} />
          </Col>
          <Col span={10}>
            <ShareSettings {...props} />
          </Col>
        </Row>
      </div>
    </div>
  )
};

export default connectWithAuth(({ opManage }) => ({ opManage }))(OpManagePage);
