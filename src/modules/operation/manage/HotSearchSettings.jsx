import React, { useState, useEffect } from "react";
import moment from "moment";
import style from "./style.scss";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Popconfirm, Table, Modal, Input, Checkbox, Upload, InputNumber } from "antd";
import { ADV_STATUS } from "../../../common/constants";

const HotSearchSettings = (props) => {

  const { getFieldDecorator, validateFields, resetFields, setFieldsValue, getFieldsValue } = props.form;
  const { editingHotSearch = {}, adsUploadImg } = props.opManage;

  const [loading, setLoading] = useState(false);
  const [uploadingFile, setUploadingFile] = useState(null);

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const resetForm = () => {
    resetFields([
      "editingHotSearchText", "editingHotSearchOrder",
      "editingHotSearchState", "editingEffectiveTime"
    ]);
  };

  const eventHandlers = {
    handleNewHotSearchStart(e) {
      resetForm();
      props.dispatch({
        type: 'opManage/editHotSearchStart',
        payload: { modalVisibility: 'hotSearch', editingHotSearch: {} },
      });
    },
    handleEditHotSearchStart(record) {
      resetForm();
      props.dispatch({
        type: 'opManage/editHotSearchStart',
        payload: { modalVisibility: 'hotSearch', editingHotSearch: record },
      });
    },
    async handleHotSearchDelete(record) {
      await props.dispatch({
        type: 'opManage/deleteHotSearch',
        payload: { hot_word_id: record.id },
      });
      handleQuery();
    },
    handleHotSearchCancel(e) {
      props.dispatch({
        type: 'opManage/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    async handleUpdateStatus(record, status) {
      await props.dispatch({
        type: 'opManage/updateHotSearchStatus',
        payload: { 
          hot_word_id: record.id, status
        },
      });
      handleQuery();
    },
    handleHotSearchSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'opManage/saveHotSearch',
          payload: values,
        });
        await props.dispatch({
          type: 'opManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const handleTableChange = (pagination) => {
    console.log('table changed: ', pagination);
    setPagination(pagination);
  };

  const handleQuery = () => {
    validateFields((err, values) => {
      props.dispatch({
        type: 'opManage/fetchHotSearchList',
        form: values,
        pagination,
      });
    });
  };

  const handlePrevDay = () => {
    const values = getFieldsValue();
    const timeRange = values.hotSearchTimeRange;
    if (!timeRange) return;
    const newTimeRange = [
      timeRange[0].add(-1, 'days'),
      timeRange[1].add(-1, 'days')
    ];
    setFieldsValue({
      hotSearchTimeRange: newTimeRange,
    });
    props.dispatch({
      type: 'opManage/fetchHotSearchList', payload: {
        form: {
          ...values,
          hotSearchTimeRange: newTimeRange,
        },
        pagination,
      }
    });
  };

  const handleNextDay = () => {
    const values = getFieldsValue();
    const timeRange = values.hotSearchTimeRange;
    if (!timeRange) return;
    const newTimeRange = [
      timeRange[0].add(1, 'days'),
      timeRange[1].add(1, 'days')
    ];
    setFieldsValue({
      hotSearchTimeRange: newTimeRange,
    });
    props.dispatch({
      type: 'opManage/fetchHotSearchList', payload: {
        form: {
          ...values,
          hotSearchTimeRange: newTimeRange,
        },
        pagination,
      }
    });
  };

  const normFile = e => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  const columns = [
    {
      title: '序号',
      render: (text, record) => {
        return <span key={record.id}>{pagination.pageSize * (pagination.current - 1) + record.sn}</span>
      }
    },
    { title: '创建时间', dataIndex: 'createdTime' },
    { title: '热词设置', dataIndex: 'hotSearchText' },
    { title: '顺序', dataIndex: 'order' },
    { title: '状态', dataIndex: 'state' },
    { title: '显示时间', dataIndex: 'effectiveTime' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleHotSearchDelete(record)}>
            <a>删除</a>
          </Popconfirm>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditHotSearchStart(record)}>编辑</Button>
          {
            record.stateVal == 1 ?
              <Popconfirm title="确认禁用吗？" onConfirm={e => eventHandlers.handleUpdateStatus(record, ADV_STATUS.DISABLED)}>
                <Button className={style.rowBtn} type="link">禁用</Button>
              </Popconfirm> : undefined
          }
          {
            record.stateVal == 2 ?
              <Popconfirm title="确认启用吗？" onConfirm={e => eventHandlers.handleUpdateStatus(record, ADV_STATUS.ENABLED)}>
                <Button className={style.rowBtn} type="link">启用</Button>
              </Popconfirm> : undefined
          }
        </span>
      )
    },
  ];

  useEffect(handleQuery, [pagination]);

  return (
    <React.Fragment>
      <Card title="热搜词设置"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingTop: 0 }}
        extra={
          <Button type="dashed" onClick={eventHandlers.handleNewHotSearchStart}>
            <Icon type="plus" /> 新增
          </Button>
        }
      >
        <Form layout="horizontal" style={{ marginBottom: 20 }}>
          <Row gutter={16} type="flex" align="bottom">
            <Col span={6}>
              <Form.Item label="时间" className={style.formItem}>
                {getFieldDecorator('hotSearchTimeRange')(
                  <DatePicker.RangePicker />
                )}
              </Form.Item>
            </Col>
            <Col span={6}>
              <Button type="primary" className={style.actionBtn} onClick={handleQuery}>搜索</Button>
              <Button type="primary" className={style.actionBtn} onClick={handlePrevDay}>上一天</Button>
              <Button type="primary" className={style.actionBtn} onClick={handleNextDay}>下一天</Button>
            </Col>
          </Row>
        </Form>
        <Table
          columns={columns}
          rowKey={record => record.id}
          onChange={handleTableChange}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.opManage.hotSearchTotal,
            showTotal: () => <span>总共 {props.opManage.hotSearchTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          scroll={{ x: 'max-content' }}
          dataSource={props.opManage.hotSearchList}
        />
      </Card>
      <Modal
        title="广告设置"
        width={600}
        visible={props.opManage.modalVisibility === 'hotSearch'}
        onCancel={eventHandlers.handleHotSearchCancel}
        onOk={eventHandlers.handleHotSearchSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="热词设置">
            {getFieldDecorator('editingHotSearchText', {
              rules: [{ required: true, message: '请输入热词文本' }],
              initialValue: editingHotSearch.hotSearchText
            })(<Input />)}
          </Form.Item>
          <Form.Item label="顺序">
            {getFieldDecorator('editingHotSearchOrder', {
              rules: [{ required: true, message: '请输入顺序' }],
              initialValue: editingHotSearch.order,
            })(<InputNumber />)}
          </Form.Item>
          <Form.Item label="状态">
            {getFieldDecorator('editingHotSearchState', {
              initialValue: editingHotSearch.stateVal == 1,
              valuePropName: 'checked',
            })(
              <Checkbox>启用</Checkbox>
            )}
          </Form.Item>
          <Form.Item label="显示时间">
            {getFieldDecorator('editingEffectiveTime', {
              initialValue: moment(editingHotSearch.effectiveTime)
            })(<DatePicker />)}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(HotSearchSettings);
