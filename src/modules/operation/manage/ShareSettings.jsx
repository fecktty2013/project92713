import React, { useEffect } from "react";

import style from "./style.scss";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber } from "antd";

const ShareSettings = (props) => {

  const { getFieldDecorator, validateFields } = props.form;

  const eventHandlers = {
    handleShareSubmit(e) {
      validateFields(async (err, values) => {
        await props.dispatch({
          type: 'opManage/saveShare',
          payload: {
            content: values.editingShareSettings
          }
        });
        Modal.success({
          title: '提示',
          content: '保存成功',
          okText: '确认',
        });
      });
    },
  };

  useEffect(() => {
    props.dispatch({ type: 'opManage/fetchShareSettings' });
  }, []);

  return (
    <React.Fragment>
      <Card title="分享内容"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form>
          <Form.Item>
            {getFieldDecorator('editingShareSettings', {
              initialValue: props.opManage.shareSettings,
            })(
              <Input.TextArea rows={6} />
            )}
          </Form.Item>
          <Form.Item>
            <Button style={{ width: 160, height: 40 }} type="primary" onClick={eventHandlers.handleShareSubmit}>确定</Button>
          </Form.Item>
        </Form>
      </Card>
    </React.Fragment>
  )
};

export default Form.create()(ShareSettings);
