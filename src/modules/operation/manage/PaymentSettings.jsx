import React, { useEffect } from "react";
import moment from "moment";
import style from "./style.scss";
import { Card, Button, Icon, Form, Row, Col, Popconfirm, Select, Table, Modal, Input, Checkbox, Upload, InputNumber } from "antd";
import { MEDIA_TYPE, ADV_STATUS } from "../../../common/constants";

const PaymentSettings = (props) => {

  const { getFieldDecorator, validateFields, resetFields } = props.form;

  const { editingPayment } = props.opManage;

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const resetForm = () => {
    resetFields([
      "editingType", "editingWatchTimeLength", "editingDownloadTimeLength", "editingState"
    ]);
  };

  const handleQuery = () => {
    props.dispatch({ type: 'opManage/fetchPaymentList' });
  };

  const eventHandlers = {
    handleNewPaymentStart(e) {
      resetForm();
      props.dispatch({
        type: 'opManage/editPaymentStart',
        payload: { modalVisibility: 'payment', editingPayment: {} },
      });
    },
    handleEditPaymentStart(record) {
      resetForm();
      props.dispatch({
        type: 'opManage/editPaymentStart',
        payload: { modalVisibility: 'payment', editingPayment: record },
      });
    },
    async handlePaymentDelete(record) {
      await props.dispatch({
        type: 'opManage/deletePayment',
        payload: { media_period_id: record.id },
      });
      handleQuery();
    },
    async handleUpdatePaymentStatus(record, status) {
      await props.dispatch({
        type: 'opManage/updatePaymentStatus',
        payload: {
          media_period_id: record.id, status
        },
      });
      handleQuery();
    },
    handlePaymentCancel(e) {
      props.dispatch({
        type: 'opManage/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    handlePaymentSubmit(e) {
      validateFields(async (err, values) => {
        console.log(values);
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'opManage/savePayment',
          payload: values,
        });
        await props.dispatch({
          type: 'opManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const columns = [
    { title: '序号', dataIndex: 'sn' },
    { title: '类型', dataIndex: 'type' },
    {
      title: '观看',
      key: 'viewL',
      render: (text, record) => (
        <span>{moment.duration(record.watchTimeLength, "seconds").humanize()}</span>
      )
    },
    {
      title: '下载',
      key: 'downloadL',
      render: (text, record) => (
        <span>{moment.duration(record.downloadTimeLength, "seconds").humanize()}</span>
      )
    },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handlePaymentDelete(record)}>
            <a>删除</a>
          </Popconfirm>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditPaymentStart(record)}>编辑</Button>
          {
            record.stateVal == 1 ?
              <Popconfirm title="确认禁用吗？" onConfirm={e => eventHandlers.handleUpdatePaymentStatus(record, ADV_STATUS.DISABLED)}>
                <Button className={style.rowBtn} type="link">禁用</Button>
              </Popconfirm> : undefined
          }
          {
            record.stateVal == 2 ?
              <Popconfirm title="确认启用吗？" onConfirm={e => eventHandlers.handleUpdatePaymentStatus(record, ADV_STATUS.ENABLED)}>
                <Button className={style.rowBtn} type="link">启用</Button>
              </Popconfirm> : undefined
          }
        </span>
      )
    },
  ];

  useEffect(handleQuery, []);

  return (
    <React.Fragment>
      <Card title="付费有效设置"
        bordered={false}
        className={style.paymentCard}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
        extra={
          <Button type="dashed" onClick={eventHandlers.handleNewPaymentStart}>
            <Icon type="plus" /> 新增
          </Button>
        }
      >
        <Table
          rowKey={record => record.id}
          columns={columns}
          pagination={false}
          scroll={{ x: 'max-content' }}
          dataSource={props.opManage.paymentList}
        />
      </Card>
      <Modal
        title="付费有效设置"
        width={600}
        visible={props.opManage.modalVisibility === 'payment'}
        onCancel={eventHandlers.handlePaymentCancel}
        onOk={eventHandlers.handlePaymentSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="媒体类型">
            {getFieldDecorator('editingType', {
              rules: [{ required: true, message: '请选择类型'}],
              initialValue: editingPayment.typeVal,
            })(
              <Select>
                <Select.Option value={MEDIA_TYPE.VIDEO}>视频</Select.Option>
                <Select.Option value={MEDIA_TYPE.AUDIO}>音频</Select.Option>
                <Select.Option value={MEDIA_TYPE.PHOTO}>相册</Select.Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item label="观看">
            {getFieldDecorator('editingWatchTimeLength', {
              rules: [{ required: true, message: '请输入观看设置'}],
              initialValue: editingPayment.watchTimeLength,
            })(<InputNumber />)}
          </Form.Item>
          <Form.Item label="下载">
            {getFieldDecorator('editingDownloadTimeLength', {
              initialValue: editingPayment.downloadTimeLength,
            })(
              <InputNumber />
            )}
          </Form.Item>
          <Form.Item label="状态">
            {getFieldDecorator('editingState', {
              initialValue: editingPayment.stateVal == 1,
              valuePropName: 'checked',
            })(<Checkbox>启用</Checkbox>)}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(PaymentSettings);
