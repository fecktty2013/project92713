import $http, { APP_BASE_URL } from "../../../core/http";

export function fetchAdvertCommon() {
  return $http.get("/operation/advert/common");
}

export function uploadImage(file) {
  const formData = new FormData();
  formData.append("file", file);

  return $http.post("/api/upload_img", formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    'baseURL': APP_BASE_URL,
  });
}

export function fetchAdvertList(payload) {
  return $http.get("/operation/advert/list", {
    params: payload,
  });
}

export function saveAdvert(payload) {
  return $http.post("/operation/advert", payload);
}

export function deleteAdvert(payload) {
  return $http.delete("/operation/advert", {
    params: payload,
  });
}

export function getAdvertDetail(payload) {
  return $http.get("/operation/advert", {
    params: payload,
  });
}

export function getHotSearchList(payload) {
  return $http.get("/operation/hot_word/list", {
    params: payload,
  });
}

export function saveHotSearch(payload) {
  return $http.post("/operation/hot_word", payload);
}

export function deleteHotSearch(payload) {
  return $http.delete("/operation/hot_word", {
    params: payload,
  });
}

export function updateHotSearchStatus(payload) {
  return $http.post("operation/hot_word/status", payload);
}

export function getPaymentList() {
  return $http.get("/operation/media_period/list");
}

export function savePayment(payload) {
  return $http.post("/operation/media_period", payload);
}

export function deletePayment(payload) {
  return $http.delete("/operation/media_period", {
    params: payload,
  });
}

export function updatePaymentStatus(payload) {
  return $http.post("/operation/media_period/status", payload);
}

export function getShare() {
  return $http.get("/operation/share");
}

export function saveShare(payload) {
  return $http.post("/operation/share", payload);
}
