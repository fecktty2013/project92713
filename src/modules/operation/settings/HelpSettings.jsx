import React, { useState, useEffect } from "react";

import style from "./style.scss";
import { Card, Button, Icon, Form, Row, Col, Popconfirm, Select, Table, Modal, Input, Checkbox, Upload, InputNumber } from "antd";

const HelpSettings = (props) => {

  const { getFieldDecorator, validateFields, resetFields } = props.form;
  const { editingHelp } = props.opSettings;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleQuery = () => {
    validateFields((err, values) => {
      props.dispatch({
        type: 'opSettings/fetchHelpList',
        pagination,
      });
    });
  };

  const resetForm = () => {
    resetFields([
      "editingCategory", "editingQuestion", "editingAnswer"
    ]);
  };

  const handleTableChange = (pagination) => {
    console.log('table changed: ', pagination);
    setPagination(pagination);
  };

  const eventHandlers = {
    handleNewHelpStart(e) {
      resetForm();
      props.dispatch({
        type: 'opSettings/editHelpStart',
        payload: { modalVisibility: 'help', editingHelp: {} },
      });
    },
    handleEditHelpStart(record) {
      resetForm();
      props.dispatch({
        type: 'opSettings/editHelpStart',
        payload: { modalVisibility: 'help', editingHelp: record },
      });
    },
    handleHelpCancel(e) {
      props.dispatch({
        type: 'opSettings/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    async handleHelpDelete(record) {
      await props.dispatch({
        type: 'opSettings/deleteHelp',
        payload: { question_id: record.id },
      });
      handleQuery();
    },
    handleHelpSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'opSettings/saveHelp',
          payload: values,
        });
        await props.dispatch({
          type: 'opSettings/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const columns = [
    {
      title: '序号',
      render: (text, record) => {
        return <span key={record.id}>{pagination.pageSize * (pagination.current - 1) + record.sn}</span>
      }
    },
    { title: '分类', dataIndex: 'category' },
    { title: '问题', dataIndex: 'question' },
    { title: '答案', dataIndex: 'answer' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleHelpDelete(record)}>
            <a>删除</a>
          </Popconfirm>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditHelpStart(record)}>编辑</Button>
        </span>
      )
    },
  ];

  useEffect(handleQuery, [pagination]);

  return (
    <React.Fragment>
      <Card title="帮助设置"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
        extra={
          <Button type="dashed" onClick={eventHandlers.handleNewHelpStart}>
            <Icon type="plus" /> 新增
          </Button>
        }
      >
        <Table
          rowKey="id"
          columns={columns}
          onChange={handleTableChange}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.opSettings.helpTotal,
            showTotal: () => <span>总共 {props.opSettings.helpTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          scroll={{ x: 'max-content' }}
          dataSource={props.opSettings.helpList}
        />
      </Card>
      <Modal
        title="帮助设置"
        width={600}
        visible={props.opSettings.modalVisibility === 'help'}
        onCancel={eventHandlers.handleHelpCancel}
        onOk={eventHandlers.handleHelpSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="分类">
            {getFieldDecorator('editingCategory', {
              rules: [{ required: true, message: '请输入分类'}],
              initialValue: editingHelp.category,
            })(
            <Input />
            )}
          </Form.Item>
          <Form.Item label="问题">
            {getFieldDecorator('editingQuestion', {
              rules: [{ required: true, message: '请输入问题'}],
              initialValue: editingHelp.question
            })(<Input />)}
          </Form.Item>
          <Form.Item label="答案">
            {getFieldDecorator('editingAnswer', {
              initialValue: editingHelp.answer
            })(
              <Input />
            )}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(HelpSettings);
