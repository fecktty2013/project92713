import React from "react";
import { Breadcrumb, Row, Col } from "antd";
import BulletinSettings from "./BulletinSettings";
import HelpSettings from "./HelpSettings";
import UserFeedback from "./UserFeedback";
import AppLaunchSettings from "./AppLaunchSettings";

import connectWithAuth from "../../../core/connectWithAuth";

import style from "./style.scss";

const OpSettingsPage = (props) => {

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>运营管理</Breadcrumb.Item>
          <Breadcrumb.Item>运营设置</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <BulletinSettings {...props} />
        <HelpSettings {...props} />
        <UserFeedback {...props} />
      </div>
    </div>
  )
};

export default connectWithAuth(({ opSettings }) => ({ opSettings }))(OpSettingsPage);
