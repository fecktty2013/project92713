import React, { useState, useEffect } from "react";

import style from "./style.scss";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber } from "antd";

const UserFeedback = (props) => {

  const { getFieldDecorator, validateFields, resetFields } = props.form;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const handleTableChange = (pagination) => {
    console.log('table changed: ', pagination);
    setPagination(pagination);
  };

  const handleQuery = (e) => {
    props.dispatch({
      type: 'opSettings/fetchFeedbackList',
      pagination,
    });
  };

  const eventHandlers = {
    handleReplyStart(record) {
      props.dispatch({
        type: 'opSettings/replyFeedbackStart',
        payload: {
          modalVisibility: 'feedback',
          replyingFeedback: record,
        },
      });
    },
    handleReplyCancel(e) {
      props.dispatch({
        type: 'opSettings/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    handleReplySubmit(e) {
      validateFields(async (err, values) => {
        console.log(values);
        await props.dispatch({
          type: 'opSettings/submitReply',
          payload: values,
        });
        await props.dispatch({
          type: 'opSettings/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
  };

  const columns = [
    {
      title: '序号',
      render: (text, record) => {
        return <span key={record.id}>{pagination.pageSize * (pagination.current - 1) + record.sn}</span>
      }
    },
    { title: '反馈问题', dataIndex: 'summary' },
    { title: '具体内容', dataIndex: 'detail' },
    {
      title: '用户昵称（id）',
      render: (text, record) => (<span>{`${record.userName}(${record.userId})`}</span>)
    },
    { title: '回复内容', dataIndex: 'reply' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type="link"
            className={style.rowBtn}
            onClick={() => eventHandlers.handleReplyStart(record)}>回复</Button>
        </span>
      )
    },
  ];

  useEffect(handleQuery, [pagination]);

  return (
    <React.Fragment>
      <Card title="用户反馈"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Table
          rowKey="id"
          columns={columns}
          onChange={handleTableChange}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.opSettings.feedbackTotal,
            showTotal: () => <span>总共 {props.opSettings.feedbackTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          // scroll={{ x: 'max-content' }}
          dataSource={props.opSettings.feedbackList}
        />
      </Card>
      <Modal
        title="回复"
        width={600}
        visible={props.opSettings.modalVisibility === 'feedback'}
        onCancel={eventHandlers.handleReplyCancel}
        onOk={eventHandlers.handleReplySubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form>
          <Form.Item style={{ width: '100%' }}>
            {getFieldDecorator('editingFeedbackReply', {
              rules: [{ required: true, message: '请输入回复内容'}],
            })(<Input.TextArea style={{ width: '100%' }} rows={6} placeholder="这里是回复内容" />)}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(UserFeedback);
