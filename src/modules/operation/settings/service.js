import $http, { APP_BASE_URL } from "../../../core/http";

export function fetchCommon() {
  return $http.get("/operation/advert/common");
}

export function fetchBulletinList(pagination) {
  return $http.get("/operation/notify/list");
}

export function saveBulletin(payload) {
  return $http.post("/operation/notify", payload);
}

export function deleteBulletin(payload) {
  return $http.delete("/operation/notify", {
    params: payload,
  });
}

export function fetchHelpList(pagination) {
  return $http.get("/operation/faq/list", {
    params: pagination,
  });
}

export function saveHelp(payload) {
  return $http.post("/operation/faq", payload);
}

export function deleteHelp(payload) {
  return $http.delete("/operation/faq", {
    params: payload,
  });
}

export function fetchFeedbackList(pagination) {
  return $http.get("/operation/feedback/list", pagination);
}

export function replyFeedback(payload) {
  return $http.post("/operation/feedback/reply", payload);
}
