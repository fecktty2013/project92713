import { routerRedux } from 'dva/router';
import * as service from './service';

const initialState = {
  bulletinList: [],
  bulletinTotal: 0,
  editingBulletin: {},

  helpList: [],
  helpTotal: 0,
  editingHelp: {},

  feedbackList: [],
  feedbackTotal: 0,
  replyingFeedback: {},

  adsUploadImg: null,
  welcomeUploadImg: null,

  modalVisibility: '',

  positionList: [],
  jumpTypeList: [],

};

export default {
  namespace: 'opSettings',

  state: initialState,

  subscriptions: {
  },

  effects: {
    *fetchCommonList(action, { put, call }) {
      const result = yield call(service.fetchCommon);
      yield put({
        type: 'stateChanged',
        payload: {
          positionList: result.data.position_list,
          jumpTypeList: result.data.jump_type_list,
        }
      });
    },
    *fetchBulletinList({ pagination }, { call, put, select }) {
      const httpResult = yield call(service.fetchBulletinList, pagination);
      const mappedData = httpResult.data.notify_list.map((item, idx) => ({
        sn: idx + 1,
        id: item.notify_id,
        bulletinDesc: item.content,
        jumpTypeVal: item.jump_type,
        target: item.target,
        effectiveTime: item.create_time,
      }));
      yield put({ type: 'stateChanged', payload: { bulletinList: mappedData, bulletinTotal: httpResult.data.total_count } });
    },
    *bulletinEditStart({ payload }, { put }) {
      yield put({
        type: 'stateChanged',
        payload: {
          modalVisibility: payload.modalVisibility,
          editingBulletin: payload.record,
        }
      });
    },
    *saveBulletin({ payload }, { select, put, call }) {
      const state = yield select(s => s.opSettings);
      const httpPayload = {
        content: payload.editingBulletinDesc,
        jump_type: payload.editingJumpType,
        target: payload.editingUrl,
      };
      if (state.editingBulletin.id) {
        httpPayload.notify_id = state.editingBulletin.id;
      }
      yield call(service.saveBulletin, httpPayload);
    },
    *deleteBulletin({ payload }, { put, call }) {
      const httpPayload = {
        notify_id: payload.id,
      };
      yield call(service.deleteBulletin, httpPayload);
    },
    *fetchHelpList({ pagination }, { put, call, select }) {
      const httpResult = yield call(service.fetchHelpList, {
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const helpList = httpResult.data.faq_list.map((item, idx) => ({
        id: item.question_id,
        sn: idx + 1,
        category: item.question_type,
        question: item.question,
        answer: item.answer,
      }));
      yield put({ type: 'stateChanged', payload: { helpTotal: httpResult.data.total_count, helpList } });
    },
    *editHelpStart({ payload }, { put }) {
      yield put({ type: 'stateChanged', payload });
    },
    *saveHelp({ payload }, { call, select }) {
      const opSettingState = yield select(state => state.opSettings);
      const httpPayload = {
        question_type: payload.editingCategory,
        question: payload.editingQuestion,
        answer: payload.editingAnswer
      }
      if (opSettingState.editingHelp.id) {
        httpPayload.question_id = opSettingState.editingHelp.id;
      }
      yield call(service.saveHelp, httpPayload);
    },
    *deleteHelp({ payload }, { call, put }) {
      yield call(service.deleteHelp, payload);
    },
    *fetchFeedbackList({ pagination }, { put, call }) {
      const httpResult = yield call(service.fetchFeedbackList, {
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const feedbackList = httpResult.data.feedback_list.map((item, idx) => ({
        id: item.feedback_id,
        sn: idx + 1,
        summary: item.question,
        detail: item.content,
        reply: item.reply,
        userId: item.user_id,
        userName: item.username,
      }));
      const feedbackTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { feedbackList, feedbackTotal } });
    },
    *replyFeedbackStart({ payload }, { put }) {
      yield put({ type: 'stateChanged', payload });
    },
    *submitReply({ payload }, { select, call }) {
      const opSettingState = yield select(state => state.opSettings);
      yield call(service.replyFeedback, {
        feedback_id: opSettingState.replyingFeedback.id,
        reply: payload.editingFeedbackReply,
      });
    },
    *uploadAdsImg({ payload: { adsUploadImg }}, { put }) {
      yield put({ type: 'stateChanged', payload: { adsUploadImg }})
    },
    *uploadWelcomeImg({ payload: { welcomeUploadImg }}, { put }) {
      yield put({ type: 'stateChanged', payload: { welcomeUploadImg }})
    },
    *setModalVisibility({ payload: { modalVisibility }}, { put }) {
      yield put({ type: 'stateChanged', payload: { modalVisibility }});
    },
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}