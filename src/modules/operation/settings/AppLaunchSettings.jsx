import React, { useState, useEffect } from "react";

import style from "./style.scss";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber } from "antd";

const AppLaunchSettings = (props) => {

  const { adsUploadImg, welcomeUploadImg } = props.opSettings;

  const [loading, setLoading] = useState(false);

  const eventHandlers = {
    handleAppAdsUpload(file) {
      let reader = new FileReader();
      reader.addEventListener('load', () => {
        props.dispatch({
          type: 'opSettings/uploadAdsImg',
          payload: {
            adsUploadImg: { uid: file.uid, name: file.name, status: 'done', url: reader.result, file }
          }
        });
      });
      reader.readAsDataURL(file);
      return false;
    },
    handleAppWelcomeUpload(file) {
      let reader = new FileReader();
      reader.addEventListener('load', () => {
        props.dispatch({
          type: 'opSettings/uploadWelcomeImg',
          payload: {
            welcomeUploadImg: { uid: file.uid, name: file.name, status: 'done', url: reader.result, file }
          }
        });
      });
      reader.readAsDataURL(file);
      return false;
    },
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 4 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12, offset: 2 },
    },
  };

  useEffect(() => {
    // props.dispatch({ type: 'opManage/fetchHelpList' });
  }, []);

  return (
    <React.Fragment>
      <Card title="APP启动页设置"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout} layout="horizontal">
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item wrapperCol={{ span: 12, offset: 2 }} label="开屏广告" className={style.formItem}>
                <Upload
                  name="appAdsPhoto"
                  listType="picture-card"
                  className={style.imgUploader}
                  showUploadList={false}
                  action={null}
                  accept={null}
                  withCredentials
                  beforeUpload={eventHandlers.handleAppAdsUpload}
                >
                  {adsUploadImg?.url ?
                    <img src={adsUploadImg.url} />
                    :
                    <div>
                      <Icon type={loading ? 'loading' : 'plus'} />
                      <div className="ant-upload-text">上传</div>
                    </div>
                  }
                </Upload>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item wrapperCol={{ span: 12, offset: 2 }} label="开屏动画" className={style.formItem}>
                <Upload
                  name="appWelcomePhoto"
                  listType="picture-card"
                  className={style.imgUploader}
                  showUploadList={false}
                  action={null}
                  accept={null}
                  withCredentials
                  beforeUpload={eventHandlers.handleAppWelcomeUpload}
                >
                  {welcomeUploadImg?.url ?
                    <img src={welcomeUploadImg.url} />
                    :
                    <div>
                      <Icon type={loading ? 'loading' : 'plus'} />
                      <div className="ant-upload-text">上传</div>
                    </div>
                  }
                </Upload>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>
    </React.Fragment>
  )
};

export default AppLaunchSettings;
