import React from "react";
import OpSettingsPage from "./OpSettingsPage";
import model from "./model";

export default {
  component: OpSettingsPage, model,
};
