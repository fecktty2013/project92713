import React, { useState, useEffect } from "react";
import moment from "moment";

import style from "./style.scss";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber, Popconfirm } from "antd";

const BulletinSettings = (props) => {

  const { getFieldDecorator, validateFields, resetFields } = props.form;

  const { editingBulletin, jumpTypeList } = props.opSettings;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });


  const resetForm = () => {
    resetFields([
      "editingBulletinDesc",
      "editingBulletinChannel",
      "editingEffectiveTime",
    ])
  };

  const handleQuery = (e) => {
    props.dispatch({
      type: 'opSettings/fetchBulletinList',
      pagination,
    });
  };

  const eventHandlers = {
    handleNewBulletinStart(record) {
      resetForm();
      props.dispatch({
        type: 'opSettings/bulletinEditStart',
        payload: { modalVisibility: 'bulletin', record: {} },
      });
    },
    handleEditBulletinStart(record) {
      resetForm();
      props.dispatch({
        type: 'opSettings/bulletinEditStart',
        payload: { 
          modalVisibility: 'bulletin',
          record,
        },
      });
    },
    handleBulletinCancel(e) {
      props.dispatch({
        type: 'opSettings/setModalVisibility',
        payload: { modalVisibility: '' },
      });
    },
    handleBulletinSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'opSettings/saveBulletin',
          payload: values,
        });
        await props.dispatch({
          type: 'opSettings/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
    async handleDeleteBulletin(record) {
      await props.dispatch({
        type: 'opSettings/deleteBulletin',
        payload: record,
      });
      handleQuery();
    }
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const columns = [
    {
      title: '序号',
      render: (text, record) => {
        return <span key={record.id}>{pagination.pageSize * (pagination.current - 1) + record.sn}</span>
      },
    },
    { title: '公告内容', dataIndex: 'bulletinDesc' },
    { title: '设置时间', dataIndex: 'effectiveTime' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleDeleteBulletin(record)}>
              <a>删除</a>
            </Popconfirm>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditBulletinStart(record)}>编辑</Button>
        </span>
      )
    },
  ];

  const handleTableChange = (pagination) => {
    console.log('table changed: ', pagination);
    setPagination(pagination);
  };

  useEffect(handleQuery, [pagination]);

  useEffect(() => {
    props.dispatch({ type: 'opSettings/fetchCommonList' });
  }, []);

  return (
    <React.Fragment>
      <Card title="公告设置"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
        extra={
          <Button type="dashed" onClick={eventHandlers.handleNewBulletinStart}>
            <Icon type="plus" /> 新增
          </Button>
        }
      >
        <Table
          rowKey="id"
          columns={columns}
          onChange={handleTableChange}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            total: props.opSettings.bulletinTotal,
            showTotal: () => <span>总共 {props.opSettings.bulletinTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          scroll={{ x: 'max-content' }}
          dataSource={props.opSettings.bulletinList}
        />
      </Card>
      <Modal
        title={props.opSettings.editingBulletin.id !== undefined ? '编辑公告' : '新增公告'}
        width={600}
        visible={props.opSettings.modalVisibility === 'bulletin'}
        onCancel={eventHandlers.handleBulletinCancel}
        onOk={eventHandlers.handleBulletinSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="公告内容">
            {getFieldDecorator('editingBulletinDesc', {
              rules: [{ required: true, message: '请输入公告内容'}],
              initialValue: editingBulletin.bulletinDesc
            })(<Input.TextArea rows={6} />)}
          </Form.Item>
          <Form.Item label="跳转类型">
            {getFieldDecorator('editingJumpType', {
              rules: [{ required: true, message: '请选择跳转类型' }],
              initialValue: editingBulletin.jumpTypeVal,
            })(
              <Select placeholder="请选择跳转类型" style={{ width: '100%' }}>
                {
                  jumpTypeList.map(p => (
                    <Select.Option key={p.jump_type} value={p.jump_type}>{p.jump_type_zh}</Select.Option>
                  ))
                }
              </Select>
            )}
          </Form.Item>
          <Form.Item label="跳转目标">
            {getFieldDecorator('editingUrl', {
              initialValue: editingBulletin.target,
            })(<Input />)}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(BulletinSettings);
