import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber, Popconfirm } from "antd";

import style from "./style.scss";

const AutoAlbumSelectTable = ({ albumRowSelection, albumColumns, allAlbums }) => {
  return <Table
    rowKey="id"
    size="small"
    rowSelection={albumRowSelection}
    columns={albumColumns}
    showHeader={false}
    pagination={false}
    scroll={{ x: false, y: 180 }}
    dataSource={allAlbums}
  />;
};

function areEqual(prevProps, nextProps) {
  return prevProps.allAlbums === nextProps.allAlbums;
}

export default React.memo(AutoAlbumSelectTable, areEqual);
