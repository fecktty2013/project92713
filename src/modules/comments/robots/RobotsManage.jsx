import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber, Radio, Popconfirm } from "antd";

import style from "./style.scss";

const RobotsManage = (props) => {

  const fileInput = React.createRef();

  const { getFieldDecorator, validateFields, getFieldValue } = props.form;

  const { uploadingImg, editingRobot } = props.robotsManage;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const [selectedRows, setSelectedRows] = useState([]);

  const normFile = e => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  const handleTableChange = (pagination) => {
    setPagination(pagination);
  };

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRows(selectedRowKeys);
    },
    getCheckboxProps: record => ({
      name: record.id.toString(),
    })
  }

  const handleQuery = () => {
    validateFields((err, values) => {
      props.dispatch({ type: 'robotsManage/fetchRobotsList', pagination });
    });
  }

  const uploadEvents = {
    handleUploadStart(e) {
      fileInput.current.click();
    },
    async handleUploadComplete(e) {
      console.log(Array.from(e.target.files));
      await props.dispatch({
        type: 'robotsManage/batchImportRobots',
        payload: {
          file: e.target.files[0],
        }
      });
      handleQuery();
    },
  };

  const eventHandlers = {
    handleImgBeforeUpload(file) {
      let reader = new FileReader();
      reader.addEventListener('load', () => {
        props.dispatch({
          type: 'robotsManage/uploadAvatarImg',
          payload: {
            uploadingImg: { uid: file.uid, name: file.name, status: 'done', url: reader.result, file }
          }
        });
      });
      reader.readAsDataURL(file);
      return false;
    },
    handleImgUpload(e) {

    },
    async handleBatchAdd(e) {
      const nums = getFieldValue("robotsNumber");
      if (!nums) {
        return;
      }
      await props.dispatch({
        type: 'robotsManage/batchAddRobots',
        payload: { count: nums }
      });
      handleQuery();
    },
    handleBatchDelete(e) {
      if (!selectedRows || selectedRows.length == 0) {
        message.warn("没有选中行，请先选择数据行", 5);
        return;
      }
      Modal.confirm({
        title: '批量删除',
        content: '确认批量删除选中的机器人吗？',
        onOk: async () => {
          await props.dispatch({
            type: 'robotsManage/batchDeleteRobots',
            payload: { ids: selectedRows }
          });
          await props.dispatch({
            type: 'robotsManage/setModalVisibility',
            payload: { modalVisibility: '' }
          });
          handleQuery();
        },
        onCancel: () => {
          props.dispatch({
            type: 'robotsManage/setModalVisibility',
            payload: { modalVisibility: '' }
          });
        }
      });
    },
    handleEditStart(record) {
      props.dispatch({ type: 'robotsManage/editRobotStart', payload: record });
    },
    handleEditSubmit(e) {
      validateFields(async (err, values) => {
        if (err) {
          console.log(err);
          return;
        }
        await props.dispatch({
          type: 'robotsManage/editRobotSave',
          payload: values,
        });
        await props.dispatch({
          type: 'robotsManage/setModalVisibility',
          payload: { modalVisibility: '' },
        });
        handleQuery();
      });
    },
    handleEditCancel(e) {
      props.dispatch({
        type: 'robotsManage/setModalVisibility',
        payload: {
          modalVisibility: ''
        }
      });
    },
    async handleDelete(record) {
      await props.dispatch({
        type: 'robotsManage/batchDeleteRobots',
        payload: {
          ids: [record.id]
        }
      });
      handleQuery();
    }
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const columns = [
    { title: '序号', dataIndex: 'sn' },
    {
      title: '机器人昵称',
      render: (text, record) => (
        <span>{`${record.robotName}(${record.robotId})`}</span>
      )
    },
    { title: '创建时间', dataIndex: 'createTime' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleEditStart(record)}>编辑</Button>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleDelete(record)}>
            <a>删除</a>
          </Popconfirm>
        </span>
      )
    },
  ];

  useEffect(handleQuery, [pagination]);

  return (
    <React.Fragment>
      <Card title="机器人管理"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
        extra={
          <React.Fragment>
            <Button type="dashed" className={style.headBtn} onClick={uploadEvents.handleUploadStart}>
              <Icon type="plus" /> 批量添加
          </Button>
            <Button type="dashed" className={style.headBtn} onClick={eventHandlers.handleBatchDelete}>
              <Icon type="close" /> 批量删除
          </Button>
          </React.Fragment>
        }
      >
        <input type="file"
          id="batchUploader"
          ref={fileInput}
          accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
          style={{ display: 'none' }}
          onChange={null} />
        <Form style={{ marginBottom: 24 }}>
          <Row>
            <Col span={6}>
              <Form.Item className={style.formItem}>
                {getFieldDecorator('robotsNumber')(
                  <Input type="number" />
                )}
              </Form.Item>
            </Col>
            <Col span={6}>
              <Button style={{ marginLeft: 12, transform: 'translateY(3px)' }} type="primary" onClick={eventHandlers.handleBatchAdd}>批量生成机器人</Button>
            </Col>
          </Row>
        </Form>
        <Table
          rowKey={record => record.id}
          rowSelection={rowSelection}
          columns={columns}
          onChange={handleTableChange}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            defaultPageSize: 5,
            total: props.robotsManage.robotTotal,
            showTotal: () => <span>总共 {props.robotsManage.robotTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          scroll={{ x: 'max-content' }}
          dataSource={props.robotsManage.robotList}
        />
      </Card>
      <Modal
        title="编辑机器人"
        width={600}
        visible={props.robotsManage.modalVisibility === 'robotEdit'}
        onCancel={eventHandlers.handleEditCancel}
        onOk={eventHandlers.handleEditSubmit}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="用户名">
            {getFieldDecorator('editUserName', {
              initialValue: editingRobot.robotName,
            })(
              <Input />
            )}
          </Form.Item>
          <Form.Item label="性别">
            {getFieldDecorator('editGender', {
              initialValue: editingRobot.gender,
            })(
              <Radio.Group>
                <Radio.Button value="1">男</Radio.Button>
                <Radio.Button value="2">女</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>
          <Form.Item label="头像">
            {getFieldDecorator('editAvatar', {
              valuePropName: 'fileList',
              getValueFromEvent: normFile,
            })(
              <Upload name="editAvatar"
                listType="picture-card"
                className={style.imgUploader}
                showUploadList={false}
                action={null}
                accept={null}
                withCredentials
                beforeUpload={eventHandlers.handleImgBeforeUpload}
                onChange={eventHandlers.handleImgUpload}
              >
                {uploadingImg?.previewUrl ?
                  <img src={uploadingImg.previewUrl} />
                  :
                  <div>
                    <div className="ant-upload-text">上传</div>
                  </div>
                }
              </Upload>
            )}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(RobotsManage);
