import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber, Popconfirm } from "antd";

import style from "./style.scss";
import AutoRobotSelectTable from "./AutoRobotSelectTable";
import AutoAlbumSelectTable from "./AutoAlbumSelectTable";

const AutomaticComments = (props) => {

  const { allRobots, allAlbums } = props.robotsManage;

  const [selectedRows, setSelectedRows] = useState([]);
  const [albumSelectedRows, setAlbumSelectedRows] = useState([]);
  const [commentContent, setCommentContent] = useState('');

  const handleAutomatedComment = () => {
    const contentList = commentContent.split(/\r?\n/);
    props.dispatch({
      type: 'robotsManage/batchAutomatedComment',
      payload: {
        user_ids: selectedRows,
        content_list: contentList,
        album_ids: albumSelectedRows,
      }
    });
  };

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRows(selectedRowKeys);
    },
    getCheckboxProps: record => ({
      name: record.id.toString(),
    })
  };

  const albumRowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setAlbumSelectedRows(selectedRowKeys);
    },
    getCheckboxProps: record => ({
      name: record.id.toString(),
    })
  };

  const onContentChanged = (e) => {
    setCommentContent(e.target.value);
  };

  const columns = [
    {
      title: '名称',
      dataIndex: 'robotName',
    }
  ];

  const albumColumns = [
    {
      title: '名称',
      dataIndex: 'name',
    }
  ];

  useEffect(() => {
    props.dispatch({ type: 'robotsManage/fetchAllRobotsList' });
    props.dispatch({ type: 'robotsManage/fetchAllAlbums' });
  }, []);

  return (
    <React.Fragment>
      <Card title="自动评论"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form>
          <Row gutter={32} type="flex" align="middle">
            <Col span={6}>
              <Form.Item className={style.formItem}>
                <h4>机器人 <Button type="link">选择机器人</Button></h4>
                <AutoRobotSelectTable rowSelection={rowSelection} columns={columns} allRobots={allRobots} />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item className={style.formItem}>
                <h4>评论内容</h4>
                <Input.TextArea rows={8} value={commentContent} onChange={onContentChanged} />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item className={style.formItem}>
                <h4>评论专辑</h4>
                <AutoAlbumSelectTable albumRowSelection={albumRowSelection} albumColumns={albumColumns} allAlbums={allAlbums} />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Popconfirm title="确认开始机器人评论操作吗？" onConfirm={handleAutomatedComment}>
                <Button style={{ width: 180 }} type="primary" size="large">开始评论</Button>
              </Popconfirm>
            </Col>
          </Row>
        </Form>
      </Card>
    </React.Fragment>
  )
};

export default AutomaticComments;
