import $http, { APP_BASE_URL } from "../../../core/http";

export function fetchRobotList(payload) {
  return $http.get("/user/robot/list", {
    params: payload,
  });
}

export function saveRobot(payload) {
  return $http.post("/user/robot", payload);
}

export function batchCreateRobots(payload) {
  return $http.post("/user/robot/list", payload);
}

export function batchImportRobots(payload) {
  var formData = new FormData();
  formData.append("file", payload.file);
  return $http.post("/user/robot/upload", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    }
  });
}

export function batchDeleteRobots(payload) {
  return $http.delete("/user/robot/list", {
    headers: {
      'Content-Type': 'application/json',
    },
    data: payload,
  });
}

export function saveRobotComments(payload) {
  return $http.post("/user/robot/comments", payload);
}

export function fetchAlbumList() {
  return $http.get("/media/album/list", {
    params: { page_no: 0, page_size: 100 },
  });
}

export function uploadImage(file) {
  const formData = new FormData();
  formData.append("file", file);

  return $http.post("/api/upload_img", formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    'baseURL': APP_BASE_URL,
  });
}
