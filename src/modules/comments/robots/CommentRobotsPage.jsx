import React from "react";
import { Breadcrumb, Row, Col } from "antd";
import connectWithAuth from "../../../core/connectWithAuth";
import RobotsManage from "./RobotsManage";
import AutomaticComments from "./AutomaticComments";

import style from "./style.scss";

const CommentRobotsPage = (props) => {

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>评论管理</Breadcrumb.Item>
          <Breadcrumb.Item>机器人管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <RobotsManage {...props} />
        <AutomaticComments {...props} />
      </div>
    </div>
  )
};

export default connectWithAuth(({ robotsManage }) => ({ robotsManage }))(CommentRobotsPage);
