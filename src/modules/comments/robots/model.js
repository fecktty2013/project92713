import { routerRedux } from 'dva/router';
import * as service from './service';

const initialState = {
  robotList: [],
  robotTotal: 0,
  editingRobot: {},

  uploadingImg: {},

  allRobots: [],
  allAlbums: [],

  modalVisibility: '',
};

export default {
  namespace: 'robotsManage',

  state: initialState,

  subscriptions: {
  },

  effects: {
    *fetchAllAlbums(payload, { put, call }) {
      const httpResult = yield call(service.fetchAlbumList);
      const allAlbums = httpResult.data.album_list.map((itm, idx) => ({
        id: itm.album_id,
        mediaType: itm.media_type,
        name: itm.title,
      }));
      yield put({ type: 'stateChanged', payload: { allAlbums }});
    },
    *fetchAllRobotsList(payload, { put, call }) {
      const httpResult = yield call(service.fetchRobotList, {
        page_no: 0,
        page_size: 1000,
      });
      const allRobots = httpResult.data.robot_list.map((itm, idx) => ({
        id: itm.user_id,
        sn: idx + 1,
        robotName: itm.username,
        robotId: itm.user_id,
        createTime: 'NA',
        gender: itm.sex,
        headImg: itm.head_img,
      }));
      yield put({ type: 'stateChanged', payload: { allRobots } });
    },
    *fetchRobotsList({ pagination, form = {} }, { put, call }) {
      const httpResult = yield call(service.fetchRobotList, {
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const robotList = httpResult.data.robot_list.map((itm, idx) => ({
        id: itm.user_id,
        sn: idx + 1,
        robotName: itm.username,
        robotId: itm.user_id,
        createTime: 'NA',
        gender: itm.sex,
        headImg: itm.head_img,
      }));
      const robotTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { robotList, robotTotal } });
    },
    *editRobotStart({ payload }, { put, call }) {
      let uploadingImg = {};
      if (payload.head_img) {
        uploadingImg.url = payload.head_img;
      }
      yield put({
        type: 'stateChanged',
        payload: { modalVisibility: 'robotEdit', editingRobot: payload, uploadingImg }
      });
    },
    *editRobotSave({ payload }, { put, select, call }) {
      const robotsState = yield select(state => state.robotsManage);
      const httpRequest = {
        username: payload.editUserName,
        sex: payload.editGender,
        head_img: robotsState.uploadingImg.url,
      }
      if (robotsState.editingRobot.id) {
        httpRequest.user_id = robotsState.editingRobot.id;
      }
      yield call(service.saveRobot, httpRequest);
    },
    *batchDeleteRobots({ payload }, { call, put }) {
      yield call(service.batchDeleteRobots, {
        user_ids: payload.ids,
      });
    },
    *batchAddRobots({ payload }, { call, put }) {
      yield call(service.batchCreateRobots, payload);
    },
    *batchImportRobots({ payload }, { call, put }) {
      yield call(service.batchImportRobots, {
        payload: {
          file: payload.file,
        }
      });
    },
    *batchAutomatedComment({ payload }, { call, put }) {
      yield call(service.saveRobotComments, payload);
    },
    *uploadAvatarImg({ payload: { uploadingImg } }, { call, put }) {
      const uploadResult = yield call(service.uploadImage, uploadingImg.file);
      yield put({
        type: 'stateChanged', payload: {
          uploadingImg: {
            ...uploadingImg,
            url: uploadResult.data.url,
            previewUrl: uploadingImg.url,
          },
        }
      });
    },
    *setModalVisibility({ payload: { modalVisibility }}, { put }) {
      yield put({ type: 'stateChanged', payload: { modalVisibility }});
    },
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}