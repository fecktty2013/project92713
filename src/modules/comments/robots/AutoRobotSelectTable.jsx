import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber, Popconfirm } from "antd";

import style from "./style.scss";

const AutoRobotSelectTable = ({ rowSelection, columns, allRobots }) => {
  return <Table
    rowKey="id"
    rowSelection={rowSelection}
    size="small"
    columns={columns}
    showHeader={false}
    pagination={false}
    scroll={{ x: false, y: 180 }}
    dataSource={allRobots}
  />;
};

function areEqual(prevProps, nextProps) {
  return prevProps.allRobots === nextProps.allRobots;
}

export default React.memo(AutoRobotSelectTable, areEqual);
