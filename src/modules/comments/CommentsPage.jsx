import React from "react";
import { Breadcrumb, Row, Col } from "antd";
import connectWithAuth from "../../core/connectWithAuth";
import NormalComments from "./NormalComments";
import OffenseComments from "./OffenseComments";

import style from "./style.scss";

const CommentsPage = (props) => {

  const parentEvents = {

  };

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>评论管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <NormalComments {...props} />
        <OffenseComments {...props} />
      </div>
    </div>
  )
};

export default connectWithAuth(({ commentsManage }) => ({ commentsManage }))(CommentsPage);
