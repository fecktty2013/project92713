import { routerRedux } from 'dva/router';
import * as service from './service';
import { COMMENT_RESULT } from '../../common/constants';

const initialState = {
  normalCommentList: [],
  normalCommentTotal: 0,

  offenseCommentList: [],
  offenseCommentTotal: 0,

  editingComment: {},

  modalVisibility: '',

};

export default {
  namespace: 'commentsManage',

  state: initialState,

  subscriptions: {
  },

  effects: {
    *fetchNormalCommentsList({ payload: { form, pagination }}, { put, call }) {
      const httpResult = yield call(service.fetchCommentList, {
        user_id: form.id,
        title: form.title,
        media_type: form.type,
        is_legal: "1",
        result: form.result,
        begin: form.publishTime && form.publishTime[0] && form.publishTime[0].format('YYYY-MM-DD HH:mm:ss'),
        end: form.publishTime && form.publishTime[1] && form.publishTime[1].format('YYYY-MM-DD HH:mm:ss'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const normalCommentList = httpResult.data.comment_list.map((c, idx) => ({
        id: c.comment_id,
        key: c.comment_id,
        sn: idx + 1,
        content: c.content,
        title: c.title,
        type: c.media_type_zh,
        typeVal: c.media_type,
        userName: c.username,
        userId: c.user_id,
        status: c.result_zh,
        statusVal: c.result,
        publishTime: c.publish_time
      }));
      const normalCommentTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { normalCommentList, normalCommentTotal } });
    },
    *batchReviewComments({ payload }, { call, put }) {
      yield call(service.batchReviewComments, {
        comment_id_list: payload.comments,
        result: payload.editReviewStatus
      });
    },
    *deleteComment({ payload }, { call, put }) {
      yield call(service.deleteComment, {
        comment_id: payload.id,
      });
    },
    *batchDeleteComments({ payload }, { call, put }) {
      yield call(service.batchDeleteComments, {
        comment_id_list: payload.comments,
      });
    },
    *editCommentStart({ payload }, { call, put }) {
      yield put({
        type: 'stateChanged',
        payload: {
          editingComment: payload.editingComment,
          modalVisibility: payload.modalVisibility,
        }
      });
    },
    *saveComment({ payload }, { select, call, put }) {
      const commentState = yield select(state => state.commentsManage);
      yield call(service.saveComment, {
        comment_id: commentState.editingComment.id,
        title: payload.editCommentTitle,
        content: payload.editCommentContent,
        result: payload.editCommentStatus,
      });
    },
    *fetchOffenseCommentsList({ payload: { form, pagination }}, { put, call }) {
      const httpResult = yield call(service.fetchCommentList, {
        user_id: form.id,
        title: form.title,
        media_type: form.type,
        is_legal: "0",
        result: form.result,
        begin: form.publishTime && form.publishTime[0] && form.publishTime[0].format('YYYY-MM-DD HH:mm:ss'),
        end: form.publishTime && form.publishTime[1] && form.publishTime[1].format('YYYY-MM-DD HH:mm:ss'),
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const offenseCommentList = httpResult.data.comment_list.map((c, idx) => ({
        id: c.comment_id,
        key: c.comment_id,
        sn: idx + 1,
        content: c.content,
        title: c.title,
        type: c.media_type_zh,
        typeVal: c.media_type,
        userName: c.username,
        userId: c.user_id,
        status: c.result_zh,
        statusVal: c.result,
        publishTime: c.publish_time
      }));
      const offenseCommentTotal = httpResult.data.total_count;
      yield put({ type: 'stateChanged', payload: { offenseCommentList, offenseCommentTotal } });
    },
    *setModalVisibility({ payload: { modalVisibility }}, { put }) {
      yield put({ type: 'stateChanged', payload: { modalVisibility }});
    },
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    },
  }
}