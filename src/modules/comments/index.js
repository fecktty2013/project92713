import React from "react";
import CommentsPage from "./CommentsPage";
import model from "./model";

export default {
  component: CommentsPage, model,
};
