import React, { useState, useEffect } from "react";
import { Card, Button, Icon, Form, Row, Col, DatePicker, Select, Table, Modal, Input, Checkbox, Upload, InputNumber, message, Radio, Popconfirm } from "antd";

import style from "./style.scss";
import { MEDIA_TYPE, COMMENT_RESULT } from "../../common/constants";

const OffenseComments = (props) => {

  const { getFieldDecorator, validateFields, resetFields } = props.form;

  const { offenseCommentList, offenseCommentTotal, editingComment } = props.commentsManage;

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const [selectedRows, setSelectedRows] = useState([]);

  const resetForm = () => {
    resetFields([

    ]);
  };

  const eventHandlers = {
    handleBatchCancel(e) {
      props.dispatch({
        type: 'commentsManage/setModalVisibility',
        payload: { modalVisibility: '' }
      });
    },
    handleBatchApproveStart(e) {
      props.dispatch({
        type: 'commentsManage/setModalVisibility',
        payload: { modalVisibility: 'approvingOffense' }
      });
    },
    handleBatchApproving(e) {
      validateFields(async (err, values) => {
        await props.dispatch({
          type: 'commentsManage/batchReviewComments',
          payload: {
            ...values,
            comments: selectedRows,
          }
        });
        await props.dispatch({
          type: 'commentsManage/setModalVisibility',
          payload: { modalVisibility: '' }
        });
        window.location.reload(false);
      });
    },
    handleCommentEditStart(record) {
      props.dispatch({
        type: 'commentsManage/editCommentStart',
        payload: {
          editingComment: record,
          modalVisibility: 'editingOffense',
        },
      });
    },
    handleCommentEditSave(e) {
      validateFields(async (err, values) => {
        await props.dispatch({
          type: 'commentsManage/saveComment',
          payload: values,
        });
        await props.dispatch({
          type: 'commentsManage/setModalVisibility',
          payload: { modalVisibility: '' }
        });
        window.location.reload(false);
      });
    },
    async handleCommentDelete(record) {
      await props.dispatch({
        type: 'commentsManage/deleteComment',
        payload: record,
      });
      setSelectedRows([]);
      handleQuery();
    },
    handleBatchDelete(e) {
      if (!selectedRows || selectedRows.length == 0) {
        message.warn("没有选中行，请先选择数据行", 5);
        return;
      }
      Modal.confirm({
        title: '批量删除',
        content: '确认批量删除选中的评论吗？',
        onOk: async () => {
          await props.dispatch({
            type: 'commentsManage/batchDeleteComments',
            payload: { comments: selectedRows }
          });
          await props.dispatch({
            type: 'commentsManage/setModalVisibility',
            payload: { modalVisibility: '' }
          });
          handleQuery();
        },
        onCancel: () => {
          props.dispatch({
            type: 'commentsManage/setModalVisibility',
            payload: { modalVisibility: '' }
          });
        }
      })
    },
  };

  const handleQuery = (p) => {
    if (!p) {
      p = pagination;
    }
    validateFields((err, values) => {
      props.dispatch({
        type: 'commentsManage/fetchOffenseCommentsList',
        payload: {
          form: values,
          pagination: p,
        }
      });
    });
  };

  const handleTableChange = (pagination) => {
    console.log('table changed: ', pagination);
    setPagination(pagination);
    handleQuery(pagination);
  };

  const rowSelection = {
    selectedRowKeys: selectedRows,
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRows(selectedRowKeys);
    },
    getCheckboxProps: record => ({
      name: record.id.toString(),
    })
  }


  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const columns = [
    {
      title: '序号',
      render: (text, record) => {
        return <span key={record.id}>{pagination.pageSize * (pagination.current - 1) + record.sn}</span>
      }
    },
    { title: '评论内容', dataIndex: 'content' },
    { title: '专辑标题', dataIndex: 'title' },
    { title: '类型', dataIndex: 'type' },
    { title: '用户昵称', dataIndex: 'userName' },
    { title: '用户id', dataIndex: 'userId' },
    { title: '状态', dataIndex: 'status' },
    { title: '发布时间', dataIndex: 'publishTime' },
    {
      title: '操作',
      key: 'action',
      render: (text, record) => (
        <span>
          <Button type="link" className={style.rowBtn} onClick={() => eventHandlers.handleCommentEditStart(record)}>编辑</Button>
          <Popconfirm title="确认删除吗？" onConfirm={() => eventHandlers.handleCommentDelete(record)}>
            <a>删除</a>
          </Popconfirm>
        </span>
      )
    },
  ];

  useEffect(handleQuery, []);

  return (
    <React.Fragment>
      <Card title="违规评论"
        bordered={false}
        headStyle={{ padding: '0 24px 0 0' }}
        style={{ width: '100%' }}
        bodyStyle={{ paddingBottom: 0 }}
        extra={
          <React.Fragment>
            <Button type="dashed" className={style.headBtn} onClick={eventHandlers.handleBatchApproveStart}>
              <Icon type="plus" /> 批量审核
          </Button>
            <Button type="dashed" className={style.headBtn} onClick={eventHandlers.handleBatchDelete}>
              <Icon type="close" /> 批量删除
          </Button>
          </React.Fragment>
        }
      >
        <Form layout="horizontal" style={{ marginBottom: 20 }}>
          <Row gutter={16} type="flex" align="bottom">
            <Col>
              <Form.Item label="id" className={style.formItem}>
                {getFieldDecorator('id')(
                  <Input />
                )}
              </Form.Item>
            </Col>
            <Col>
              <Form.Item label="标题" className={style.formItem}>
                {getFieldDecorator('title')(
                  <Input />
                )}
              </Form.Item>
            </Col>
            <Col>
              <Form.Item label="类型" className={style.formItem}>
                {getFieldDecorator('type')(
                  <Select>
                    <Select.Option value={MEDIA_TYPE.VIDEO}>视频</Select.Option>
                    <Select.Option value={MEDIA_TYPE.AUDIO}>音频</Select.Option>
                    <Select.Option value={MEDIA_TYPE.PHOTO}>相册</Select.Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col>
              <Form.Item label="时间" className={style.formItem}>
                {getFieldDecorator('publishTime')(
                  <DatePicker.RangePicker />
                )}
              </Form.Item>
            </Col>
            <Col>
              <Button type="primary" className={style.actionBtn} onClick={() => handleQuery()}>查询</Button>
            </Col>
          </Row>
        </Form>
        <Table
          rowKey={record => record.id}
          columns={columns}
          rowSelection={rowSelection}
          pagination={{
            ...pagination,
            showSizeChanger: true,
            showQuickJumper: true,
            defaultPageSize: 5,
            total: offenseCommentTotal,
            showTotal: () => <span>总共 {offenseCommentTotal} 条记录</span>,
            pageSizeOptions: ['5', '10', '20'],
          }}
          onChange={handleTableChange}
          // scroll={{ x: 'max-content' }}
          dataSource={offenseCommentList}
        />
      </Card>
      <Modal
        title="批量审核"
        width={600}
        visible={props.commentsManage.modalVisibility === 'approvingOffense'}
        onCancel={eventHandlers.handleBatchCancel}
        onOk={eventHandlers.handleBatchApproving}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="审核状态">
            {getFieldDecorator('editReviewStatus')(
              <Radio.Group>
                <Radio.Button value={COMMENT_RESULT.NORMAL}>通过</Radio.Button>
                <Radio.Button value={COMMENT_RESULT.BLOCK}>拒绝</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>
        </Form>
      </Modal>
      <Modal
        title="编辑评论"
        width={600}
        visible={props.commentsManage.modalVisibility === 'editingOffense'}
        onCancel={eventHandlers.handleBatchCancel}
        onOk={eventHandlers.handleCommentEditSave}
        bodyStyle={{ paddingBottom: 0 }}
      >
        <Form {...formItemLayout}>
          <Form.Item label="评论内容">
            {getFieldDecorator('editCommentContent', {
              initialValue: editingComment.content,
            })(
              <Input multiple />
            )}
          </Form.Item>
          <Form.Item label="审核状态">
            {getFieldDecorator('editCommentStatus')(
              <Radio.Group>
                <Radio.Button value={COMMENT_RESULT.NORMAL}>通过</Radio.Button>
                <Radio.Button value={COMMENT_RESULT.BLOCK}>拒绝</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  )
};

export default Form.create()(OffenseComments);
