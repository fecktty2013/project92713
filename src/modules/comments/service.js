import $http from "../../core/http";

export function getCommentDetail(payload) {
  return $http.get("/comment", {
    params: payload,
  });
}

export function saveComment(payload) {
  return $http.post("/comment", payload);
}

export function deleteComment(payload) {
  return $http.delete("/comment", {
    params: payload,
  });
}

export function getCommentStatus(payload) {
  return $http.get("/comment/review_result", {
    params: payload,
  });
}

export function fetchCommentList(payload) {
  return $http.get("/comment/list", {
    params: payload,
  });
}

export function batchReviewComments(payload) {
  return $http.post("/comment/list", payload);
}

export function batchDeleteComments(payload) {
  return $http.delete("/comment/list", {
    data: payload,
  }, {
    data: payload,
  });
}
