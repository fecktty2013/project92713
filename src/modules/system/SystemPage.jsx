import React, { useEffect } from "react";
import connectWithAuth from "../../core/connectWithAuth";
import { Breadcrumb, Card, Button, Icon, Modal } from "antd";
import SystemTable from "./SystemTable";

import style from "./style.scss";
import VersionHistory from "./VersionHistory";

const SystemPage = (props) => {

  return (
    <div className={style.container}>
      <div className="breadCrumbContainer">
        <Breadcrumb>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>系统管理</Breadcrumb.Item>
        </Breadcrumb>
      </div>
      <div className="pageBody">
        <SystemTable {...props} />
        <VersionHistory {...props} />
      </div>
    </div>
  );
};

export default connectWithAuth(({ system }) => ({ system }))(SystemPage);
// export default SystemPage;
