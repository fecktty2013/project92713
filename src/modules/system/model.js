import * as service from './service';
import cloneDeep from 'lodash/cloneDeep';

const initialState = {
  versionHistoryList: [],
  versionHistoryTotal: 0,

  latestAndroidVersionHistory: {},
  latestIOSVersionHistory: {},

  modalVisibility: false,
};

export default {
  namespace: 'system',

  state: initialState,

  subscriptions: {},

  effects: {
    *fetchVersionHistoryList({ pagination = { current: 1, pageSize: 5 } }, { call, put }) {
      const httpResult = yield call(service.fetchAppReleaseList, {
        page_no: pagination.current - 1,
        page_size: pagination.pageSize,
      });
      const versionHistoryList = httpResult.data.version_list.map((itm, idx) => ({
        id: itm.version_id,
        versionType: itm.platform,
        version: itm.version_seq,
        versionDisplay: itm.version,
        releaseNotes: itm.description,
        releaseLink: itm.url,
        isForceUpdate: itm.is_forced,
        isDisplayUpdate: itm.is_explicit,
        releaseTime: itm.publish_time,
      }));
      const versionHistoryTotal = httpResult.data.total_count;
      const latestAndroidVersionHistory = cloneDeep(versionHistoryList && versionHistoryList.filter(v => v.versionType === 'android')[0]) || {};
      const latestIOSVersionHistory = cloneDeep(versionHistoryList && versionHistoryList.filter(v => v.versionType === 'ios')[0]) || {};
      yield put({ type: 'stateChanged', payload: { versionHistoryList, versionHistoryTotal, latestAndroidVersionHistory, latestIOSVersionHistory } });
    },
    *saveVersion({ payload }, { call, put }) {
      yield call(service.saveAppRelease, payload);
    },
    *setModalVisibility({ payload: { visibility } }, { put }) {
      const payload = {
        modalVisibility: visibility
      };
      yield put({ type: 'stateChanged', payload });
    },
  },

  reducers: {
    stateChanged(state, { payload }) {
      return { ...state, ...payload };
    }
  }
}

