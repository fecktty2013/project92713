import React from "react";
import { Form, Input, Select, InputNumber, Radio, Checkbox, DatePicker } from "antd";

const NewVersionForm = (props) => {

  const { getFieldDecorator } = props.form;

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  return (
    <Form {...formItemLayout}>
      <Form.Item label="平台">
        {getFieldDecorator('newPlatform', {
          rules: [
            { required: true, message: '请选择平台类型' },
          ],
        })(
          <Radio.Group>
            <Radio.Button value="android">安卓</Radio.Button>
            <Radio.Button value="ios">苹果</Radio.Button>
          </Radio.Group>
        )}
      </Form.Item>
      <Form.Item label="版本">
        {getFieldDecorator('newVersion', {
          rules: [
            { required: true, message: '请输入版本号'},
          ],
        })(<Input />)}
      </Form.Item>
      <Form.Item label="下载链接">
        {getFieldDecorator('newReleaseLink', {
          rules: [
            { required: true, message: '请输入下载链接'},
          ],
        })(<Input />)}
      </Form.Item>
      <Form.Item label="更新内容">
        {getFieldDecorator('newReleaseNotes', {
          rules: [
            { required: true, message: '请输入更新内容'},
          ],
        })(<Input />)}
      </Form.Item>
      <Form.Item label="是否强制更新">
        {getFieldDecorator('newIsForced', {
          valuePropName: 'checked',
        })(
          <Checkbox />
        )}
      </Form.Item>
      <Form.Item label="是否显式更新">
        {getFieldDecorator('newIsExplicit', {
          valuePropName: 'checked',
        })(
          <Checkbox />
        )}
      </Form.Item>
      <Form.Item label="版本发布时间">
        {getFieldDecorator('newReleaseDate', {
          rules: [
            { required: true, message: '请选择版本发布时间' },
          ]
        })(
          <DatePicker />
        )}
      </Form.Item>
    </Form>
  );
}

export default NewVersionForm;
