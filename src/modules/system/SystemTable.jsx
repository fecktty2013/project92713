import React, { useEffect } from "react"
import { Card, Button, Icon, Form, Table, Select, Modal, Popconfirm, Row, Col, Checkbox, Input, InputNumber, message } from "antd";
import NewCategoryForm from "./NewVersionForm";

import style from "./style.scss";
import NewVersionForm from "./NewVersionForm";

const SystemTable = (props) => {

  const { getFieldsValue, getFieldDecorator } = props.form;

  const { latestAndroidVersionHistory, latestIOSVersionHistory } = props.system;

  const handleNewVersionStart = (e) => {
    props.dispatch({
      type: 'system/setModalVisibility',
      payload: { visibility: true }
    });
  };

  const handleNewVersionSave = (e) => {
    props.form.validateFields(async (err, values) => {
      if (err) {
        console.log(err);
        return;
      }
      await props.dispatch({
        type: 'system/saveVersion',
        payload: {
          platform: values.newPlatform,
          version: values.newVersion,
          url: values.newReleaseLink,
          description: values.newReleaseNotes,
          is_forced: values.newIsForced ? 1 : 0,
          is_explicit: values.newIsExplicit ? 1 : 0,
          publish_time: values.newReleaseDate && values.newReleaseDate.format('YYYY-MM-DD'),
        }
      });
      await props.dispatch({
        type: 'system/setModalVisibility',
        payload: { visibility: false }
      });
      Modal.success({
        title: '提示',
        content: '保存成功',
        okText: '确认',
        onOk: () => {
          window.location.reload(false);
        }
      })
    });
  };

  const handleNewVersionCancel = (e) => {
    props.dispatch({
      type: 'system/setModalVisibility',
      payload: { visibility: false }
    });
  };

  const handleNewVersionClose = (e) => {
    props.form.resetFields();
  };

  const handleSaveAndroid = async () => {
    if (latestAndroidVersionHistory.id === undefined) {
      return;
    }
    const values = getFieldsValue();
    await props.dispatch({
      type: 'system/saveVersion',
      payload: {
        version_id: latestAndroidVersionHistory.id,
        platform: 'android',
        version: values.androidVersionDisplay,
        url: values.androidDownloadUrl,
        description: values.androidReleaseNotes,
        is_forced: values.androidForceUpdate ? 1 : 0,
        is_explicit: values.androidExplicitUpdate ? 1 : 0,
        publish_time: latestAndroidVersionHistory.releaseTime,
      }
    });
    Modal.success({
      title: '提示',
      content: '保存成功',
      okText: '确认',
      onOk: () => {
        window.location.reload(false);
      }
    })
  };

  const handleSaveIOS = async () => {
    if (latestIOSVersionHistory.id === undefined) {
      return;
    }
    const values = getFieldsValue();
    await props.dispatch({
      type: 'system/saveVersion',
      payload: {
        version_id: latestIOSVersionHistory.id,
        platform: 'ios',
        version: values.iosVersionDisplay,
        url: values.iosDownloadUrl,
        description: values.iosReleaseNotes,
        is_forced: values.iosForceUpdate ? 1 : 0,
        is_explicit: values.iosExplicitUpdate ? 1 : 0,
        publish_time: latestIOSVersionHistory.releaseTime,
      }
    });
    Modal.success({
      title: '提示',
      content: '保存成功',
      okText: '确认',
      onOk: () => {
        window.location.reload(false);
      }
    })
  };

  return <React.Fragment>
    <Card title="更新管理"
      bordered={false}
      headStyle={{ padding: '0 24px 0 0' }}
      style={{ maxWidth: 860 }}
      className={style.systemTable}
      bodyStyle={{ paddingBottom: 0, paddingTop: 24 }}
      extra={
        <Button type="dashed" onClick={handleNewVersionStart}>
          <Icon type="plus" />新增
        </Button>
      }
    >
      <Form>
        <Row gutter={32}>
          <Col span={12} className={style.tableInfo}>
            <Row>
              <Col span={8} className={style.label}>当前版本</Col>
              <Col span={16} className={style.val}>
                {getFieldDecorator('androidPlatform', {
                  initialValue: '安卓',
                })(
                  <Input />
                )}
              </Col>
            </Row>
            <Row>
              <Col span={8} className={style.label}>数字版本号</Col>
              <Col span={16} className={style.val}>
                {getFieldDecorator('androidVersionSeq', {
                  initialValue: latestAndroidVersionHistory.version,
                })(
                  <InputNumber />
                )}
              </Col>
            </Row>
            <Row>
              <Col span={8} className={style.label}>显示版本号</Col>
              <Col span={16} className={style.val}>
                {getFieldDecorator('androidVersionDisplay', {
                  initialValue: latestAndroidVersionHistory.versionDisplay,
                })(
                  <Input />
                )}
              </Col>
            </Row>
            <Row>
              <Col span={8} className={style.label}>下载地址</Col>
              <Col span={16} className={style.val}>
                {getFieldDecorator('androidDownloadUrl', {
                  initialValue: latestAndroidVersionHistory.releaseLink,
                })(
                  <Input />
                )}
              </Col>
            </Row>
            <Row>
              <Col span={8} className={style.label}>更新内容</Col>
              <Col span={16} className={style.val}>
                {getFieldDecorator('androidReleaseNotes', {
                  initialValue: latestAndroidVersionHistory.releaseNotes,
                })(
                  <Input />
                )}
              </Col>
            </Row>
            <Row style={{ margin: '8px 0' }}>
              <Col span={12}>
                {getFieldDecorator('androidForceUpdate', {
                  initialValue: latestAndroidVersionHistory.isForceUpdate == 1,
                  valuePropName: 'checked',
                })(
                  <Checkbox>是否强制更新</Checkbox>
                )}
              </Col>
              <Col span={12}>
                {getFieldDecorator('androidExplicitUpdate', {
                  initialValue: latestAndroidVersionHistory.isDisplayUpdate == 1,
                  valuePropName: 'checked',
                })(
                  <Checkbox>是否显式更新</Checkbox>
                )}
              </Col>
            </Row>
            <Row align="middle">
              <Button type="primary" onClick={handleSaveAndroid}>确定</Button>
            </Row>
          </Col>
          <Col span={12} className={style.tableInfo}>
            <Row>
              <Col span={8} className={style.label}>当前版本</Col>
              <Col span={16} className={style.val}>
                {getFieldDecorator('iosPlatform', {
                  initialValue: '苹果',
                })(
                  <Input readOnly />
                )}
              </Col>
            </Row>
            <Row>
              <Col span={8} className={style.label}>数字版本号</Col>
              <Col span={16} className={style.val}>
                {getFieldDecorator('iosVersionSeq', {
                  initialValue: latestIOSVersionHistory.version,
                })(
                  <InputNumber />
                )}
              </Col>
            </Row>
            <Row>
              <Col span={8} className={style.label}>显示版本号</Col>
              <Col span={16} className={style.val}>
                {getFieldDecorator('iosVersionDisplay', {
                  initialValue: latestIOSVersionHistory.versionDisplay,
                })(
                  <Input />
                )}
              </Col>
            </Row>
            <Row>
              <Col span={8} className={style.label}>下载地址</Col>
              <Col span={16} className={style.val}>
                {getFieldDecorator('iosDownloadUrl', {
                  initialValue: latestIOSVersionHistory.releaseLink,
                })(
                  <Input />
                )}
              </Col>
            </Row>
            <Row>
              <Col span={8} className={style.label}>更新内容</Col>
              <Col span={16} className={style.val}>
                {getFieldDecorator('iosReleaseNotes', {
                  initialValue: latestIOSVersionHistory.releaseNotes,
                })(
                  <Input />
                )}
              </Col>
            </Row>
            <Row style={{ margin: '8px 0' }}>
              <Col span={12}>
                {getFieldDecorator('iosForceUpdate', {
                  initialValue: latestIOSVersionHistory.isForceUpdate == 1,
                  valuePropName: 'checked',
                })(
                  <Checkbox>是否强制更新</Checkbox>
                )}
              </Col>
              <Col span={12}>
                {getFieldDecorator('iosExplicitUpdate', {
                  initialValue: latestIOSVersionHistory.isDisplayUpdate == 1,
                  valuePropName: 'checked',
                })(
                  <Checkbox>是否显式更新</Checkbox>
                )}
              </Col>
            </Row>
            <Row align="middle">
              <Button type="primary" onClick={handleSaveIOS}>确定</Button>
            </Row>
          </Col>
        </Row>
      </Form>
    </Card>
    <Modal
      visible={props.system.modalVisibility}
      title="新增版本"
      okText="确认"
      cancelText="取消"
      onOk={handleNewVersionSave}
      onCancel={handleNewVersionCancel}
      afterClose={handleNewVersionClose}
    >
      <NewVersionForm {...props} />
    </Modal>
  </React.Fragment>
};

export default Form.create()(SystemTable);
