import $http from "../../core/http";

export function fetchCategoryList(form, pagination) {
  return $http.get("/media/category/list", {
    params: {
      media_type: form.mediaType,
      page_no: (pagination.current - 1) || 0,
      page_size: pagination.pageSize,
    }
  });
}

export function saveCategory(form) {
  return $http.post("/media/category", form);
}

export function deleteCategory(form) {
  return $http.delete("/media/category", {
    params: {
      category_id: form.key
    }
  });
}

export function fetchAppReleaseList(payload) {
  return $http.get("/app/version/list", {
    params: payload,
  });
}

export function saveAppRelease(payload) {
  return $http.post("/app/version", payload);
}
