import React, { useEffect, useState } from "react"
import { Card, Button, Icon, Form, Table, Select, Modal, Popconfirm } from "antd";

import style from "./style.scss";

const VersionHistory = (props) => {

  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });

  const versionColumns = [
    { title: '版本', dataIndex: 'versionType' },
    { title: '数字版本号', dataIndex: 'version' },
    { title: '显示版本号', dataIndex: 'versionDisplay' },
    { title: '更新内容', dataIndex: 'releaseNotes' },
    { title: '下载地址', dataIndex: 'releaseLink' },
    {
      title: '是否强制更新', key: 'isForceUpdate',
      render: (text, record) => (
        <span>{record.isForceUpdate == 1 ? '是' : '否'}</span>
      )
    },
    {
      title: '是否显式更新', key: 'isDisplayUpdate',
      render: (text, record) => (
        <span>{record.isDisplayUpdate == 1 ? '是' : '否'}</span>
      )
    },
    { title: '发布时间', dataIndex: 'releaseTime' },
  ];

  const handleQuery = (p) => {
    props.dispatch({
      type: 'system/fetchVersionHistoryList',
      pagination
    });
  };

  const handleTableChanged = (p) => {
    setPagination(p);
  };

  useEffect(handleQuery, [pagination]);

  return <Card title="发布历史"
    bordered={false}
    headStyle={{ padding: '12px 24px 0 0' }}
    style={{ width: '100%' }}
    bodyStyle={{ paddingBottom: 0, paddingTop: 12 }}
  >
    <Table
      size="middle"
      columns={versionColumns}
      dataSource={props.system.versionHistoryList}
      rowKey={record => record.version + record.releaseTime}
      onChange={handleTableChanged}
      pagination={{
        ...pagination,
        showSizeChanger: true,
        total: props.system.versionHistoryTotal,
        showTotal: () => <span>总共 {props.system.versionHistoryTotal} 条记录</span>,
        pageSizeOptions: ['5', '10', '20'],
      }}
      scroll={{ x: 'max-content' }}
    />
  </Card>
};

export default VersionHistory;
