import React, { useEffect } from "react"
import { Card, Form, Row } from "antd";

import style from "./style.scss";

const ConfigSMS = (props) => {

  useEffect(() => {
    //
  }, []);

  const { getFieldDecorator } = props.form;

  return <React.Fragment>
    <Card title="短信区域配置"
      bordered={false}
      headStyle={{ padding: '0 24px 0 0' }}
      style={{ width: '100%' }}
      bodyStyle={{ paddingBottom: 0, paddingTop: 12 }}
    >
      <Row type="flex" justify="center" align="middle" style={{ minHeight: 120 }}>
        <h4>短信配置区域</h4>
      </Row>
    </Card>
  </React.Fragment>
};

export default Form.create()(ConfigSMS);
