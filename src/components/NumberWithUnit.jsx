import React from "react";
import { Input, Row, Col, Radio } from "antd";

class NumberWithUnit extends React.Component {
  static getDerivedStateFromProps(nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      return {
        ...(nextProps.value || {}),
      };
    }
    return null;
  }

  constructor(props) {
    super(props);

    const value = props.value || {};
    this.state = {
      number: value.number,
      unit: value.unit,
    };
  }

  triggerChange = changedValue => {
    // Should provide an event to pass value to Form.
    const { onChange } = this.props;
    if (onChange) {
      onChange(Object.assign({}, this.state, changedValue));
    }
  };

  handleNumberChange = (e) => {
    const number = parseInt(e.target.value || 0, 10);
    if (Number.isNaN(number)) {
      return;
    }
    this.setState({ number });
    this.triggerChange({ number });
  };

  handleUnitChange = (e, fieldName) => {
    const unit = e.target.value;
    
    this.setState({ unit });
    this.triggerChange({ unit });
  };

  render() {
    return (
      <Row className="numberUnit" gutter={8}>
        <Col span={10} style={{ paddingRight: 16 }}>
          <Input 
            type="number"
            placeholder="请输入"
            value={this.state.number}
            onChange={this.handleNumberChange} />
        </Col>
        <Col span={14}>
          <Radio.Group onChange={this.handleUnitChange} value={this.state.unit}>
            {this.props.unitOptions.map(u => (
              <Radio key={u.value} value={u.value}>{u.text}</Radio>
            ))}
          </Radio.Group>
        </Col>
      </Row>
    )
  }

}

export default NumberWithUnit;
