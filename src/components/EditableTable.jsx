import React from "react";
import { InputNumber, Select, Input, Popconfirm, Table, Form } from "antd";

import style from "./EditableTable.scss";

const EditableContext = React.createContext();

class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />
    } else if (this.props.inputType === 'select') {
      return <Select>
        {this.props.selectOptions.map(opt => (
          <Select.Option key={opt.value} value={opt.value}>{opt.text}</Select.Option>
        ))}
      </Select>;
    } else {
      return <Input />;
    }
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      selectOptions,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules: [
                {
                  required: true,
                  message: `请输入 ${title}!`,
                },
              ],
              initialValue: record[dataIndex],
            })(this.getInput())}
          </Form.Item>
        ) : (
            children
          )}
      </td>
    );
  };

  render() {
    return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
  }
}

class EditableTable extends React.Component {

  // TODO
  // columns, data

  constructor(props) {
    super(props);
    this.state = { editingKey: '' };
    this.columns = props.columns.concat({
      title: '操作',
      dataIndex: 'action',
      render: (text, record) => {
        const { editingKey } = this.state;
        const editable = this.isEditing(record);
        return editable ? (
          <span>
            <EditableContext.Consumer>
              {form => (
                <a href="javascript:;"
                  onClick={() => this.save(form, record.key)}
                  style={{ marginRight: 8 }}
                >
                  保存
                </a>
              )}
            </EditableContext.Consumer>
            <Popconfirm title="确认取消吗？" onConfirm={() => this.cancel(record.key)}>
              <a>取消</a>
            </Popconfirm>
          </span>
        ) : (
            <React.Fragment>
              <a disabled={editingKey !== ''} style={{ marginRight: 8 }} onClick={() => this.edit(record.key)}>
                编辑
            </a>
              <Popconfirm title="确认删除吗？" onConfirm={() => props.handleDeleteRow(this.props.form, record.key)}>
                <a disabled={editingKey !== ''}>删除</a>
              </Popconfirm>
            </React.Fragment>
          );
      },
    })
  }

  isEditing = record => record.key === this.state.editingKey;

  save(form, key) {
    form.validateFields((error, row) => {
      if (error) {
        console.log(error);
        return;
      }
      this.props.handleSaveRow(key, row, () => {
        this.setState({ editingKey: '' });
      });
    });
  }

  cancel() {
    this.setState({ editingKey: '' });
  }

  edit(key) {
    this.setState({ editingKey: key });
  }

  render() {
    const components = {
      body: { cell: EditableCell }
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.inputType,
          selectOptions: col.selectOptions,
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });

    return (
      <EditableContext.Provider value={this.props.form}>
        <Table
          components={components}
          bordered
          dataSource={this.props.dataSource}
          columns={columns}
          rowClassName={style.editableRow}
          pagination={this.props.pagination}
          rowKey={this.props.rowKey}
        />
      </EditableContext.Provider>
    );
  }
}

export default Form.create()(EditableTable);
