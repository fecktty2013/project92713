import React from "react";
import { Input, Row, Col } from "antd";

class NumberRange extends React.Component {
  static getDerivedStateFromProps(nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      return {
        ...(nextProps.value || {}),
      };
    }
    return null;
  }

  constructor(props) {
    super(props);

    const value = props.value || {};
    this.state = {
      start: value.start,
      end: value.end,
    };
  }

  triggerChange = changedValue => {
    // Should provide an event to pass value to Form.
    const { onChange } = this.props;
    if (onChange) {
      onChange(Object.assign({}, this.state, changedValue));
    }
  };

  handleNumberChange = (e, fieldName) => {
    const number = parseInt(e.target.value || 0, 10);
    if (Number.isNaN(number)) {
      return;
    }
    if (!('value' in this.props)) {
      this.setState({ [fieldName]: number });
    }
    this.triggerChange({ [fieldName]: number });
  };

  render() {
    return (
      <Row>
        <Col span={11}>
          <Input 
            type="number"
            placeholder="请输入"
            value={this.state.start}
            onChange={(e) => this.handleNumberChange(e, 'start')} />
        </Col>
        <Col span={2} style={{ textAlign: 'center' }}>
          <span>-</span>
        </Col>
        <Col span={11}>
        <Input 
            type="number"
            placeholder="请输入"
            value={this.state.end}
            onChange={(e) => this.handleNumberChange(e, 'end')} />
        </Col>
      </Row>
    )
  }

}

export default NumberRange;
