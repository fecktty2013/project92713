import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'dva';
import { withRouter } from "dva/router";
import { Link } from 'dva/router';
import { Layout, Menu, Icon, Button, Avatar } from 'antd';
import style from './style.scss';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

function RootLayout(props) {

  let handleSideDisplayChange = (sideDisplay) => {
    props.dispatch({ type: 'app/sideDisplayChange', payload: { sideDisplay } })
  };

  let selectedKeys = [];
  let menus = props.menus;

  for (let menuIndex = 0; menuIndex < menus.length; menuIndex++) {
    const menu = menus[menuIndex];
    if (menu.href === props.pathname) {
      selectedKeys.push(menu.pageId);
      break;
    }
    const subMenus = menu.subMenus || [];
    for (let subMenuIndex = 0; subMenuIndex < subMenus.length; subMenuIndex++) {
      const subMenu = subMenus[subMenuIndex];
      if (subMenu.href === props.pathname) {
        selectedKeys.push(subMenu.pageId);
        break;
      }
    }
  }

  let handleSubMenuClicked = (pathname) => {
    if (pathname) {
      props.dispatch({ type: 'app/navigateTo', payload: { pathname }});
    }
  };

  let logout = () => {
    props.dispatch({ type: 'app/logout', payload: { }});
  };

  let folded = props.sideDisplay === 'fold';

  return (
    <div id="appRoot">
      <Layout style={{ minHeight: '100vh' }}>
        <Sider className={classNames({
          [style.sider]: true,
          collapsed: folded,
          })}
          collapsible collapsed={folded} trigger={null}>
          <div className={classNames(style.logo, "logo")} />
          <Menu
            theme="dark"
            selectedKeys={selectedKeys}
            mode="inline">
            {menus.map(menu => {
              return menu.subMenus ? <SubMenu key={menu.pageId} title={
                <span>
                  <Icon type={menu.icon} />
                  <span>{menu.text}</span>
                </span>
              } onTitleClick={e => handleSubMenuClicked(menu.href)}>
                {menu.subMenus.map(subMenu => (
                  <Menu.Item key={subMenu.pageId}>
                    <Link to={subMenu.href}>{subMenu.text}</Link>
                  </Menu.Item>
                ))}
              </SubMenu> : <Menu.Item key={menu.pageId} title={menu.text}>
                <Link to={menu.href}>
                  <Icon type={menu.icon} />
                  <span style={{ display: folded ? 'none' : 'inline' }}>{menu.text}</span>
                </Link>
              </Menu.Item>
            })}
          </Menu>
        </Sider>
        <Layout className={style.main}>
          <Header className={style.header}>
            {
              folded
                ?
                <div className={style.sideToggler} onClick={ e => handleSideDisplayChange('unfold') }>
                  <Icon type="menu-unfold" />
                </div>
                :
                <div className={style.sideToggler} onClick={ e => handleSideDisplayChange('fold') }>
                  <Icon type="menu-fold" />
                </div>
            }
            <span className={style.avatar}>
              <Avatar className={style.avatarImg} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
              <span>{props.userName}</span>
              {props.authenticated ? <Button type="link" onClick={logout}>登出</Button> : undefined}
            </span>
          </Header>
          <Content className={style.body}>
            {props.children}
          </Content>
          <Footer className={style.footer}>视频App - Web端后台管理</Footer>
        </Layout>
      </Layout>
    </div>
  );
}

RootLayout.propTypes = {
  menus: PropTypes.array,
  sideDisplay: PropTypes.oneOf(['fold', 'unfold']),
  pathname: PropTypes.string,
}

export default connect(store => ({
  menus: store.app.menus,
  sideDisplay: store.app.sideDisplay,
  userName: store.app.userName,
  authenticated: store.app.authenticated,
  pathname: store.app.location.pathname,
}))(withRouter(RootLayout));
