
export const menuTree = [
    {
        title: '总览',
        key: 'summary-summary',
    },
    {
        title: '用户管理',
        key: 'user',
        children: [
            { title: '用户总览', key: 'user-user_summary' },
            { title: '头像管理', key: 'user-head_img' },
            { title: '分销管理', key: 'user-user_invite' }
        ]
    },
    {
        title: '内容管理',
        key: 'media',
        children: [
            { title: '视频管理', key: 'media-video' },
            { title: '音频管理', key: 'media-audio' },
            { title: '相册管理', key: 'media-photo' }
        ]
    },
    {
        title: '分类管理',
        key: 'category-category'
    },
    {
        title: '运营管理',
        key: 'operation',
        children: [
            { title: '基础管理', key: 'operation-base' },
            { title: '运营设置', key: 'operation-config' },
        ]
    },
    {
        title: '资金管理',
        key: 'fund-fund',
    },
    {
        title: '设置管理',
        key: 'config-config',
    },
    {
        title: '评论管理',
        key: 'comment',
        children: [
            { title: '评论管理', key: 'comment-management' },
            { title: '机器人管理', key: 'comment-robot' }
        ]
    },
    {
        title: '账户管理',
        key: 'account-account'
    },
    {
        title: '日志管理',
        key: 'log-log'
    },
    {
        title: '系统管理',
        key: 'system-system',
    }
]