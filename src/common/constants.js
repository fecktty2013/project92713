
export const MEDIA_TYPE = {
  VIDEO: 1,
  PHOTO: 2,
  AUDIO: 3,
};

export const STATUS = {
  DELETED: 0,
  NORMAL: 1,
  FREEZED: 2,
};

export const ADV_STATUS = {
  ENABLED: 1,
  DISABLED: 2,
};

export const COMMENT_RESULT = {
  NORMAL: 'pass',
  BLOCK: 'block',
};

export const ACTIVITY_TYPE = {
  LAST_7_DAYS: '最近7天已登录', // 'l_7days',
  LAST_30_DAYS: '最近30天已登录', // 'l_30days',
  LAST_365_DAYS: '最近365天已登录', // 'l_365days',
  NO_7_DAYS: '最近7天未登录', // 'n_7days',
  NO_30_DAYS: '最近30天未登录', // 'n_30days',
  NO_365_DAYS: '最近365天未登录', // 'n_365days',
};
