# 服务器环境和部署指南

## 基础环境
- nginx

## 部署事项
1. `/dist` 目录为web应用根目录
2. `/dist/backups` 目录为web应用artifacts待发布包
3. `/dist/deploy.sh` 脚本为服务器部署并重启nginx，参数为artifacts包路径
4. `/dist/webapps/` 目录为应用程序发布目录，nginx配置指向此目录

## 本地deploy
这里其实是上传发布包到远程服务器，运行本地项目根目录deploy.sh，参数为服务器IP地址

## 上线流程
本地build -> 本地deploy -> 服务器deploy -> 发布成功
```
需要注意：本地脚本运行成功的前提是ssh公私钥已经配置好，并授权到远程服务器
```
