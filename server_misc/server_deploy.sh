#!/usr/bin/env bash

rm -rf /dist/webapps/video-web-cms/*

tar -zxvf $1 --strip-components=1 -C /dist/webapps/video-web-cms/

nginx -s reload