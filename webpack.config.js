const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const plugins = [
  new HtmlWebpackPlugin({
    filename: "index.html",
    template: path.join(__dirname, "src", "index.html")
  }),
  new MiniCssExtractPlugin({
    filename: "[name]-[contenthash:6].css",
    chunkFilename: "[id].[chunkhash:6].css",
    ignoreOrder: true,
  }),
  new webpack.NoEmitOnErrorsPlugin(),
  new CleanWebpackPlugin(),
];

if (process.env.NODE_ENV === 'development') {
  plugins.push(new webpack.HotModuleReplacementPlugin());
}
if (process.env.ANALYZE) {
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
  plugins.push(new BundleAnalyzerPlugin());
}
plugins.push(new webpack.DefinePlugin({
  PRODUCTION: JSON.stringify(process.env.NODE_ENV === 'production')
}));

module.exports = {
  entry: {
    app: path.join(__dirname, "src", "index.js"),
  },
  output: {
    path: path.join(__dirname, "dist"),
    chunkFilename: "[name]-[chunkhash:6].chunk.js",
    filename: "[name].[hash:6].js",
    publicPath: "/",
  },
  optimization: {
    splitChunks: {
      name: false,
      chunks: "async",
      minSize: 30000,
    }
  },
  devtool: "source-map",
  resolve: {
    extensions: [".wasm", ".mjs", ".js", ".jsx", ".json"]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /antd.*\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader?modules=false"],
      },
      {
        test: /.(css|scss)$/,
        exclude: /antd.*\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader?modules=true", "sass-loader"],
      },
      {
        test: /.(jpg|jpeg|png|gif|mp3|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path][name]-[hash:8].[ext]"
            }
          }
        ]
      }
    ]
  },
  plugins,
  devServer: {
    historyApiFallback: true,
    hot: true,
    stats: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    }
  },
}
