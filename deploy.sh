#!/usr/bin/env bash

# parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

projectname="video_cms"

timestamp=$(date +%Y%m%d%H%M%S)

filename="${projectname}_${timestamp}.tar.gz"

echo "Compressing $filename..."

tar -czvPf $filename dist/*

echo "Compressing $filename complete"

echo "Uploading..."

scp $filename $1:/dist/backups/

echo "Uploading complete"

echo "Clean up $filename"

rm -f $filename

echo "Done!"
